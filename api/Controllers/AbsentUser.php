<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 22.01.2015
 * Time: 10:59
 */
class Controllers_AbsentUser extends RestController
{
    private $_arStatus = array();
    private $_arStatusIds = array();
    private $_iblockId = 3;
    function __construct($request)
    {
        parent::__construct($request);
        $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $this->_iblockId, "CODE" => "ABSENCE_TYPE"));
        while ($enum_fields = $property_enums->GetNext()) {
            $this->_arStatus[$enum_fields['XML_ID']] = $enum_fields['ID'];
            $this->_arStatusIds[$enum_fields['ID']] = $enum_fields['XML_ID'];
        }
    }
    public function get()
    {
        // TODO: Implement get() method.
    }

    public function post()
    {
        // TODO: Implement post() method.
       $s = '{
            "GUID": "753672aa-1b84-11e3-94cd-100000021734",
            "ACTIVE_FROM": "01.02.2015",
            "ACTIVE_TO": "10.02.2015",
            "ABSENCE_TYPE": "VACATION",
            "EMAIL": "d.korotihin@ngsec.ru"}';
        $this->responseStatus = 200;
        $arData = $this->request['params'];
        //$arData = json_decode($s, true);
        $rsUsers = CUser::GetList($by, $order, array('EMAIL' => $arData['EMAIL']),array('FIELDS' =>array('ID','NAME','LAST_NAME')));
        if($arUser = $rsUsers->GetNext())
        {
            $userId = $arUser['ID'];
            $el = new CIBlockElement();
            $arFields = array(
                'ACTIVE_FROM' => $arData['ACTIVE_FROM'],
                'ACTIVE_TO' => $arData['ACTIVE_TO'],
                'NAME' => $arData['NAME'],
                'PROPERTY_VALUES' => array(
                    'USER' => $userId,
                    'ABSENCE_TYPE' => $this->_arStatus[$arData['ABSENCE_TYPE']]
                )
            );
            $list = CIBlockElement::GetList(array(),array('CHECK_PERMISSIONS' => 'N',"XML_ID" => $arData['GUID'],'IBLOCK_ID' => $this->_iblockId),false,false,array('ID'));
            if($row = $list->GetNext())
            {
                $success = $el->Update($row['ID'], $arFields);
                if ($success)
                    $this->response = json_encode(array('success' => 1));
                else
                    $this->response = json_encode(array('success' => 0, 'error' => $el->LAST_ERROR));
            }
            else {
                $arFields['XML_ID'] = $arData['GUID'];
                $arFields['IBLOCK_ID'] = $this->_iblockId;
                $success = $el->Add($arFields, false, false,false );
                if ($success)
                    $this->response = json_encode(array('success' => 1));
                else
                    $this->response = json_encode(array('success' => 0, 'error' => $el->LAST_ERROR));

            }
        }
        else {
            $this->response = json_encode(array('success' => 0,'error' => 'not found user'));
        }
    }

    public function put()
    {
        // TODO: Implement put() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}