<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 12.02.2015
 * Time: 14:40
 */

class Controllers_Tasks extends RestController
{
    function __construct($request){
        parent::__construct($request);
        \Bitrix\Main\loader::includeModule('tasks');
    }
    public function get()
    {
        // TODO: Implement get() method.
    }

    public function post()
    {
        $this->responseStatus = 200;
        $request = $this->request['params'];
        $arData = array(
            'TITLE' => $request['TITLE'],
            'DESCRIPTION' => $request['DESCRIPTION'],
            'DEADLINE' => $request['DEADLINE'],
            'DESCRIPTION_IN_BBCODE' => 'N',
            'AUDITORS' => array(579)
        );
        if ($request['DEAL_ID'] > 0)
        {
            $arLinks = array();
            $arLinks[] = 'D_'.$request['DEAL_ID'];
            $arData['UF_CRM_TASK'] = $arLinks;

        }
        if (strlen($request['RESPONSIBLE_ID']) > 0) {
            $rsUsers = CUser::GetList($by, $order, array('XML_ID' => $request['RESPONSIBLE_ID']), array('FIELDS' => array('ID')));
            if($arUser = $rsUsers->GetNext())
            {
                $arData['RESPONSIBLE_ID'] = $arUser['ID'];
            }

        }
        try {
            CTaskItem::add($arData, 1);
        }
        catch(Exception $ex)
        {
            $this->response = json_encode(array('success' => 0,'error' => $ex->getMessage()));
            return;
        }
         $this->response = json_encode(array('success' => 1));
    }

    public function put()
    {
        // TODO: Implement put() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }
}