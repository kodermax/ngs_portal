<?

class Controllers_Deals1s extends RestController
{
    public $arStatus = null;
    public $arStage = null;
    public $arStageId = array();
    public $arScope = array(
        1 => 'Банки и финансы',
        2 => 'Государственные структуры',
        3 => 'Здравоохранение',
        4 => 'Информационные технологии',
        5 => 'Телекоммуникация',
        6 => 'Сервис-провайдер',
        7 => 'Многопрофильные компании',
        8 => 'Производственные компании',
        9 => 'Образование',
        10 => 'СМИ и издательства',
        11 => 'Розничные продажи',
        12 => 'Услуги',
        13 => 'Транспорт',
        14 => 'Оптовая торговляанки',
        15 => 'ДРУГОЕ',
    );
    public $arVendors = array();
    public $arStatusId = array();
    protected $LAST_ERROR = '';

    function __construct($request)
    {

        parent::__construct($request);

        \Bitrix\Main\Loader::includeModule('crm');
        \Bitrix\Main\Loader::includeModule('iblock');
        $rsStage = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => "UF_CRM_STAGE_DEAL"
        ));
        while ($obStage = $rsStage->GetNext())
        {
            $this->arStage[$obStage['XML_ID']] = array(
                "ID" => $obStage["ID"],
                "NAME" => $obStage['VALUE']);
            $this->arStageId[$obStage["ID"]] = $obStage['VALUE'];
        }
        $rsStatus = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => "UF_CRM_1364300676",
        ));
        while ($obStatus = $rsStatus->GetNext())
        {
            $this->arStatus[$obStatus['XML_ID']] = array(
                "ID" => $obStatus["ID"],
                "NAME" => $obStatus["VALUE"]);
            $this->arStatusId[$obStatus['ID']] =
                array(
                    "XML_ID" => $obStatus['XML_ID'],
                    "TITLE" =>  $obStatus['VALUE']
                );
        }

        //Вендоры
        $obRes = CCrmCompany::GetList(
            array(),
            array('CHECK_PERMISSIONS' => 'N','COMPANY_TYPE' => 1),
            array('ID','TITLE', 'ORIGIN_ID', 'CURRENCY_ID')
        );
        while ($arRes = $obRes->Fetch())
        {
            $this->arVendors[$arRes['ID']] = array(
                'TITLE' => $arRes['TITLE'],
                'ORIGIN_ID' => $arRes['ORIGIN_ID'],
                'CURRENCY_ID' => $arRes['CURRENCY_ID']
            );
        }

    }
    public function get()
    {
        $arResult = array();
        $this->response = json_encode(array('SUCCESS' => 1));
        $this->responseStatus = 204;

    }

    public function post()
    {
        $CCrmDeal = new CCrmDeal(false);
        $arDeals = $this->request['params'];
        //Поиск сделки
        foreach($arDeals as $arDeal)
        {

            if ($arDeal['CURRENCY'] == 'KZT') continue;
            $list = $CCrmDeal->GetList(array(),array('ID' => $arDeal['ID'],'CHECK_PERMISSIONS'=>'N'));
            if($row = $list->GetNext())
            {
                $dealId = $row['ID'];
                $arFields = array(
                    'CURRENCY_ID' => $arDeal['CURRENCY'],
                    'UF_MARGIN' => $arDeal['MARGIN'],
                    'EXCHANGE_1C' => true
                );
                $bSuccess = $CCrmDeal->Update($dealId,$arFields,false,false);
                $arRows = array();
                foreach($arDeal['GOODS'] as $arItem)
                {
                    $arProduct = $this->GetProductIdByGuid($arItem['GUID']);
                    $arRows[] = array(
                        'PRODUCT_ID' => $arProduct['ID'],
                        'PRODUCT_NAME' => $arProduct['NAME'],
                        'PRICE' => $arItem['PRICE'],
                        'QUANTITY' => $arItem['AMOUNT'],
                        'TAX_RATE' => $arItem['NDS'],
                        'TAX_INCLUDED' => 'Y'
                    );
                }
                if (count($arRows) > 0) {
                    // SYNC OPPORTUNITY WITH PRODUCT ROW SUM TOTAL

                    $CCrmDeal->SaveProductRows($dealId, $arRows, false, false, true);
                    $arTotalInfo = CCrmProductRow::CalculateTotalInfo('D', $dealId, false);
                    if (is_array($arTotalInfo)) {
                        $arFields = array(
                            'OPPORTUNITY' => isset($arTotalInfo['OPPORTUNITY']) ? $arTotalInfo['OPPORTUNITY'] : 0.0,
                            'TAX_VALUE' => isset($arTotalInfo['TAX_VALUE']) ? $arTotalInfo['TAX_VALUE'] : 0.0,
                            'EXCHANGE_1C' => true
                        );
                    }
                    $bSuccess = $CCrmDeal->Update($dealId, $arFields, false, false, array());
                }

            }
        }

        $this->response = json_encode(array('SUCCESS' => $bSuccess ? 1 : 0,'ERROR' => $CCrmDeal->LAST_ERROR));
        $this->responseStatus = 200;
    }
    private function GetProductIdByGuid($guid)
    {
        if (strlen($guid) <= 0) return array();
        $list = CIBlockElement::GetList(array(),array('IBLOCK_ID'=> IBLOCK_CRM_CATALOG, 'CODE' => $guid, 'CHECK_PERMISSIONS' => 'N'), false,false, array('ID', 'NAME'));
        if ($row = $list->GetNext())
        {
            return array('ID' => $row['ID'],'NAME' => $row['~NAME']);
        }
        return array();
    }

    public function put()
    {
        $this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}


