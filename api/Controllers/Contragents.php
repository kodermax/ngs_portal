<?

class Controllers_Contragents extends RestController
{
    public function get()
    {
        global $APPLICATION;
        $this->responseStatus = 200;
        $this->response = array("result" => null);
    }

    public function post()
    {

        \Bitrix\Main\loader::includeModule('crm');
        $success = 0;
        $str = '[{
                    "GUID": "b9bc1eab-4f0d-11dd-b6bd-0019bbc828d6",
                    "TIP": "PARTNER",
                    "NAME": "1АБ Центр Проектных Технологий (ООО)",
                    "ENGNAME": "ENG_TEST",
                    "INN": "4324234",
                    "KPP": "6546746",
                    "MANAGER": "7613adc3-a8c6-11e1-9762-10000001987c",
                    "ADDRESSFACT": "105120, Москва г, Сыромятническая Ниж. ул, дом № 5\/7 стр. 2",
                    "ADDRESSUR": "105120, Москва г, Сыромятническая Ниж. ул, дом № 5\/7 стр. 2",
                    "ADDRESSDEL": null,
                    "PHONE": "502-18-30",
                    "EMAIL": "kodermax@gmail.com",
                    "WEB": "www.ya.ru",
                    "ACTIVE": "Y"
                    }]';
        $arClasses = array();
        $rsClass = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => 'UF_CLASS',
        ));
        while($arClass = $rsClass->GetNext())
        {
            $arClasses[$arClass['VALUE']] = $arClass['ID'];
        }

        $arTypes = array('EMAIL', 'PHONE', 'WEB');
        $arData = $this->request['params'];
        $crm = new CCrmCompany(false);
        $arErrors = array();
        foreach($arData as $arItem)
        {
            if ($arItem['TIP'] == 'VENDOR')
                $arItem['TIP'] = 1;
            //Поиск менеджера
            $companyId = 0;
            $rsUsers = CUser::GetList($by,$order,array('XML_ID' => trim($arItem['MANAGER'])),array('SELECT' => array('ID')));
            if($arUser = $rsUsers->GetNext())
            {
                $managerId = $arUser['ID'];
            }
            else {
                $arErrors[] = array('er' => 'Не найден пользователь '.$arItem['MANAGER']. ' Компания: '.$arItem['NAME']);
                continue;
            }
            $arFilter = array(
                'ORIGIN_ID' => trim($arItem['GUID']),
                'CHECK_PERMISSIONS' => 'N');
            $list = CCrmCompany::GetList(array(),$arFilter,array('ID'));
            if($row = $list->GetNext())
            {

                $companyId = $row['ID'];
                if ($companyId > 0)
                {
                        $managerId = $arUser['ID'];
                        $arFields = array(
                            'IS_EXTERNAL' => true,
                            'TITLE' => $arItem['NAME'],
                            'UF_CRM_CO_NAME_EN' => $arItem['ENGNAME'],
                            'UF_CRM_1363603025' => $arItem['INN'],
                            'UF_CRM_1363603036' => $arItem['KPP'],
                            'UF_CLASS' => $arClasses[$arItem['CLASS']],
                            'ASSIGNED_BY_ID' => $managerId,
                            'ADDRESS' => $arItem['ADDRESSFACT'],
                            'COMPANY_TYPE' => $arItem['TIP'],
                            'ADDRESS_LEGAL' => $arItem['ADDRESSUR'],
                            'ACTIVE' => $arItem['ACTIVE'],
                            'UF_CRM_CO_S_ADR_RU' => $arItem['ADDRESSDEL'],
                            'UF_CRM_BANK_DETAILS' => $arItem['BANK_DETAILS']

                        );
                        $res = CCrmFieldMulti::GetList(
                            array('ID' => 'asc'),
                            array(
                                'ENTITY_ID' => 'COMPANY',
                                'ELEMENT_ID' => $companyId,
                            )
                        );
                        while($ar = $res->Fetch())
                        {
                            $arFields['FM'][$ar['TYPE_ID']][$ar['ID']] = array('VALUE' => $arItem[$ar['TYPE_ID']], 'VALUE_TYPE' => $ar['VALUE_TYPE']);
                            $arFields['FM'][$ar['TYPE_ID']][$ar['ID']] = array('VALUE' => $arItem[$ar['TYPE_ID']], 'VALUE_TYPE' => $ar['VALUE_TYPE']);
                        }
                        foreach($arTypes as $type)
                        {
                            if(!array_key_exists($type, $arFields['FM']))
                                $arFields['FM'][$type]['n0'] = array('VALUE' => $arItem[$type], 'VALUE_TYPE' => 'WORK');
                        }

                        $crm->Update($companyId, $arFields,false,false);
                        $success++;
                }

            }
            else {
                $arErrors[] = array('er' => 'Не найдена компания '.$arItem['NAME']);
               //Добавляем компанию
                $arFields = array(
                    'IS_EXTERNAL' => true,
                    'TITLE' => $arItem['NAME'],
                    'UF_CRM_CO_NAME_EN' => $arItem['ENGNAME'],
                    'UF_CRM_1363603025' => $arItem['INN'],
                    'UF_CRM_1363603036' => $arItem['KPP'],
                    'UF_CLASS' => $arItem['CLASS'],
                    'ASSIGNED_BY_ID' => $managerId,
                    'ADDRESS' => $arItem['ADDRESSFACT'],
                    'ADDRESS_LEGAL' => $arItem['ADDRESSUR'],
                    'ACTIVE' => $arItem['ACTIVE'],
                    'ORIGIN_ID' => $arItem['GUID'],
                    'COMPANY_TYPE' => $arItem['TIP'],
                    'UF_CRM_CO_S_ADR_RU' => $arItem['ADDRESSDEL'],
                    'FM' => array(
                        'EMAIL' => array('n0' => array('VALUE' => $arItem["EMAIL"],'VALUE_TYPE' => 'WORK')),
                        'PHONE' => array('n0' => array('VALUE' => $arItem['TEL'], 'VALUE_TYPE' => 'WORK')),
                        'WEB' => array('n0' => array('VALUE' => $arItem['WEB'], 'VALUE_TYPE' => 'WORK')))
                );

                $ID = $crm->Add($arFields,false);
                if (strlen($crm->LAST_ERROR) > 0 )
                    $arErrors[] = array('er' => $crm->LAST_ERROR);
                if ($ID > 0 ) {
                    $companyId = $ID;
                    $success++;
                }
            }
        }
        $this->response = json_encode(array(
            'SUCCESS' => 1,
            'ERRORS' => $arErrors,
            'TOTAL' => count($arData),
            'TOTAL_SUCCESS' => $success
        ));
        $this->responseStatus = 200;
    }

    public function put()
    {
        $this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
?>