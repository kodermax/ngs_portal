<?php
/**
 * Created by PhpStorm.
 * User: Администратор
 * Date: 19.01.2015
 * Time: 21:31
 */
class Controllers_OContragents extends RestController
{
    function __construct($request)
    {
        parent::__construct($request);
        \Bitrix\Main\loader::includeModule('crm');
    }
    public function get()
    {
        $crm = new CCrmCompany(false);
        $this->responseStatus = 200;
        $request = $this->request['params'];

            if (strlen($request['id']) > 0 && !array_key_exists('guid',$request)) {
            $arFilter = array(
                'CHECK_PERMISSIONS' => 'N',
                'ID' => (int)$request['id']
            );
            $list = $crm->GetList(array(), $arFilter, array('ORIGIN_ID'));
            if ($row = $list->GetNext()) {
                $this->response = (string)$row['ORIGIN_ID'];
            }
            else {
                $this->response = "NOT_FOUND";
            }
        }
        elseif($request['id'] > 0 && array_key_exists('guid',$request)) {
            //Проверка компании
            $arFilter = array(
            'CHECK_PERMISSIONS' => 'N',
                'ID' => $request['id']
            );
            $list = $crm->GetList(array(), $arFilter, array('ORIGIN_ID'));
            if ($row = $list->GetNext()) {
                $arFields = array('ORIGIN_ID' => trim($request['guid']));
                $result = $crm->Update($request['id'], $arFields, false, false);
                if ($result)
                    $this->response = "DONE";
                else
                    $this->response = "ERROR";
            }
            else {
                $this->response = "NOT_FOUND";
            }
        }
    }
    public function post()
    {

    }
    public function put(){

    }
    public function delete() {

    }

}