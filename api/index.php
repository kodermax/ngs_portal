<?

$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', false);
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);
require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__)));
// Define application environment
defined('APPLICATION_ENV')  || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));
// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH),
    get_include_path(),
)));
// Define path to data directory
defined('APPLICATION_DATA')
|| define('APPLICATION_DATA', realpath(dirname(__FILE__) . '/../../data/logs'));

require_once("Rest.php");
require_once ('Controllers/Invoices.php');
require_once ('Controllers/InvoicesPayment.php');
require_once ('Controllers/User.php');
require_once ('Controllers/Deals.php');
//require_once ('Controllers/DealsTrustWave.php');
require_once ('Controllers/Vendors.php');
require_once ('Controllers/Products.php');
require_once ('Controllers/Staff.php');
require_once('Controllers/Contragents.php');
require_once('Controllers/Deals1s.php');
require_once('Controllers/OContragents.php');
require_once('Controllers/AbsentUser.php');
require_once('Controllers/Tasks.php');
$rest = new Rest();
$rest->process();



