<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Зарплата");?>
<?
$date = new DateTime();
$date->sub(new DateInterval('P1M'));
$date = $date->format('01.'.'m.Y');
?>
<script type="text/javascript" src="/shared/lib/jquery/jquery.fileDownload.js"></script>
    <style>
        .content_1c table {table-layout: fixed; padding: 0px; padding-left: 2px; vertical-align:bottom; border-collapse:collapse;width: 100%; font-family: Arial; font-size: 8pt; font-style: normal; }
        .content_1c td { padding: 0px; padding-left: 2px; overflow:hidden; }

    </style>
    <div class="ui form" style="width:1150px;">
        <div class="three fields">
			<div class="field" style="width:140px;">

                    <div class="ui mini icon input">
                        <input name="date_fld" id="date_fld" type="text" value="<?=$date;?>" placeholder="Дата">
                        <i class="calendar icon" onclick="BX.calendar({node:this, field:'date_fld', form: '', bTime: false, currentTime: '1424964037', bHideTime: false});"></i>
                    </div>
            </div>
            <div class="field" style="width:100px;">

                <div class="ui mini icon input">
                    <input name="pin" id="pin" type="password" value="" placeholder="Пин">
                </div>
            </div>
			<div class="field">                

				<div class="ui tiny buttons">
					<div class="ui tiny blue button" style="height:34px;" onclick="showReport('N')"><i class="desktop icon"></i>Получить</div>
					<div class="or"></div>
					<div class="ui tiny green button" style="height:34px;" onclick="downloadExcel()"><i class="file excel outline icon"></i>Сохранить</div>
				</div>

            </div>
        </div>


    </div>
    <div class="content content_1c">

    </div>
<script>
    function downloadExcel() {
        var inputDate = $('#date_fld').val();	/* Дата из формы, на которую формировать отчет */
        var inputPin = $('#pin').val();
        $('.content').html('Загрузка данных....');
        $.fileDownload('/app/api/v1/reports/zp/file?date='+inputDate+'&pin='+inputPin, {
            successCallback: function () {
                $('.content').html('Файл загружен.');
            }});
		$('#pin').val('');
    }
    function showReport(saveValue) {
		var inputDate = $('#date_fld').val();	/* Дата из формы, на которую формировать отчет */
		var inputPin = $('#pin').val();			/* Пин код. */

        $('.content').html('Загрузка данных....');
        $.ajax({
            cache:false,
            type: "POST",
			url: "/app/api/v1/reports/zp",  /* сюда создай и запихни свой новый запрос к моему сервису*/
			data: {date: inputDate, pin: inputPin, save: saveValue} /* Я хз как правильно передать переменную saveValue */

        }).done(function(data) {
            $('.content').html(data);
            $('#pin').val('');
        });
    }
</script>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>