<?
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

if (!CModule::IncludeModule('crm'))
{
    return;
}

global $USER, $APPLICATION;
CUtil::JSPostUnescape();
$APPLICATION->RestartBuffer();
Header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);
if (!$USER->IsAuthorized() || $_SERVER['REQUEST_METHOD'] != 'POST')
{
    return;
}
$mode = isset($_POST['MODE']) ? $_POST['MODE'] : '';
$eventText = isset($_POST['VALUE']) ? $_POST['VALUE'] : '';
if(!isset($mode[0]))
{
    die();
}
$ownerID = $_POST["OWNER_ID"] ? $_POST["OWNER_ID"] : '';
$ownerTypeName = 'DEAL';
$perms = new CCrmPerms($USER->GetID());

if(!isset($mode[0]))
{
    die();
}
if ($mode == "ADD_COMMENT")
{
    if($ownerID <= 0)
    {
        echo CUtil::PhpToJSObject(array('ERROR'=>'OWNER_ID_NOT_FOUND'));
        die();
    }
    // Register event only for owner
    $CCrmEvent = new CCrmEvent();
    $CCrmEvent->Add(
        array(
            'ENTITY' => array(
                $ownerID => array(
                    'ENTITY_TYPE' => $ownerTypeName,
                    'ENTITY_ID' => $ownerID,
                    'ENTITY_FIELD' => 'STAGE_ID'
                )
            ),
            'EVENT_NAME' => 'Изменено поле "Стадия сделки"',
            'EVENT_TYPE' => '1',
            'EVENT_TEXT_1' => $eventText,
        )
    );
    die();
}
