<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
global $APPLICATION;
$APPLICATION->SetAdditionalCSS('/bitrix/js/crm/css/crm.css');
$APPLICATION->AddHeadScript('/bitrix/js/crm/interface_form.js');
// Looking for 'tab_1' (Only single tab is supported).
$mainTab = null;
foreach($arResult['TABS'] as $tab):
    if($tab['id'] !== 'tab_1')
        continue;

    $mainTab = $tab;
    break;
endforeach;

//Take first tab if tab_1 is not found
if(!$mainTab):
    foreach($arResult['TABS'] as $tab):
        $mainTab = $tab;
        break;
    endforeach;
endif;

if(!($mainTab && isset($mainTab['fields']) && is_array($mainTab['fields'])))
    return;

$arEmphasizedFields = array();
$arEmphasizedHeaders = isset($arParams['EMPHASIZED_HEADERS']) ? $arParams['EMPHASIZED_HEADERS'] : array();
if(!empty($arEmphasizedHeaders))
{
    foreach($mainTab['fields'] as $k => &$field):
        if(in_array($field['id'], $arEmphasizedHeaders)):
            $arEmphasizedFields[$k] = $field;
            unset($mainTab['fields'][$k]);

        endif;
    endforeach;
    unset($field);
}
$hasRequiredFields = false;

?><div class="bx-interface-form bx-crm-edit-form"><?
if($GLOBALS['USER']->IsAuthorized() && $arParams["SHOW_SETTINGS"] == true):?>
    <div class="bx-crm-edit-form-menu-wrapper">
    <a href="javascript:void(0)" onclick="bxForm_<?=$arParams["FORM_ID"]?>.menu.ShowMenu(this, bxForm_<?=$arParams["FORM_ID"]?>.settingsMenu);" title="<?=htmlspecialcharsbx(GetMessage("interface_form_settings"))?>" class="bx-context-button bx-form-menu"><span></span></a>
    </div><?
endif;
?><script type="text/javascript">
    var bxForm_<?=$arParams['FORM_ID']?> = null;
</script><?
if($arParams['SHOW_FORM_TAG']):
?><form name="form_<?=$arParams['FORM_ID']?>" id="form_<?=$arParams["FORM_ID"]?>" action="<?=POST_FORM_ACTION_URI?>" method="POST" enctype="multipart/form-data"><?
echo bitrix_sessid_post();
?><input type="hidden" id="<?=$arParams["FORM_ID"]?>_active_tab" name="<?=$arParams["FORM_ID"]?>_active_tab" value="<?=htmlspecialcharsbx($arResult["SELECTED_TAB"])?>"><?
endif;

$arUserSearchFields = array();

if(count($arEmphasizedFields) > 0):
    ?><div class="webform-round-corners webform-main-fields">
    <div class="webform-corners-top">
        <div class="webform-left-corner"></div>
        <div class="webform-right-corner"></div>
    </div><?
    foreach($arEmphasizedFields as &$field):
        $required = isset($field['required']) && $field['required'] === true;
        if($required && !$hasRequiredFields)
            $hasRequiredFields = true;

        $params = '';
        if(is_array($field['params']))
            foreach($field['params'] as $p=>$v)
                $params .= ' '.$p.'="'.$v.'"';

        ?><div class="webform-content">
        <div class="bx-crm-edit-title-label"><?if($required):?><span class="required">*</span><?endif; echo htmlspecialcharsEx($field['name'])?></div>
        <div class="bx-crm-edit-title-wrapper">
            <input type="text" class="bx-crm-edit-title" name="<?=htmlspecialcharsbx($field["id"])?>" value="<?=isset($field['value']) ? htmlspecialcharsbx($field['value']) : ''?>" <?=$params?>/>
        </div>
        </div><?
    endforeach;
    unset($field);
    ?></div><?
endif;

$arSections = array();
$sectionIndex = -1;
foreach($mainTab['fields'] as &$field):

    if(!is_array($field))
        continue;

    if($field['type'] === 'section'):
        $arSections[] = array(
            'SECTION_FIELD' => $field,
            'SECTION_ID' => $field['id'],
            'SECTION_NAME' => $field['name'],
            'FIELDS' => array()
        );
        $sectionIndex++;
        continue;
    endif;

    //Skip empty BP params
    if(strpos($field['id'], 'BP_PARAMETERS') === 0 && $field['value'] === '')
    {
        continue;
    }

    if($sectionIndex < 0):
        $arSections[] = array(
            'SECTION_FIELD' => null,
            'SECTION_ID' => '',
            'SECTION_NAME' => '',
            'FIELDS' => array()
        );
        $sectionIndex = 0;
    endif;

    $arSections[$sectionIndex]['FIELDS'][] = $field;
endforeach;
unset($field);

foreach($arSections as &$arSection):
if(empty($arSection['FIELDS']))
    continue;
$required = (isset($arSection['SECTION_FIELD']['required']) && $arSection['SECTION_FIELD']['required'] === true) ? true : false;
?><div class="bx-crm-edit-content-block" id="<?=$arSection["SECTION_ID"]?>">
<div class="bx-crm-edit-content-block-title"><?if($required):?><span class="required">*</span><?endif;?><?=htmlspecialcharsbx($arSection['SECTION_NAME'])?></div><?

$fieldCount = 0;
foreach($arSection['FIELDS'] as &$field):


$required = isset($field['required']) && $field['required'] === true;

//default attributes
if(!is_array($field['params']))
    $field['params'] = array();

if($field['type'] == '' || $field['type'] == 'text')
{
    if($field['params']['size'] == '')
        $field['params']['size'] = '30';
}
elseif($field['type'] == 'textarea')
{
    if($field['params']['cols'] == '')
        $field['params']['cols'] = '40';

    if($field['params']['rows'] == '')
        $field['params']['rows'] = '3';
}
elseif($field['type'] == 'date')
{
    if($field['params']['size'] == '')
        $field['params']['size'] = '10';
}
elseif($field['type'] == 'date_short')
{
    if($field['params']['size'] == '')
        $field['params']['size'] = '10';
}

$params = '';
if(is_array($field['params']) && $field['type'] <> 'file')
    foreach($field['params'] as $p=>$v)
        $params .= ' '.$p.'="'.$v.'"';

$val = isset($field['value']) ? $field['value'] : $arParams['~DATA'][$field['id']];

if($field['type'] === 'vertical_container'):

    if($fieldCount > 0):
        ?><div class="bx-crm-edit-content-separator"></div><?
    endif; ?>
    <div class="bx-crm-edit-content-block-vertical-element">
    <div class="bx-crm-edit-content-block-vertical-element-name"><?if($required):?><span class="required">*</span><?endif; echo htmlspecialcharsEx($field['name'])?></div><?
    if(isset($field['value'])):
        if ($field['id'] == 'COMMENTS'):?>
            <textarea rows="10" cols="100" disabled="disabled"><?=$arParams['DATA']['~COMMENTS']?></textarea>
        <?else:?>
            <div class="bx-crm-edit-content-block-vertical-element-wrapper"><?=$val?></div>
        <?endif;?>
    <?endif;?>
    </div><?
    $fieldCount++;
    continue;
elseif($field['type'] === 'vertical_checkbox'):
    if($fieldCount > 0):
        ?><div class="bx-crm-edit-content-separator"></div><?
    endif;
    ?><div class="bx-crm-edit-content-block-checkbox">
    <input type="hidden" name="<?=$field['id']?>" value="N" />
    <input type="checkbox" class="bx-crm-edit-content-checkbox" name="<?=$field['id']?>" value="Y" <?=(($val === true || $val === 'Y')? ' checked':'')?><?=$params?>/>
    <div class="bx-crm-edit-content-checkbox-label"><?if($required):?><span class="required">*</span><?endif; echo htmlspecialcharsEx($field['name'])?></div>
    <div class="bx-crm-edit-content-checkbox-description"><?= isset($field['title']) ? htmlspecialcharsEx($field['title']) : '' ?></div>
    </div><?
    $fieldCount++;
    continue;
endif;

$isWide = $field['colspan'] === true;
$isHidden = $field['hidden'] === true;

?><div<?= isset($field['hidden']) ? ' id="hidden_'.$field['id'].'"' : '' ?> class="<?=$isWide ? 'bx-crm-edit-content-block-wide-element' : 'bx-crm-edit-content-block-element'?>"><?
if(!$isWide):
    $required = isset($field['required']) && $field['required'] === true;
    if($required && !$hasRequiredFields)
        $hasRequiredFields = true;

    ?><span class="bx-crm-edit-content-block-element-name"><?if($required):?><span class="required">*</span><?endif; if(isset($field['nameWrapper'])): echo '<span id="', htmlspecialcharsbx($field['nameWrapper']), '">', htmlspecialcharsEx($field['name']), '</span>'; else: echo htmlspecialcharsEx($field['name']); endif;?>:</span><?
endif;

switch($field['type']):
case 'label':
    echo '<div id="'.$field["id"].'" class="crm-fld-block-readonly">', $val, '</div>';
    break;
case 'custom':
$isUserField = strpos($field['id'], 'UF_') === 0;
$wrap = isset($field['wrap']) && $field['wrap'] === true;
if($isUserField):
?><div class="bx-crm-edit-user-field"><?
elseif($wrap):
?><div class="bx-crm-edit-field"><?
    endif;

    echo $val;
    if($isUserField || $wrap):
    ?></div><?
endif;
break;
case 'checkbox':
    ?><input type="hidden" name="<?=$field["id"]?>" value="N">
    <input type="checkbox" name="<?=$field["id"]?>" value="Y"<?=($val == "Y"? ' checked':'')?><?=$params?>><?
    break;
case 'textarea':
    ?><textarea class="bx-crm-edit-text-area" name="<?=$field["id"]?>"<?=$params?>><?=$val?></textarea><?
    break;
case 'list':
    ?><select <?if($field['id']=='STAGE_ID'):?>disabled="disabled" <?endif;?>class="bx-crm-edit-input" name="<?=$field["id"]?>"<?=$params?>><?
    if(is_array($field["items"])):
        if(!is_array($val))
            $val = array($val);
        foreach($field["items"] as $k=>$v):
            ?><option value="<?=htmlspecialcharsbx($k)?>"<?=(in_array($k, $val)? ' selected':'')?>><?=htmlspecialcharsEx($v)?></option><?
        endforeach;
    endif;
    ?></select><?
    break;
case 'file':
    ?><div class="bx-crm-edit-file-field"><?
    $arDefParams = array("iMaxW"=>150, "iMaxH"=>150, "sParams"=>"border=0", "strImageUrl"=>"", "bPopup"=>true, "sPopupTitle"=>false, "size"=>20);
    foreach($arDefParams as $k=>$v)
        if(!array_key_exists($k, $field["params"]))
            $field["params"][$k] = $v;

    echo CFile::InputFile($field["id"], $field["params"]["size"], $val);
    if($val <> '')
        echo '<br>'.CFile::ShowImage($val, $field["params"]["iMaxW"], $field["params"]["iMaxH"], $field["params"]["sParams"], $field["params"]["strImageUrl"], $field["params"]["bPopup"], $field["params"]["sPopupTitle"]);
    ?></div><?
    break;
case 'date':
    $APPLICATION->IncludeComponent(
        "bitrix:main.calendar",
        "",
        array(
            "SHOW_INPUT"=>"Y",
            "INPUT_NAME"=>$field["id"],
            "INPUT_VALUE"=>$val,
            "INPUT_ADDITIONAL_ATTR"=>$params,
            "SHOW_TIME" => 'Y'
        ),
        $component,
        array("HIDE_ICONS"=>true)
    );
    break;
case 'date_short':
    $APPLICATION->IncludeComponent(
        "bitrix:main.calendar",
        "",
        array(
            "SHOW_INPUT"=>"Y",
            "INPUT_NAME"=>$field["id"],
            "INPUT_VALUE"=>$val,
            "INPUT_ADDITIONAL_ATTR"=>$params,
            "SHOW_TIME" => "N",
            "HIDE_TIMEBAR" => "Y"
        ),
        $component,
        array("HIDE_ICONS"=>true)
    );
    break;
case 'date_link':
    $dataID = "{$arParams['FORM_ID']}_{$field['id']}_DATA";
    $viewID = "{$arParams['FORM_ID']}_{$field['id']}_VIEW";
    ?><span id="<?=htmlspecialcharsbx($viewID)?>" class="bx-crm-edit-datetime-link"><?=htmlspecialcharsEx($val !== '' ? $val : GetMessage('interface_form_set_datetime'))?></span>
    <input id="<?=htmlspecialcharsbx($dataID)?>" type="hidden" name="<?=htmlspecialcharsbx($field['id'])?>" value="<?=htmlspecialcharsbx($val)?>" <?=$params?>>
    <script type="text/javascript">BX.ready(function(){ BX.CrmDateLinkField.create(BX('<?=CUtil::addslashes($dataID)?>'), BX('<?=CUtil::addslashes($viewID)?>'), { showTime: false }); });</script><?
    break;
case 'intranet_user_search':
    $params = isset($field['componentParams']) ? $field['componentParams'] : array();
    if(!empty($params)):
        $rsUser = CUser::GetByID($val);
        if($arUser = $rsUser->Fetch()):
            $params['USER'] = $arUser;
        endif;
        ?><input type="text" class="bx-crm-edit-input" name="<?=htmlspecialcharsbx($params['SEARCH_INPUT_NAME'])?>">
        <input type="hidden" name="<?=htmlspecialcharsbx($params['INPUT_NAME'])?>" value="<?=htmlspecialcharsbx($val)?>"><?
        $arUserSearchFields[] = $params;
        $APPLICATION->IncludeComponent(
            'bitrix:intranet.user.selector.new',
            '',
            array(
                'MULTIPLE' => 'N',
                'NAME' => $params['NAME'],
                'INPUT_NAME' => $params['SEARCH_INPUT_NAME'],
                'POPUP' => 'Y',
                'SITE_ID' => SITE_ID,
                'NAME_TEMPLATE' => $params['NAME_TEMPLATE']
            ),
            null,
            array('HIDE_ICONS' => 'Y')
        );
    endif;
    break;
case 'crm_entity_selector':
    CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/crm.js');
    $params = isset($field['componentParams']) ? $field['componentParams'] : array();
    if(!empty($params)):
        $entityType = isset($params['ENTITY_TYPE']) ? $params['ENTITY_TYPE'] : '';
        $entityID = isset($params['INPUT_VALUE']) ? intval($params['INPUT_VALUE']) : 0;
        $editorID = "{$arParams['FORM_ID']}_{$field['id']}";
        $containerID = "{$arParams['FORM_ID']}_FIELD_CONTAINER_{$field['id']}";
        $selectorID = "{$arParams['FORM_ID']}_ENTITY_SELECTOR_{$field['id']}";
        $changeButtonID = "{$arParams['FORM_ID']}_CHANGE_BTN_{$field['id']}";
        $dataInputName = isset($params['INPUT_NAME']) ? $params['INPUT_NAME'] : $field['id'];
        $dataInputID = "{$arParams['FORM_ID']}_DATA_INPUT_{$dataInputName}";
        $newDataInputName = isset($params['NEW_INPUT_NAME']) ? $params['NEW_INPUT_NAME'] : '';
        $newDataInputID = $newDataInputName !== '' ? "{$arParams['FORM_ID']}_NEW_DATA_INPUT_{$dataInputName}" : '';
        $entityInfo = CCrmEntitySelectorHelper::PrepareEntityInfo($entityType, $entityID);
        ?><div id="<?=htmlspecialcharsbx($containerID)?>" class="bx-crm-edit-crm-entity-field">
        <div class="bx-crm-entity-info-wrapper"><?
            if($entityID > 0):
                ?><a href="<?=htmlspecialcharsbx($entityInfo['URL'])?>" target="_blank" class="bx-crm-entity-info-link"><?=htmlspecialcharsEx($entityInfo['TITLE'])?></a><span class="crm-element-item-delete"></span><?
            endif;
            ?></div>
        <input type="hidden" id="<?=htmlspecialcharsbx($dataInputID)?>" name="<?=htmlspecialcharsbx($dataInputName)?>" value="<?=htmlspecialcharsbx($entityID)?>" /><?
        if($newDataInputName !== ''):
            ?><input type="hidden" id="<?=htmlspecialcharsbx($newDataInputID)?>" name="<?=htmlspecialcharsbx($newDataInputName)?>" value="" /><?
        endif;
        ?><div class="bx-crm-entity-buttons-wrapper">
            <span id="<?=htmlspecialcharsbx($changeButtonID)?>" class="bx-crm-edit-crm-entity-change"><?= htmlspecialcharsbx(GetMessage('intarface_form_select'))?></span><?
            if($newDataInputName !== ''):
                ?> <span class="bx-crm-edit-crm-entity-add"><?=htmlspecialcharsEx(GetMessage('interface_form_add_new_entity'))?></span><?
            endif;
            ?></div>
    </div><?
        $serviceUrl = '';
        $actionName = '';
        $dialogSettings = array(
            'addButtonName' => GetMessage('interface_form_add_dialog_btn_add'),
            'cancelButtonName' => GetMessage('interface_form_cancel')
        );
        if($entityType === 'CONTACT')
        {
            $serviceUrl = '/bitrix/components/bitrix/crm.contact.edit/ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get();
            $actionName = 'SAVE_CONTACT';

            $dialogSettings['title'] = GetMessage('interface_form_add_contact_dlg_title');
            $dialogSettings['lastNameTitle'] = GetMessage('interface_form_add_contact_fld_last_name');
            $dialogSettings['nameTitle'] = GetMessage('interface_form_add_contact_fld_name');
            $dialogSettings['secondNameTitle'] = GetMessage('interface_form_add_contact_fld_second_name');
            $dialogSettings['emailTitle'] = GetMessage('interface_form_add_contact_fld_email');
            $dialogSettings['phoneTitle'] = GetMessage('interface_form_add_contact_fld_phone');
            $dialogSettings['exportTitle'] = GetMessage('interface_form_add_contact_fld_export');
        }
        elseif($entityType === 'COMPANY')
        {
            $serviceUrl = '/bitrix/components/bitrix/crm.company.edit/ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get();
            $actionName = 'SAVE_COMPANY';

            $dialogSettings['title'] = GetMessage('interface_form_add_company_dlg_title');
            $dialogSettings['titleTitle'] = GetMessage('interface_form_add_company_fld_title_name');
            $dialogSettings['companyTypeTitle'] = GetMessage('interface_form_add_conpany_fld_company_type');
            $dialogSettings['industryTitle'] = GetMessage('interface_form_add_company_fld_industry');
            $dialogSettings['emailTitle'] = GetMessage('interface_form_add_conpany_fld_email');
            $dialogSettings['phoneTitle'] = GetMessage('interface_form_add_company_fld_phone');
            $dialogSettings['addressLegalTitle'] = GetMessage('interface_form_add_company_fld_address_legal');
            $dialogSettings['companyTypeItems'] = CCrmEntitySelectorHelper::PrepareListItems(CCrmStatus::GetStatusList('COMPANY_TYPE'));
            $dialogSettings['industryItems'] = CCrmEntitySelectorHelper::PrepareListItems(CCrmStatus::GetStatusList('INDUSTRY'));
        }
        elseif($entityType === 'DEAL')
        {
            $dialogSettings['title'] = GetMessage('interface_form_add_company_dlg_title');
            $dialogSettings['titleTitle'] = GetMessage('interface_form_add_company_fld_title_name');
            $dialogSettings['dealTypeTitle'] = GetMessage('interface_form_add_conpany_fld_company_type');
            $dialogSettings['dealPriceTitle'] = GetMessage('interface_form_add_company_fld_industry');
            $dialogSettings['companyTypeItems'] = CCrmEntitySelectorHelper::PrepareListItems(CCrmStatus::GetStatusList('DEAL_TYPE'));
        }
        elseif($entityType === 'QUOTE')
        {
            $dialogSettings['titleTitle'] = GetMessage('interface_form_add_company_fld_title_name');
        }

        ?><script type="text/javascript">
        BX.ready(
            function()
            {
                var entitySelectorId = CRM.Set(
                    BX('<?=CUtil::JSEscape($changeButtonID) ?>'),
                    '<?=CUtil::JSEscape($selectorID)?>',
                    '',
                    <?=CUtil::PhpToJsObject(CCrmEntitySelectorHelper::PreparePopupItems($entityType, false, isset($params['NAME_TEMPLATE']) ? $params['NAME_TEMPLATE'] : \Bitrix\Crm\Format\PersonNameFormatter::getFormat()))?>,
                    false,
                    false,
                    ['<?=CUtil::JSEscape(strtolower($entityType))?>'],
                    <?=CUtil::PhpToJsObject(CCrmEntitySelectorHelper::PrepareCommonMessages())?>,
                    true
                );

                BX.CrmEntityEditor.messages =
                {
                    'unknownError': '<?=GetMessageJS('interface_form_ajax_unknown_error')?>'
                };

                BX.CrmEntityEditor.create(
                    '<?=CUtil::JSEscape($editorID)?>',
                    {
                        'typeName': '<?=CUtil::JSEscape($entityType)?>',
                        'containerId': '<?=CUtil::JSEscape($containerID)?>',
                        'dataInputId': '<?=CUtil::JSEscape($dataInputID)?>',
                        'newDataInputId': '<?=CUtil::JSEscape($newDataInputID)?>',
                        'entitySelectorId': entitySelectorId,
                        'serviceUrl': '<?= CUtil::JSEscape($serviceUrl) ?>',
                        'actionName': '<?= CUtil::JSEscape($actionName) ?>',
                        'dialog': <?=CUtil::PhpToJSObject($dialogSettings)?>
                    }
                );
            }
        );
    </script><?
    endif;
    break;
case 'crm_client_selector':
    CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/crm.js');
    $params = isset($field['componentParams']) ? $field['componentParams'] : array();
    if(!empty($params))
    {
        $entityID = $inputValue = isset($params['INPUT_VALUE']) ? $params['INPUT_VALUE'] : '';
        $entityType = isset($params['ENTITY_TYPE']) ? $params['ENTITY_TYPE'] : '';
        switch (substr($entityID, 0, 2))
        {
            case 'C_':
                $valEntityType = 'contact';
                break;
            case 'CO':
                $valEntityType = 'company';
                break;
            default:
                $valEntityType = '';
        }
        $entityID = intval(substr($entityID, intval(strpos($entityID, '_')) + 1));
        $editorID = "{$arParams['FORM_ID']}_{$field['id']}";
        $containerID = "{$arParams['FORM_ID']}_FIELD_CONTAINER_{$field['id']}";
        $createEntitiesBlockID = "{$arParams['FORM_ID']}_CREATE_ENTITIES_{$field['id']}";
        $selectorID = "{$arParams['FORM_ID']}_ENTITY_SELECTOR_{$field['id']}";
        $changeButtonID = "{$arParams['FORM_ID']}_CHANGE_BTN_{$field['id']}";
        $addContactButtonID = "{$arParams['FORM_ID']}_ADD_CONTACT_BTN_{$field['id']}";
        $addCompanyButtonID = "{$arParams['FORM_ID']}_ADD_COMPANY_BTN_{$field['id']}";
        $dataInputName = isset($params['INPUT_NAME']) ? $params['INPUT_NAME'] : $field['id'];
        $dataInputID = "{$arParams['FORM_ID']}_DATA_INPUT_{$dataInputName}";
        $newDataInputName = isset($params['NEW_INPUT_NAME']) ? $params['NEW_INPUT_NAME'] : '';
        $newDataInputID = $newDataInputName !== '' ? "{$arParams['FORM_ID']}_NEW_DATA_INPUT_{$dataInputName}" : '';
        $entityInfo = CCrmEntitySelectorHelper::PrepareEntityInfo($valEntityType, $entityID);
        ?><div id="<?=htmlspecialcharsbx($containerID)?>" class="bx-crm-edit-crm-entity-field">
        <div class="bx-crm-entity-info-wrapper">
            <? if($entityID > 0): ?>
                <a href="<?=htmlspecialcharsbx($entityInfo['URL'])?>" target="_blank" class="bx-crm-entity-info-link"><?=htmlspecialcharsEx($entityInfo['TITLE'])?></a><span class="crm-element-item-delete"></span>
            <? endif;?>
        </div>
        <input type="hidden" id="<?=htmlspecialcharsbx($dataInputID)?>" name="<?=htmlspecialcharsbx($dataInputName)?>" value="<?=htmlspecialcharsbx($inputValue)?>" />
        <? if($newDataInputName !== ''): ?>
            <input type="hidden" id="<?=htmlspecialcharsbx($newDataInputID)?>" name="<?=htmlspecialcharsbx($newDataInputName)?>" value="" />
        <? endif; ?>
        <!--<div class="bx-crm-entity-buttons-wrapper">-->
        <span id="<?=htmlspecialcharsbx($changeButtonID)?>" class="bx-crm-edit-crm-entity-change"><?= htmlspecialcharsbx(GetMessage('intarface_form_select'))?></span>
        <? if($newDataInputName !== ''): ?>
            <br>
            <span id="<?=htmlspecialcharsbx($createEntitiesBlockID)?>" class="bx-crm-edit-description"<?=($entityID>0)?' style="display: none;"':''?>>
							<span><?=htmlspecialcharsEx(GetMessage('interface_form_add_new_entity'))?> </span>
							<span id="<?=htmlspecialcharsbx($addCompanyButtonID)?>" class="bx-crm-edit-crm-entity-add"><?= htmlspecialcharsbx(GetMessage('interface_form_add_btn_company'))?></span>
							<span><?= htmlspecialcharsbx(' '.GetMessage('interface_form_add_btn_or')).' '?></span>
							<span id="<?=htmlspecialcharsbx($addContactButtonID)?>" class="bx-crm-edit-crm-entity-add"><?= htmlspecialcharsbx(GetMessage('interface_form_add_btn_contact'))?></span>





















							</span>
        <? endif; ?>
        <!--</div>-->
    </div><?
        $dialogSettings['CONTACT'] = array(
            'addButtonName' => GetMessage('interface_form_add_dialog_btn_add'),
            'cancelButtonName' => GetMessage('interface_form_cancel'),
            'title' => GetMessage('interface_form_add_contact_dlg_title'),
            'lastNameTitle' => GetMessage('interface_form_add_contact_fld_last_name'),
            'nameTitle' => GetMessage('interface_form_add_contact_fld_name'),
            'secondNameTitle' => GetMessage('interface_form_add_contact_fld_second_name'),
            'emailTitle' => GetMessage('interface_form_add_contact_fld_email'),
            'phoneTitle' => GetMessage('interface_form_add_contact_fld_phone'),
            'exportTitle' => GetMessage('interface_form_add_contact_fld_export')

        );
        $dialogSettings['COMPANY'] = array(
            'addButtonName' => GetMessage('interface_form_add_dialog_btn_add'),
            'cancelButtonName' => GetMessage('interface_form_cancel'),
            'title' => GetMessage('interface_form_add_company_dlg_title'),
            'titleTitle' => GetMessage('interface_form_add_company_fld_title_name'),
            'companyTypeTitle' => GetMessage('interface_form_add_conpany_fld_company_type'),
            'industryTitle' => GetMessage('interface_form_add_company_fld_industry'),
            'emailTitle' => GetMessage('interface_form_add_conpany_fld_email'),
            'phoneTitle' => GetMessage('interface_form_add_company_fld_phone'),
            'addressLegalTitle' => GetMessage('interface_form_add_company_fld_address_legal'),
            'companyTypeItems' => CCrmEntitySelectorHelper::PrepareListItems(CCrmStatus::GetStatusList('COMPANY_TYPE')),
            'industryItems' => CCrmEntitySelectorHelper::PrepareListItems(CCrmStatus::GetStatusList('INDUSTRY'))
        );
        ?><script type="text/javascript">
        BX.ready(
            function()
            {
                var entitySelectorId = CRM.Set(
                    BX('<?=CUtil::JSEscape($changeButtonID) ?>'),
                    '<?=CUtil::JSEscape($selectorID)?>',
                    '',
                    <?=CUtil::PhpToJsObject(CCrmEntitySelectorHelper::PreparePopupItems($entityType, true, isset($params['NAME_TEMPLATE']) ? $params['NAME_TEMPLATE'] : ''))?>,
                    false,
                    false,
                    <?=CUtil::PhpToJsObject($entityType)?>,
                    <?=CUtil::PhpToJsObject(CCrmEntitySelectorHelper::PrepareCommonMessages())?>,
                    true
                );

                BX.CrmEntityEditor.messages =
                {
                    'unknownError': '<?=GetMessageJS('interface_form_ajax_unknown_error')?>'
                };

                BX.CrmEntityEditor.create(
                    '<?=CUtil::JSEscape($editorID.'_C')?>',
                    {
                        'typeName': 'CONTACT',
                        'containerId': '<?=CUtil::JSEscape($containerID)?>',
                        'buttonAddId': '<?=CUtil::JSEscape($addContactButtonID)?>',
                        'enableValuePrefix': true,
                        'dataInputId': '<?=CUtil::JSEscape($dataInputID)?>',
                        'newDataInputId': '<?=CUtil::JSEscape($newDataInputID)?>',
                        'entitySelectorId': entitySelectorId,
                        'serviceUrl': '<?=CUtil::JSEscape('/bitrix/components/bitrix/crm.contact.edit/ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get())?>',
                        'actionName': 'SAVE_CONTACT',
                        'dialog': <?=CUtil::PhpToJSObject($dialogSettings['CONTACT'])?>
                    }
                );

                BX.CrmEntityEditor.create(
                    '<?=CUtil::JSEscape($editorID).'_CO'?>',
                    {
                        'typeName': 'COMPANY',
                        'containerId': '<?=CUtil::JSEscape($containerID)?>',
                        'buttonAddId': '<?=CUtil::JSEscape($addCompanyButtonID)?>',
                        'buttonChangeIgnore': true,
                        'enableValuePrefix': true,
                        'dataInputId': '<?=CUtil::JSEscape($dataInputID)?>',
                        'newDataInputId': '<?=CUtil::JSEscape($newDataInputID)?>',
                        'entitySelectorId': entitySelectorId,
                        'serviceUrl': '<?=CUtil::JSEscape('/bitrix/components/bitrix/crm.company.edit/ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get())?>',
                        'actionName': 'SAVE_COMPANY',
                        'dialog': <?=CUtil::PhpToJSObject($dialogSettings['COMPANY'])?>
                    }
                );
            }
        );
    </script><?
    }
    break;
default:
    ?><input type="text" class="bx-crm-edit-input" name="<?=$field["id"]?>" value="<?=htmlspecialcharsbx($val)?>"<?=$params?>><?
    break;
endswitch;
$fieldCount++;
?></div><?
endforeach;
unset($field);
?></div><?
endforeach;
unset($arSection);

$arFieldSets = isset($arParams['FIELD_SETS']) ? $arParams['FIELD_SETS'] : array();
if(!empty($arFieldSets)):
    foreach($arFieldSets as &$arFieldSet):
        $html = isset($arFieldSet['HTML']) ? $arFieldSet['HTML'] : '';
        if($html === '')
            continue;
        ?><div class="bx-crm-view-fieldset">
        <h2 class="bx-crm-view-fieldset-title"><?=isset($arFieldSet['NAME']) ? htmlspecialcharsbx($arFieldSet['NAME']) : ''?></h2>
        <div class="bx-crm-view-fieldset-content">
            <table class="bx-crm-view-fieldset-content-table">
                <tbody>
                <tr>
                    <td class="bx-field-value"><?=$html?></td>
                </tr>
                </tbody>
            </table>
        </div>
        </div><?
    endforeach;
    unset($arFieldSet);
endif;
?>
<div class="bx-crm-view-fieldset check-fieldset" style="display:none">
    <h2 class="bx-crm-view-fieldset-title">Результат проверки сделки</h2>
    <div class="bx-crm-view-fieldset-content">
        <table class="bx-crm-view-fieldset-content-table">
            <tbody>
            <tr>
                <td class="bx-field-errors"></td>
            </tr>
            <tr>
                <td class="bx-field-value"></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<div class="ui menu">
    <a class="item" onclick="checkDuplicate();" href="javascript:void(0);">
        <i class="large search green icon"></i>
        Проверить
    </a>
    <a class="item addTask" href="javascript:void(0)">
        <i class="large tasks black icon"></i>
        Задача на проверку спецификации
    </a>
    <a class="item confirmSpecLink" href="javascript:void(0)">
        <i class="large thumbs up outline black icon"></i>
        Подтвердить спецификацию
    </a>
    <a class="item summaryLink" href="javascript:void(0)">
        <i class="large list layout green icon"></i>
        Данные для вендора
    </a>
    <a class="item regDealLink" href="javascript:void(0)">
        <i class="large ok sign green icon"></i>
        Зарегистрировать сделку
    </a>
    <a class="item decline-deal" href="javascript:void(0);">
        <i class="large remove sign layout purple icon"></i>
        Отказать...
    </a>
</div>
<br/>
<?
if(isset($arParams['~BUTTONS'])):
if($arParams['~BUTTONS']['standard_buttons'] !== false):
?><div class="webform-buttons ">
			<span class="webform-button webform-button-create">
				<span class="webform-button-left"></span>
				<input class="webform-button-text" type="submit" name="saveAndView" id="<?=$arParams["FORM_ID"]?>_saveAndView" value="<?=htmlspecialcharsbx(GetMessage('interface_form_save_and_view'))?>" title="<?= htmlspecialcharsbx(GetMessage('interface_form_save_and_view_title'))?>" />
				<span class="webform-button-right"></span>
			</span><?
    if(isset($arParams['IS_NEW']) && $arParams['IS_NEW'] === true):
        ?><span class="webform-button">
        <span class="webform-button-left"></span>
				<input class="webform-button-text" type="submit" name="saveAndAdd" id="<?=$arParams["FORM_ID"]?>_saveAndAdd" value="<?=htmlspecialcharsbx(GetMessage('interface_form_save_and_add'))?>" title="<?= htmlspecialcharsbx(GetMessage('interface_form_save_and_add_title'))?>" />
				<span class="webform-button-right"></span>
        </span><?
    else:
        ?><span class="webform-button">
        <span class="webform-button-left"></span>
				<input class="webform-button-text" type="submit" name="apply" id="<?=$arParams["FORM_ID"]?>_apply" value="<?=htmlspecialcharsbx(GetMessage('interface_form_apply'))?>" title="<?= htmlspecialcharsbx(GetMessage('interface_form_apply_title'))?>" />
				<span class="webform-button-right"></span>
        </span><?
    endif;
    if(isset($arParams['~BUTTONS']['back_url']) && $arParams['~BUTTONS']['back_url'] !== ''):
        ?><span class="webform-button">
        <span class="webform-button-left"></span>
				<input class="webform-button-text" type="button" name="cancel" onclick="window.location='<?=CUtil::JSEscape($arParams['~BUTTONS']['back_url'])?>'" value="<?= htmlspecialcharsbx(GetMessage('interface_form_cancel'))?>" title="<?= htmlspecialcharsbx(GetMessage('interface_form_cancel_title'))?>" />
				<span class="webform-button-right"></span>
        </span><?
    endif;
    ?>
</div>

<?
endif;
if(isset($arParams['~BUTTONS']['custom_html'])):
    echo $arParams['~BUTTONS']['custom_html'];
endif;
endif;

if($arParams['SHOW_FORM_TAG']):
?>
</form><?
endif;
?>
<div class="ui modal summaryModal">
    <i class="close icon"></i>
    <div class="header">
        Данные для регистрации у вендора
    </div>
    <div class="content">
        <div class="ui form segment">
            <div class="two fields">
                <div class="field">
                    <label>Полное наименование Заказчика: </label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER']['TITLE']?> </div>
                </div>
                <div class="field">
                    <label>Полное наименование Заказчика EN: </label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER']['TITLE_EN']?> </div>
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Web–сайт компании:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER']['WWW_WORK']?> </div>
                </div>
                <div class="field">
                    <label>Web–сайт компании EN:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER']['WWW_WORK']?> </div>
                </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>Адрес / индекс:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER']['ADDRESS_LEGAL']?> </div>
                </div>
                <div class="field">
                    <label>Адрес / индекс EN:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER']['ADDRESS_LEGAL_EN']?> </div>
                </div>
            </div>
                <div class="field">
                    <label>ИНН / КПП:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER']['INN']?> / <?=$arParams['DATA']['CUSTOMER']['KPP']?> </div>
                </div>
            <div class="two fields">
                <div class="field">
                    <label>Ф.И.О. контактного лица заказчика:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER_CONTACT']['FIO']?> </div>
                </div>
                <div class="field">
                    <label>Ф.И.О. контактного лица заказчика EN:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER_CONTACT']['FIO_EN']?> </div>
                </div>
            </div>
            <div class="inline field">
                <label>Телефон контактного лица:</label>
                <div class="ui label"><?=$arParams['DATA']['CUSTOMER_CONTACT']['PHONE_WORK']?> </div>
            </div>
            <div class="two fields">
                <div class="field">
                    <label>E-Mail контактного лица:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER_CONTACT']['EMAIL_WORK']?> </div>
                </div>
                <div class="field">
                    <label>E-Mail контактного лица EN:</label>
                    <div class="ui label"><?=$arParams['DATA']['CUSTOMER_CONTACT']['EMAIL_WORK']?> </div>
                </div>
            </div>
            <div class="field">
                <label>Дата отгрузки:</label>
                <div class="ui label"><?=ConvertDateTime($arParams['DATA']['UF_CRM_1385483673'],"DD.MM.YYYY");?></div>
            </div>
            <div class="field">
                <label>Ориентировочный срок реализации проекта:</label>
                <div class="ui label"> </div>
            </div>
        </div>
        <table class="ui table segment">
            <thead>
                <tr>
                    <th>Артикул</th>
                    <th>Наименование</th>
                    <th>Кол-во</th>
                </tr>
            </thead>
            <tbody>
                <?foreach($arParams['DATA']['PRODUCT_ROWS'] as $row):?>
                <tr>
                    <td><?=$row['PRODUCT_ARTICLE']?></td>
                    <td><?=$row['PRODUCT_NAME']?></td>
                    <td><?=$row['QUANTITY']?></td>
                </tr>
                <?endforeach;?>
            </tbody>
        </table>
    </div>

</div>

<div class="ui modal declineModal">
    <i class="close icon"></i>
    <div class="header">
        Отказать в регистрации
    </div>
    <div class="content">
        <div class="ui dimmer modalLoader"><div class="ui mini text loader">Отправка письма партнёру</div></div>
        <div class="ui form">
            <div class="field">
                <label>Email на который будет отправлен отказ</label>
                <input class="email_work" type="text" name="email" id="email"/>
            </div>
            <div class="field">
                <label>Комментарий</label>
                <textarea class="message" name="message"></textarea>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="two fluid ui buttons">
            <div class="ui positive right labeled icon button">
                Да
                <i class="checkmark icon"></i>
            </div>
            <div class="ui negative labeled icon button">
                <i class="remove icon"></i>
                Нет
            </div>

        </div>
    </div>
</div>
<div class="ui modal taskModal">
    <i class="close icon"></i>
    <div class="header">
        Создать задачу "Проверить спецификацию" для <span class="title"></span>
    </div>
    <div class="content">
        <div class="ui dimmer modalLoader"><div class="ui mini text loader">Создание задачи</div></div>
        <p></p>
    </div>
    <div class="actions">
        <div class="two fluid ui buttons">
            <div class="ui positive right labeled icon button">
                Да
                <i class="checkmark icon"></i>
            </div>
            <div class="ui negative labeled icon button">
                <i class="remove icon"></i>
                Нет
            </div>

        </div>
    </div>
</div>
<div class="ui modal confirmSpecModal">
    <i class="close icon"></i>
    <div class="header">
        Подтвердить спецификацию
    </div>
    <div class="content">
        <div class="ui dimmer modalLoader"><div class="ui mini text loader">Отправка уведомлений</div></div>
        <div class="ui form">
            <div class="field">
                <label>Email на который будет отправлено уведомление</label>
                <input class="email_work" type="text" name="email" id="email"/>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="two fluid ui buttons">
            <div class="ui positive right labeled icon button">
                Да
                <i class="checkmark icon"></i>
            </div>
            <div class="ui negative labeled icon button">
                <i class="remove icon"></i>
                Нет
            </div>

        </div>
    </div>
</div>
<div class="ui modal regDealModal">
    <i class="close icon"></i>
    <div class="header">
        Зарегистрировать сделку
    </div>
    <div class="content">
        <div class="ui dimmer modalLoader"><div class="ui mini text loader">Отправка уведомлений</div></div>
        <div class="ui form">
            <div class="field">
                <label>Email Партнёра на который будет отправлено уведомление</label>
                <input class="email_work" type="text" name="email" id="email"/>
            </div>
        </div>
    </div>
    <div class="actions">
        <div class="two fluid ui buttons">
            <div class="ui positive right labeled icon button">
                Да
                <i class="checkmark icon"></i>
            </div>
            <div class="ui negative labeled icon button">
                <i class="remove icon"></i>
                Нет
            </div>

        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        var companyEmailWork = '<?=$arParams['DATA']['COMPANY_EMAIL_WORK']?>';
        $('.ui.modal').modal();
        $('.summaryLink').click(function(){
            $('.summaryModal').modal('setting', {
                closable  : true,
                onDeny    : function(){
                    return true;
                },
                onApprove : function() {
                    return false;
                }
            }).modal('show');
        });
        $('.addTask').click(function()
        {
            $('.taskModal .title').text('<?=$arParams['DATA']['MANAGER_NAME']?>');
            $('.taskModal').modal('setting', {
                closable  : true,
                onDeny    : function(){
                    return true;
                },
                onApprove : function() {

                    $('.modalLoader').addClass('active');
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "/local/php_interface/include/ngsec/crm/ajax_crm_deal.php",
                        data: {MODE: 'ADD_TASK_SPEC', DEAL_ID: '<?=$arParams['DATA']['ID']?>'}
                    }).done(function( data ) {
                        if (data != null && data.result == 1)
                        {
                            $('.modalLoader').removeClass('active');
                            $('.taskModal').modal('hide all');
                            alert('Задача создана!');
                            location.reload();
                        }
                    });
                    return false;
                }
            }).modal('show');
        });
        $('.decline-deal').click(function()
        {
            $('.declineModal .email_work').val(companyEmailWork);
            $('.declineModal').modal('setting', {
                closable  : true,
                onDeny    : function(){
                    return true;
                },
                onApprove : function() {
                    $('.modalLoader').addClass('active');
                    var emailTo =  $('.declineModal .email_work').val();
                    var message = $('.message').val();
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "/local/php_interface/include/ngsec/crm/ajax_crm_deal.php",
                        data: {MODE: 'CANCEL_DEAL', DEAL_ID: '<?=$arParams['DATA']['ID']?>',
                        EMAIL_TO: emailTo, MESSAGE: message}
                    }).done(function( data ) {
                        if (data != null && data.result == 1)
                        {
                            $('.modalLoader').removeClass('active');
                            $('.declineModal').modal('hide all');
                            alert('Регистрация сделки отменена!');
                            location.reload();
                        }
                    });
                    return false;
                }
            }).modal('show');
        });

        $('.confirmSpecLink').click(function()
        {
            $('.confirmSpecModal .email_work').val(companyEmailWork);
            $('.confirmSpecModal').modal('setting', {
                closable  : true,
                onDeny    : function(){
                    return true;
                },
                onApprove : function() {
                    $('.modalLoader').addClass('active');
                    var emailTo =  $('.confirmSpecModal .email_work').val();
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "/local/php_interface/include/ngsec/crm/ajax_crm_deal.php",
                        data: {MODE: 'CONFIRM_SPEC', DEAL_ID: '<?=$arParams['DATA']['ID']?>',
                            CONFIRM: '1', EMAIL_TO: emailTo}
                    }).done(function( data ) {
                        if (data != null && data.result == 1)
                        {
                            $('.modalLoader').removeClass('active');
                            $('.confirmSpecModal').modal('hide all');
                            alert('Спецификация сделки подтверждена!');
                            location.reload();
                        }
                    });
                    return false;
                }
            }).modal('show');
        });
        $('.regDealLink').click(function() {
            $('.regDealModal .email_work').val(companyEmailWork);
            $('.regDealModal').modal('setting', {
                closable: true,
                onDeny: function () {
                    return true;
                },
                onApprove: function () {
                    $('.modalLoader').addClass('active');
                    var emailTo = $('.regDealModal .email_work').val();
                    $.ajax({
                        type: "POST",
                        dataType: 'json',
                        url: "/local/php_interface/include/ngsec/crm/ajax_crm_deal.php",
                        data: {
                            MODE: 'REG_DEAL', DEAL_ID: '<?=$arParams['DATA']['ID']?>', EMAIL_TO: emailTo
                        }
                     }).done(function (data) {
                            if (data != null && data.result == 1) {
                                $('.modalLoader').removeClass('active');
                                $('.regDealModal').modal('hide all');
                                alert('Сделка зарегистрирована!');
                                location.reload();
                            }
                        }
                    );
            return false;
        }
    }).modal('show');
    });
});
</script>
<?
if($GLOBALS['USER']->IsAuthorized() && $arParams["SHOW_SETTINGS"] == true):?>
    <div style="display:none">

    <div id="form_settings_<?=$arParams["FORM_ID"]?>">
        <table width="100%">
            <tr class="section">
                <td colspan="2"><?echo GetMessage("interface_form_tabs")?></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td style="background-image:none" nowrap>
                                <select style="min-width:150px;" name="tabs" size="10" ondblclick="this.form.tab_edit_btn.onclick()" onchange="bxForm_<?=$arParams["FORM_ID"]?>.OnSettingsChangeTab()">
                                </select>
                            </td>
                            <td style="background-image:none">
                                <div style="margin-bottom:5px"><input type="button" name="tab_up_btn" value="<?echo GetMessage("intarface_form_up")?>" title="<?echo GetMessage("intarface_form_up_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabMoveUp()"></div>
                                <div style="margin-bottom:5px"><input type="button" name="tab_down_btn" value="<?echo GetMessage("intarface_form_up_down")?>" title="<?echo GetMessage("intarface_form_down_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabMoveDown()"></div>
                                <div style="margin-bottom:5px"><input type="button" name="tab_add_btn" value="<?echo GetMessage("intarface_form_add")?>" title="<?echo GetMessage("intarface_form_add_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabAdd()"></div>
                                <div style="margin-bottom:5px"><input type="button" name="tab_edit_btn" value="<?echo GetMessage("intarface_form_edit")?>" title="<?echo GetMessage("intarface_form_edit_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabEdit()"></div>
                                <div style="margin-bottom:5px"><input type="button" name="tab_del_btn" value="<?echo GetMessage("intarface_form_del")?>" title="<?echo GetMessage("intarface_form_del_title")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.TabDelete()"></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr class="section">
                <td colspan="2"><?echo GetMessage("intarface_form_fields")?></td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <table>
                        <tr>
                            <td style="background-image:none" nowrap>
                                <div style="margin-bottom:5px"><?echo GetMessage("intarface_form_fields_available")?></div>
                                <select style="min-width:150px;" name="all_fields" multiple size="12" ondblclick="this.form.add_btn.onclick()" onchange="bxForm_<?=$arParams["FORM_ID"]?>.ProcessButtons()">
                                </select>
                            </td>
                            <td style="background-image:none">
                                <div style="margin-bottom:5px"><input type="button" name="add_btn" value="&gt;" title="<?echo GetMessage("intarface_form_add_field")?>" style="width:30px;" disabled onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldsAdd()"></div>
                                <div style="margin-bottom:5px"><input type="button" name="del_btn" value="&lt;" title="<?echo GetMessage("intarface_form_del_field")?>" style="width:30px;" disabled onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldsDelete()"></div>
                            </td>
                            <td style="background-image:none" nowrap>
                                <div style="margin-bottom:5px"><?echo GetMessage("intarface_form_fields_on_tab")?></div>
                                <select style="min-width:150px;" name="fields" multiple size="12" ondblclick="this.form.del_btn.onclick()" onchange="bxForm_<?=$arParams["FORM_ID"]?>.ProcessButtons()">
                                </select>
                            </td>
                            <td style="background-image:none">
                                <div style="margin-bottom:5px"><input type="button" name="up_btn" value="<?echo GetMessage("intarface_form_up")?>" title="<?echo GetMessage("intarface_form_up_title")?>" style="width:80px;" disabled onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldsMoveUp()"></div>
                                <div style="margin-bottom:5px"><input type="button" name="down_btn" value="<?echo GetMessage("intarface_form_up_down")?>" title="<?echo GetMessage("intarface_form_down_title")?>" style="width:80px;" disabled onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldsMoveDown()"></div>
                                <div style="margin-bottom:5px"><input type="button" name="field_add_btn" value="<?echo GetMessage("intarface_form_add")?>" title="<?echo GetMessage("intarface_form_add_sect")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldAdd()"></div>
                                <div style="margin-bottom:5px"><input type="button" name="field_edit_btn" value="<?echo GetMessage("intarface_form_edit")?>" title="<?echo GetMessage("intarface_form_edit_field")?>" style="width:80px;" onclick="bxForm_<?=$arParams["FORM_ID"]?>.FieldEdit()"></div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    </div><?
endif; //$GLOBALS['USER']->IsAuthorized()

$variables = array(
    "mess"=>array(
        "collapseTabs"=>GetMessage("interface_form_close_all"),
        "expandTabs"=>GetMessage("interface_form_show_all"),
        "settingsTitle"=>GetMessage("intarface_form_settings"),
        "settingsSave"=>GetMessage("interface_form_save"),
        "tabSettingsTitle"=>GetMessage("intarface_form_tab"),
        "tabSettingsSave"=>"OK",
        "tabSettingsName"=>GetMessage("intarface_form_tab_name"),
        "tabSettingsCaption"=>GetMessage("intarface_form_tab_title"),
        "fieldSettingsTitle"=>GetMessage("intarface_form_field"),
        "fieldSettingsName"=>GetMessage("intarface_form_field_name"),
        "sectSettingsTitle"=>GetMessage("intarface_form_sect"),
        "sectSettingsName"=>GetMessage("intarface_form_sect_name"),
    ),
    "ajax"=>array(
        "AJAX_ID"=>$arParams["AJAX_ID"],
        "AJAX_OPTION_SHADOW"=>($arParams["AJAX_OPTION_SHADOW"] == "Y"),
    ),
    "settingWndSize"=>CUtil::GetPopupSize("InterfaceFormSettingWnd"),
    "tabSettingWndSize"=>CUtil::GetPopupSize("InterfaceFormTabSettingWnd", array('width'=>400, 'height'=>200)),
    "fieldSettingWndSize"=>CUtil::GetPopupSize("InterfaceFormFieldSettingWnd", array('width'=>400, 'height'=>150)),
    "component_path"=>$component->GetRelativePath(),
    "template_path"=>$this->GetFolder(),
    "sessid"=>bitrix_sessid(),
    "current_url"=>$APPLICATION->GetCurPageParam("", array("bxajaxid", "AJAX_CALL")),
    "GRID_ID"=>$arParams["THEME_GRID_ID"],
);

?><script type="text/javascript">
    var formSettingsDialog<?=$arParams["FORM_ID"]?>;
    bxForm_<?=$arParams["FORM_ID"]?> = new BxCrmInterfaceForm('<?=$arParams["FORM_ID"]?>', <?=CUtil::PhpToJsObject(array_keys($arResult["TABS"]))?>);
    bxForm_<?=$arParams["FORM_ID"]?>.vars = <?=CUtil::PhpToJsObject($variables)?>;<?
	if($arParams["SHOW_SETTINGS"] == true):
		?>bxForm_<?=$arParams["FORM_ID"]?>.oTabsMeta = <?=CUtil::PhpToJsObject($arResult["TABS_META"])?>;
    bxForm_<?=$arParams["FORM_ID"]?>.oFields = <?=CUtil::PhpToJsObject($arResult["AVAILABLE_FIELDS"])?>;<?
	endif
	?>bxForm_<?=$arParams["FORM_ID"]?>.settingsMenu = [];<?
	if($arParams["SHOW_SETTINGS"] == true):
		?>bxForm_<?=$arParams["FORM_ID"]?>.settingsMenu.push({'TEXT': '<?=CUtil::JSEscape(GetMessage("intarface_form_mnu_settings"))?>', 'TITLE': '<?=CUtil::JSEscape(GetMessage("intarface_form_mnu_settings_title"))?>', 'ONCLICK': 'bxForm_<?=$arParams["FORM_ID"]?>.ShowSettings()', 'DEFAULT':true, 'DISABLED':<?=($USER->IsAuthorized()? 'false':'true')?>, 'ICONCLASS':'form-settings'});<?
		if(!empty($arResult["OPTIONS"]["tabs"])):
			if($arResult["OPTIONS"]["settings_disabled"] == "Y"):
				?>bxForm_<?=$arParams["FORM_ID"]?>.settingsMenu.push({'TEXT': '<?=CUtil::JSEscape(GetMessage("intarface_form_mnu_on"))?>', 'TITLE': '<?=CUtil::JSEscape(GetMessage("intarface_form_mnu_on_title"))?>', 'ONCLICK': 'bxForm_<?=$arParams["FORM_ID"]?>.EnableSettings(true)', 'DISABLED':<?=($USER->IsAuthorized()? 'false':'true')?>, 'ICONCLASS':'form-settings-on'});<?
			else:
				?>bxForm_<?=$arParams["FORM_ID"]?>.settingsMenu.push({'TEXT': '<?=CUtil::JSEscape(GetMessage("intarface_form_mnu_off"))?>', 'TITLE': '<?=CUtil::JSEscape(GetMessage("intarface_form_mnu_off_title"))?>', 'ONCLICK': 'bxForm_<?=$arParams["FORM_ID"]?>.EnableSettings(false)', 'DISABLED':<?=($USER->IsAuthorized()? 'false':'true')?>, 'ICONCLASS':'form-settings-off'});<?
			endif;
		endif;
	endif;

	if($arResult["OPTIONS"]["expand_tabs"] == "Y"):
		?>BX.ready(function(){bxForm_<?=$arParams["FORM_ID"]?>.ToggleTabs(true);});<?
    endif;
    ?></script><?

?></div><!-- bx-interface-form --><?
if(!empty($arUserSearchFields)):
    ?><script type="text/javascript">
    BX.ready(
        function()
        {<?
			foreach($arUserSearchFields as &$arField):
				$arUserData = array();
				if(isset($arField['USER'])):
					$nameFormat = isset($arField['NAME_TEMPLATE']) ? $arField['NAME_TEMPLATE'] : '';
					if($nameFormat === '')
						$nameFormat = CSite::GetNameFormat(false);
					$arUserData['id'] = $arField['USER']['ID'];
					$arUserData['name'] = CUser::FormatName($nameFormat, $arField['USER'], true, false);
				endif;
			?>BX.CrmUserSearchField.create(
            '<?=$arField['NAME']?>',
            document.getElementsByName('<?=$arField['SEARCH_INPUT_NAME']?>')[0],
            document.getElementsByName('<?=$arField['INPUT_NAME']?>')[0],
            '<?=$arField['NAME']?>',
            <?= CUtil::PhpToJSObject($arUserData)?>
        );<?
			endforeach;
			unset($arField);
		?>}
    );
</script><?
endif;
?>
<script type="application/javascript">
    function insertAfter(elem, refElem) {
        var parent = refElem.parentNode;
        var next = refElem.nextSibling;
        if (next) {
            return parent.insertBefore(elem, next);
        } else {
            return parent.appendChild(elem);
        }
    }
    function onSubmitClick(e)
    {

    }

    $(document).ready(function() {
        $("#form_<?=$arParams["FORM_ID"]?>").submit(function (e) {
            var ownerId = "<?=$arParams['DATA']['ID']?>";
            var comment = BX('comment');
            var content = comment.value.trim();
            if(!comment) {
                return true;
            }
            else {
                if (content.length <= 0)
                {
                    comment.focus();
                    BX('commentError').style.display = 'block';
                    return false;
                }
                else {
                    return true;
                }
            }
            return true;
        });
    });
</script>
<?if($arParams["DATA"]["ID"] > 0):?>
    <script type="text/javascript">
        BX.ready(
            function()
            {
                var container = BX('workarea-content');

            });
    </script>
<?endif;?>
<script type="text/javascript">
    function checkDuplicate() {
        var ownerId = "<?=$arParams['DATA']['ID']?>";
        var fieldset = $(".check-fieldset");
        var fieldValue =  fieldset.find(".bx-field-value");
        var fieldErrors = fieldset.find(".bx-field-errors");
        fieldValue.text('').append("<span class='crm-block-cont-search-wait'></span>");
        fieldset.show();
        $.ajax({
            type: "POST",
            url: "/local/php_interface/include/ngsec/crm/ajax_crm_duplicate.php",
            data: { ID: ownerId},
            dataType: 'json',
            success: function (data) {
                fieldErrors.text('');
                //Проверка на ошибки
                var errors = [];
                var companyId = $('input[name="COMPANY_ID"]').val();
                if (companyId != null && companyId != "")
                    companyId = parseInt(companyId);
                if (companyId == 0 || companyId == "")
                    errors.push("Не выбран партнёр!");
                var vendorId = $('input[name="UF_CRM_DEAL_VENDOR"]').val();
                if (vendorId != null && vendorId != "")
                    vendorId = parseInt(vendorId);
                if (vendorId == 0 || vendorId == "")
                    errors.push("Не указан вендор!");
                if (errors.length > 0)
                {
                    fieldErrors.append('<div class="title_duplicate">Найдены ошибки:</div>');
                    var txt = "<ul>";
                    errors.forEach(function (element) {
                        txt += "<li>" + element + "</li>";
                    });
                    txt += "</ul>";
                    fieldErrors.append(txt);
                }
                else {
                    fieldValue.text('Ошибок не найдено!').addClass('success');
                }
                if (data.PRODUCTS && Object.keys(data.PRODUCTS).length > 0) {
                    fieldValue.text('').append('<div class="title_duplicate">Найдены совпадения:</div>');
                    var s = '<table class="table table-bordered">' +
                        '<thead><tr>' +
                        '<th>#</th>' +
                        '<th>Название</th>' +
                        '<th>По товарам, %</th>' +
                        '<th>По ИНН заказчика</th>' +
                        '<th>По наименованию заказчика,%</th>' +
                        '</tr></thead>' +
                        '<tbody>';
                    var i = 0;
                    for (var n in data.PRODUCTS) {
                        var item = data.PRODUCTS[n];
                        i = i + 1;
                        s =  s +'<tr>' +
                        '<td>' + i + '</td>' +
                        '<td><a target="_blank" href="' + item.URL + '">' + item.TITLE + '</a></td>' +
                        '<td>' + item.PERCENT +'</td>' +
                        '<td>' + item.INN +'</td>' +
                        '<td>' + item.PERCENT_TITLE +'</td>' +
                        '</tr>';

                    }
                    s = s + '</tbody></table>';
                    fieldValue.append(s);

                }
                $(".crm-block-cont-search-wait").remove();
            },
            error: function (data, status, errorMsg) {
                alert('Error: ' + errorMsg);
            }
        });
    }
</script>

