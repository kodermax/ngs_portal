<?php
/**
 * Created by PhpStorm.
 * User: m.karpychev
 * Date: 19.06.2014
 * Time: 12:03 
 */
if ($arParams['DATA']['COMPANY_ID']) {
    $res = CCrmFieldMulti::GetList(array('ID' => 'asc'),
        array('ENTITY_ID' => 'COMPANY', 'ELEMENT_ID' => $arParams['DATA']['COMPANY_ID']));
    while ($ar = $res->Fetch()) {
        $key = "COMPANY_" . $ar['COMPLEX_ID'];
        $arParams['DATA'][$key] = $ar['VALUE'];
    }
}
$crm = new CCrmNgs();
$result = CCrmNgs::getProductManager($arParams['DATA']['UF_CRM_DEAL_VENDOR']);
$arParams['DATA']['MANAGER_NAME'] = $result['NAME'] . " ".$result['LAST_NAME'];
//заказчик
if($arParams['DATA']['UF_CRM_1381998634'] > 0) {
    $arParams['DATA']['CUSTOMER'] = $crm->getCustomerInfo($arParams['DATA']['UF_CRM_1381998634']);
}
if($arParams['DATA']['UF_CUSTOMER_CONTACT'] > 0) {
    $arParams['DATA']['CUSTOMER_CONTACT'] = $crm->getContactInfo($arParams['DATA']['UF_CUSTOMER_CONTACT']);
}
$arParams['DATA']['PRODUCT_ROWS'] = $crm->getProductRows($arParams['DATA']['ID']);
//echo "<pre>";print_r($arParams);echo "</pre>"
?>