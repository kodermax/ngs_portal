function deal_delete(title, message, btnTitle, path)
{
	var d;
	d = new BX.CDialog({
		title: title,
		head: '',
		content: message,
		resizable: false,
		draggable: true,
		height: 70,
		width: 300
	});
	
	var _BTN = [	
		{
			title: btnTitle,
			id: 'crmOk',
			'action': function () 
			{
				window.location.href = path;
				BX.WindowManager.Get().Close();
			}
		},
		BX.CDialog.btnCancel
	];	
	d.ClearButtons();
	d.SetButtons(_BTN);
	d.Show();
}
function addPPDS(guid){
    $.ajax({
        cache:false,
        url: "/app/api/v1/deals/invoices/add_ppds",
        type:'POST',
        dataType: 'json',
        data: JSON.stringify({"GUID": guid})
    }).done(function(data) {
        if (data.status){
            $('.invoicesModal').modal('hide');
            $('.invoicesModal').modal('show');
        }
    });
}
function showInvoices(Id){
    var currency = $('.currency_id').val();
    var dealId = Id;
    var invoiceModal = $('.invoicesModal').modal('setting', {
        closable  : true,
        offset: 0,
        onShow: function(){
            $.ajax({
                cache:false,
                url: "/app/api/v1/deals/invoices/" + Id,
                dataType: 'json'

            }).done(function(data) {
                if (data && data.length > 0) {
                    $('.invoicesModal .content .ui.form').html('<table class="ui table small compact"><tbody></tbody></table>');
                    $('.invoicesModal .ui.active.dimmer').removeClass('active');
                    $.each(data, function (index, value) {
                        var d = new Date(value.DATE);
                        var ruDate = new Date(d.getTime());
                        var enDate = new Date(d.getTime());
                        ruDate = ruDate.setDate(d.getDate() + 3);
                        enDate = enDate.setDate(d.getDate() + 10);
                        if (currency === 'RUB') {
                            var payDate = new Date(ruDate);
                        }
                        else {
                            var payDate = new Date(enDate);
                        }
                        if (value.MONEY == 'N')
                            var s = '<tr class="warning" data-content="Для заказа № ' + value.NUMBER.trim() + '  не создан документ Планируемое поступление денежных средств">';
                        else
                            var s = '<tr>';
                        s += '<td><div class="ui toggle checkbox">';
                        if (value.MONEY === 'N') {
                            s += '<input class="invoice" name="invoices[]" disabled="disabled" type="checkbox" value="' + value.GUID + '">';
                            s = s + '<label><i class="attention icon"></i> Заказ №' + value.NUMBER.trim() + ' от ' + d.toLocaleDateString() + '</label>';
                        }
                        else {
                            s += '<input class="invoice" name="invoices[]" checked="checked" type="checkbox" value="' + value.GUID + '">';
                            s += '<label><i class="checkmark icon"></i> Заказ  №' + value.NUMBER.trim() + ' от ' + d.toLocaleDateString() + '</label>';
                        }
                        s += '</div></td><td> <div style="width:130px;" class="ui mini icon input">' +
                        '<input name="guid_' + value.GUID + '" id="date_' + value.GUID + '" type="text" value="' + d.toLocaleDateString() + '" placeholder="Начало периода">' +
                        '<i onclick="BX.calendar({node:this, field:\'date_' + value.GUID + '\', form: \'\', bTime: false, currentTime: \'<?=time()?>\', bHideTime: false});" class="calendar icon"></i>' +
                        '</div></td>';
                        if (value.MONEY === 'N') {
                            s += '<td><button type="button" onclick="addPPDS(\'' + value.GUID + '\')" class="ui primary tiny button createDoc" style="float:right;">ППДС</button></td>';
                        }
                        '</tr>';
                        $('.invoicesModal .content .ui.table > tbody:last').append(s);
                    })
                    var btn = ' <div class="ui buttons" style="float: right">' +

                        '<div class="ui positive button">Сформировать</div>' +
                        '</div>';
                    $('.invoicesModal .content .ui.form').append(btn);
                    $('.area-comment').show();
                    $('.invoicesModal').modal('refresh');
                    $('.ui .checkbox').checkbox();
                    $('.ui.cancel.button').click(function () {
                        $('.invoicesModal').modal('hide');
                    });
                    $('.ui.positive.button').click(function () {
                        var postData = [];
                        $(".invoice:checked").each(function () {
                            var value = $(this).val();
                            var date = $('input[name=guid_' + value + ']').val();
                            var parts = date.split('.');
                            var date = new Date(parts[2] + '/' + parts[1] + '/' + parts[0]);
                            postData.push(
                                {
                                    id: value,
                                    date: date.getFullYear() + (date.getMonth() < 9 ? '0' : '') + (date.getMonth() + 1) + (date.getDate() < 10 ? '0' : '') + date.getDate()

                                }
                            );
                        });
                        if (postData.length <= 0) {
                            alert('Ничего не выбрано!');
                            return;
                        }
                        $('.invoicesModal .ui.dimmer').addClass('active');
                        $.ajax({
                            type: "POST",
                            url: "/app/api/v1/deals/invoices/make/" + $('.element_id').val(),
                            dataType: "json",
                            method: 'POST',
                            contentType: 'application/json',
                            data: JSON.stringify({orders: postData, comment: $( "#comment" ).prop( "checked") === true ? $('.invoice_comment').text(): ''})
                        }).done(function (response) {
                            if (response.result) {
                                window.location = "/app/api/v1/deals/invoices/make/" + response.result;

                            }

                        }).fail(function (jqXHR, textStatus) {
                            alert(textStatus);
                        });
                    });

                    $('.warning').popup();
                } else {
                    $('.invoicesModal .ui.active.dimmer').removeClass('active');
                    $('.invoicesModal .content .ui.form').html('Заказы по сделке не найдены. <div class="ui button" onclick="createOrders(' + Id + ')">Создать заказы</div>');
                }
            });
        },
        onDeny    : function(){
            return true;
        },
        onApprove : function() {
            return false;
        }
    }).modal('show');

}
function createOrders(Id){
    $('.invoicesModal .ui.dimmer').addClass('active');
    $.ajax({
        cache:false,
        url: "/app/api/v1/deals/createOrders/" + Id,
        dataType:'json'


    }).done(function(data) {
        if (data.status) {
            $('.invoicesModal .ui.active.dimmer').removeClass('active');
            $('.invoicesModal').modal('hide');
            showInvoices(Id);
        }
        else {
            var errors = [];
            $.each(data.errors, function(idx2,val2) {
                var str = val2;
                errors.push(str);
            });
            alert(errors.join(", "));
            $('.invoicesModal').modal('hide');
        }
    });
}
function showAnalyze(Id){
    $('.analyzeModal').modal('setting', {
        closable  : true,
        offset: 0,
        onShow: function(){
            $.ajax({
                cache:false,
                url: "/app/api/v1/deals/analyze/" + Id

            }).done(function(data) {
                $('.analyzeModal .content').html(data);
                $('.analyzeModal').modal('refresh');
                $('.ui.accordion').accordion('open',function(){
                    $('.analyzeModal').modal('refresh').modal('can fit').modal('hide dimmer');
                });
            });
        },
        onDeny    : function(){
            return true;
        },
        onApprove : function() {
            return false;
        }
    }).modal('show');

}