<?php

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
global $APPLICATION;
if (!empty($arResult['BUTTONS']))
{
	$type = $arParams['TYPE'];
	$APPLICATION->IncludeComponent(
		'bitrix:crm.interface.toolbar',
		$type === 'list' ?  '' : 'type2',
		array(
			'TOOLBAR_ID' => $arResult['TOOLBAR_ID'],
			'BUTTONS' => $arResult['BUTTONS']
		),
		$component,
		array('HIDE_ICONS' => 'Y')
	);
}

if(isset($arResult['SONET_SUBSCRIBE']) && is_array($arResult['SONET_SUBSCRIBE'])):
	$subscribe = $arResult['SONET_SUBSCRIBE'];
?><script type="text/javascript">
BX.ready(
	function()
	{
		BX.CrmSonetSubscription.create(
			"<?=CUtil::JSEscape($subscribe['ID'])?>",
			{
				"entityType": "<?=CCrmOwnerType::DealName?>",
				"serviceUrl": "<?=CUtil::JSEscape($subscribe['SERVICE_URL'])?>",
				"actionName": "<?=CUtil::JSEscape($subscribe['ACTION_NAME'])?>"
			}
		);
	}
);
</script><?
endif;?>
<script type="text/javascript" src="/shared/lib/jquery/jquery.fileDownload.js"></script>
<input type="hidden" name="ELEMENT_ID" class="element_id" value="<?=$arParams['ELEMENT_ID']?>"/>
<input type="hidden" name="CURRENCY_ID" class="currency_id" value="<?=$arParams['CURRENCY_ID']?>"/>

<div class="ui modal scrolling analyzeModal">
    <i class="close icon"></i>
    <div class="header">
        Анализ сделки № <?=$arParams['ELEMENT_ID']?>
    </div>
    <div class="content">
        <div class="ui active dimmer">
            <div class="ui text loader">Загрузка</div>
        </div>
    </div>

</div>
<div class="ui modal scrolling invoicesModal">
    <i class="close icon"></i>
    <div class="header">
      Формирование счетов по сделке № <?=$arParams['ELEMENT_ID']?>
    </div>
    <div class="content">
        <div class="ui active dimmer">
            <div class="ui text loader">Загрузка</div>
        </div>
        <form>
            <div class="ui form">

            </div>
            <div style="display:none;" class="area-comment">
                <input style="float: left" type="checkbox"  <?if(strlen($arParams['INVOICE_COMMENT']) > 0):?>checked<?endif;?> id="comment"/>Комментарий: <div class="invoice_comment"><?=$arParams['INVOICE_COMMENT']?></div>
            </div>
        </form>
    </div>
</div>

<div class="ui modal scrolling docModal">
    <i class="close icon"></i>
    <div class="header">
       Заголовок
    </div>
    <div class="content">
        <form>
            <div class="ui form">
                <div class="ui toggle checkbox">
                    <input type="checkbox" name="public">
                    <label>Полная оплата</label>
                </div>
            </div>
            <table class="ui table">
                <thead>
                    <tr>
                        <th>Дата оплаты</th>
                        <th>Сумма</th>
                        <th>Валюта</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <div class="ui mini icon input">
                                <input name="date_fld" id="date_start" type="text" value="" placeholder="Начало периода">
                                <i class="calendar icon"
                                   onclick="BX.calendar({node:this, field:'date_start', form: '', bTime: false, currentTime: '1424964037', bHideTime: false});"></i>
                            </div>
                        </td>
                        <td><input type="text" name="sum"/></td>
                        <td><select name="currency">
                                <option value="RUB">руб.</option>
                                <option value="USD">$</option>
                                <option value="EUR">евр.</option>
                            </select></td>
                    </tr>
                </tbody>
            </table>
            <div class="ui buttons" style="float: right">
                <div class="ui  button" onclick="$('.invoicesModal').modal('show');">Отмена</div>
                <div class="or"></div>
                <div class="ui positive button">Сформировать</div>
            </div>
        </form>
    </div>
</div>