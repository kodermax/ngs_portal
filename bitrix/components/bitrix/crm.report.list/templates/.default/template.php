<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
/** CMain $APPLICATION */
global $APPLICATION;
define('CRM_REPORT_UPDATE_14_5_2_MESSAGE', 'Y');
$APPLICATION->ShowViewContent('REPORT_UPDATE_14_5_2_MESSAGE');
?>
<h3><?= htmlspecialcharsbx(GetMessage('CRM_REPORT_LIST_DEAL'))?></h3>
<? $APPLICATION->IncludeComponent(
	'bitrix:report.list',
	'',
	array(
		'PATH_TO_REPORT_LIST' => $arParams['PATH_TO_REPORT_REPORT'],
		'PATH_TO_REPORT_CONSTRUCT' => $arParams['PATH_TO_REPORT_CONSTRUCT'],
		'PATH_TO_REPORT_VIEW' => $arParams['PATH_TO_REPORT_VIEW'],
		'REPORT_HELPER_CLASS' => 'CCrmReportHelper'
	),
	false
);?>
<h3><?= htmlspecialcharsbx(GetMessage('CRM_REPORT_LIST_PRODUCT'))?></h3>
<?$APPLICATION->IncludeComponent(
	'bitrix:report.list',
	'',
	array(
		'PATH_TO_REPORT_LIST' => $arParams['PATH_TO_REPORT_REPORT'],
		'PATH_TO_REPORT_CONSTRUCT' => $arParams['PATH_TO_REPORT_CONSTRUCT'],
		'PATH_TO_REPORT_VIEW' => $arParams['PATH_TO_REPORT_VIEW'],
		'REPORT_HELPER_CLASS' => 'CCrmProductReportHelper'
	),
	false
);?>
<h3><?= htmlspecialcharsbx(GetMessage('CRM_REPORT_LIST_LEAD'))?></h3>
<? $APPLICATION->IncludeComponent(
	'bitrix:report.list',
	'',
	array(
		'PATH_TO_REPORT_LIST' => $arParams['PATH_TO_REPORT_REPORT'],
		'PATH_TO_REPORT_CONSTRUCT' => $arParams['PATH_TO_REPORT_CONSTRUCT'],
		'PATH_TO_REPORT_VIEW' => $arParams['PATH_TO_REPORT_VIEW'],
		'REPORT_HELPER_CLASS' => 'CCrmLeadReportHelper'
	),
	false
);?>
<h3><?= htmlspecialcharsbx(GetMessage('CRM_REPORT_LIST_INVOICE'))?></h3>
<? $APPLICATION->IncludeComponent(
	'bitrix:report.list',
	'',
	array(
		'PATH_TO_REPORT_LIST' => $arParams['PATH_TO_REPORT_REPORT'],
		'PATH_TO_REPORT_CONSTRUCT' => $arParams['PATH_TO_REPORT_CONSTRUCT'],
		'PATH_TO_REPORT_VIEW' => $arParams['PATH_TO_REPORT_VIEW'],
		'REPORT_HELPER_CLASS' => 'CCrmInvoiceReportHelper'
	),
	false
);?>
<h3><?= htmlspecialcharsbx(GetMessage('CRM_REPORT_LIST_ACTIVITY'))?></h3>
<? $APPLICATION->IncludeComponent(
	'bitrix:report.list',
	'',
	array(
		'PATH_TO_REPORT_LIST' => $arParams['PATH_TO_REPORT_REPORT'],
		'PATH_TO_REPORT_CONSTRUCT' => $arParams['PATH_TO_REPORT_CONSTRUCT'],
		'PATH_TO_REPORT_VIEW' => $arParams['PATH_TO_REPORT_VIEW'],
		'REPORT_HELPER_CLASS' => 'CCrmActivityReportHelper'
	),
	false
);?>
<h3>Отчеты 1С</h3>
<div class="reports-list-wrap">
    <div class="reports-list">
        <div class="reports-list-left-corner"></div>
        <div class="reports-list-right-corner"></div>
        <style>
            .reports-list-table th:hover {
                cursor: default;
            }
        </style>
        <table cellspacing="0" id="reports_list_table_crm_activity" class="reports-list-table">
            <tbody><tr>
                <th colspan="2" class="reports-first-column reports-head-cell-top">
                    <div class="reports-head-cell"><!--<span class="reports-table-arrow"></span>--><span class="reports-head-cell-title">Название отчета</span></div>
                </th>
                <th class="reports-last-column">
                    <div class="reports-head-cell"><!--<span class="reports-table-arrow"></span>--><span class="reports-head-cell-title">Дата создания</span></div>
                </th>
            </tr>
            <tr class="reports-list-item">
                <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports/external/plans_facts/">Планы и продажи</a></td>
                <td class="reports-list-menu">
                    <a class="reports-menu-button" href="#" id="rmb-136d1"><i class="reports-menu-button-icon"></i></a>
                </td>
                <td class="reports-date-column reports-last-column">26.02.2015</td>
            </tr>
            <tr class="reports-list-item">
                <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports/external/sales/">Анализ заказов по сделкам</a></td>
                <td class="reports-list-menu">
                    <a class="reports-menu-button" href="#" id="rmb-136d1"><i class="reports-menu-button-icon"></i></a>
                </td>
                <td class="reports-date-column reports-last-column">26.02.2015</td>
            </tr>
            <tr class="reports-list-item">
                <td class="reports-first-column"><a class="reports-title-link" href="/crm/reports/external/interests/">Активности по партнёрам</a></td>
                <td class="reports-list-menu">
                    <a class="reports-menu-button" href="#" id="rmb-136d1"><i class="reports-menu-button-icon"></i></a>
                </td>
                <td class="reports-date-column reports-last-column">26.02.2015</td>
            </tr>
            </tbody></table>
    </div>
</div>