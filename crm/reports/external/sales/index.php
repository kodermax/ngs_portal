<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Анализ заказов по сделкам");?>
<?
$month = date('m');
if ($month <= 6) {
    $date = '01.01.' . date('Y');
} else {
    $date = '01.07' . date('Y');
}

$list = CUser::GetList($by="LAST_NAME", $order="ASC",array('GROUPS_ID' => array(17)),array('FIELDS'=> array('NAME','LAST_NAME','XML_ID')));
while($row = $list->GetNext()){
    $arUsers[] = array(
        'Id' => $row['XML_ID'],
        'Title' => $row['LAST_NAME'].' '.$row['NAME']
    );
}
?>
    <style>
        .content_1c table {
            table-layout: fixed;
            padding: 0px;
            padding-left: 2px;
            vertical-align: bottom;
            border-collapse: collapse;
            width: 100%;
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
        }

        .content_1c td {
            padding: 0px;
            padding-left: 2px;
            overflow: hidden;
        }
    </style>
    <div class="ui form" style="width:1150px;">
        <div class="three fields">
            <div class="ui field" style="width:160px;">
                <div class="ui mini icon input">
                    <input name="date_fld" id="date_start" type="text" value="" placeholder="Начало периода">
                    <i class="calendar icon"
                       onclick="BX.calendar({node:this, field:'date_start', form: '', bTime: false, currentTime: '1424964037', bHideTime: false});"></i>
                </div>
            </div>
            <div class="ui field" style="width:500px;">
                <div class="ui label" style="font-size: small;width:80px;">Оплата</div>
                <div class="pay_full ui checkbox">
                    <input type="checkbox" name="pay_full">
                    <label  style="font-size: small;">Полностью</label>
                </div>
                <div class="pay_part ui checkbox">
                    <input type="checkbox" name="pay_part">
                    <label  style="font-size: small;">Частично</label>
                </div>
                <div class="pay_no ui checkbox">
                    <input  type="checkbox" name="pay_no">
                    <label style="font-size: small;">Не оплачено</label>
                </div>
            </div>
        </div>
        <div class="three fields">
            <div class="ui field" style="width:160px;">
                <div class="ui mini icon input">
                    <input name="date_fld" id="date_end" type="text" value="" placeholder="Конец периода">
                    <i class="calendar icon"
                       onclick="BX.calendar({node:this, field:'date_end', form: '', bTime: false, currentTime: '1424964037', bHideTime: false});"></i>
                </div>
            </div>
            <div class="ui field" style="width:500px;">
                <div class="ui label" style="font-size: small;width:80px;">Отгрузка</div>
                <div class="del_full ui checkbox">
                    <input type="checkbox" name="del_full">
                    <label  style="font-size: small;">Полностью</label>
                </div>
                <div class="del_part ui checkbox">
                    <input  type="checkbox" name="del_part">
                    <label  style="font-size: small;">Частично</label>
                </div>
                <div class="del_no ui checkbox">
                    <input type="checkbox" name="del_no">
                    <label style="font-size: small;">Не отгружено</label>
                </div>
            </div>
        </div>
                <select id="manager" name="manager" class="ui mini search dropdown">
                    <option value="0">Выберите менеджера</option>
                    <?foreach($arUsers as $arUser):?>
                        <option value="<?=$arUser['Id']?>"><?=$arUser['Title']?></option>
                    <?endforeach;?>
                </select>
            <button class="tiny ui primary button" onclick="showReport()">Сформировать отчёт</button>
    </div>
    <div class="content content_1c" style="min-height:300px;">

    </div>
    <script>
        $(function(){
            $(".ui.dropdown").dropdown();
            $('.ui.checkbox').checkbox();
        });

        function showReport() {
            var startDate = $('#date_start').val();
            var endDate = $('#date_end').val();
            var user = $('.ui.dropdown').dropdown('get value');
            var pay_full = $(".pay_full").checkbox('is checked');
            var pay_part = $(".pay_part").checkbox('is checked');
            var pay_no = $(".pay_no").checkbox('is checked');
            var del_full = $(".del_full").checkbox('is checked');
            var del_part = $(".del_part").checkbox('is checked');
            var del_no = $(".del_no").checkbox('is checked');

            $('.content').html('Загрузка данных....');
            $.ajax({
                cache: false,
                type: "POST",
                url: "/app/api/v1/reports/sales",
                data: {
                    startDate: startDate,
                    endDate: endDate,
                    user: user,
                    pay_full: pay_full,
                    pay_part: pay_part,
                    pay_no: pay_no,
                    del_full: del_full,
                    del_part: del_part,
                    del_no: del_no
                }

            }).done(function (data) {
                $('.content').html(data);
            });
        }
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>