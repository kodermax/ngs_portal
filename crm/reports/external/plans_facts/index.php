<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("План и продажи (Валюта: USD)");?>
<?
$month = date('m');
if ($month <= 6) {
    $date = '01.01.'.date('Y');
}
else {
    $date = '01.07'.date('Y');
}
?>
<style>
    .content_1c table {table-layout: fixed; padding: 0px; padding-left: 2px; vertical-align:bottom; border-collapse:collapse;width: 100%; font-family: Arial; font-size: 8pt; font-style: normal; }
    .content_1c td { padding: 0px; padding-left: 2px; overflow:hidden; }

</style>
<div class="ui form">
    <div class="inline field">
        <label>Дата начала периода</label>
        <div class="ui small icon input">
            <input name="date_fld" id="date_fld" type="text" value="<?=$date;?>">
            <i class="calendar icon" onclick="BX.calendar({node:this, field:'date_fld', form: '', bTime: false, currentTime: '1424964037', bHideTime: false});"></i>
        </div>
        <button class="tiny ui primary button" onclick="showReport()">Сформировать отчёт</button>
    </div>
</div>
<div class="content content_1c">

</div>
<script>
    function showReport() {
        var inputDate = $('#date_fld').val();
        $('.content').html('Загрузка данных....');
        $.ajax({
            cache:false,
            type: "POST",
            url: "/app/api/v1/reports/plans_facts",
            data: {date: inputDate}

        }).done(function(data) {
            $('.content').html(data);
        });
    }
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>