<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Активности по партнёрам");?>
<?

\Bitrix\Main\loader::includeModule('crm');

$month = date('m');
if ($month <= 6) {
    $date = '01.01.' . date('Y');
} else {
    $date = '01.07' . date('Y');
}
$arUsers = array();
$list = CUser::GetList($by="LAST_NAME", $order="ASC",array('GROUPS_ID' => array(17)),array('FIELDS'=> array('NAME','LAST_NAME','XML_ID')));
while($row = $list->GetNext()){
    $arUsers[] = array(
        'Id' => $row['XML_ID'],
        'Title' => $row['LAST_NAME'].' '.$row['NAME']
    );
}
$arVendors = array();
$list = CCrmCompany::GetList(array('TITLE' => 'ASC'),array('CHECK_PERMISSIONS' => 'N','COMPANY_TYPE' => 1),array('ID','ORIGIN_ID','TITLE'));
while($row = $list->GetNext()){
    if (strlen($row['ORIGIN_ID']) > 0) {
        $arVendors[$row['ORIGIN_ID']] = $row['TITLE'];
    }
}
$arPartners = array();
$list = CCrmCompany::GetList(array('TITLE' => 'ASC'),array('CHECK_PERMISSIONS' => 'N','COMPANY_TYPE' => 'PARTNER'), array('ID','ORIGIN_ID','TITLE'));
while($row = $list->GetNext()){
    $arPartners[] = array(
        'GUID' => $row['ORIGIN_ID'],
        'TITLE' => $row['TITLE']
    );
}
?>
    <style>
        .content_1c table {
            table-layout: fixed;
            padding: 0px;
            padding-left: 2px;
            vertical-align: bottom;
            border-collapse: collapse;
            width: 100%;
            font-family: Arial;
            font-size: 8pt;
            font-style: normal;
        }

        .content_1c td {
            padding: 0px;
            padding-left: 2px;
            overflow: hidden;
        }
    </style>
    <div class="ui form" style="width:1150px;">
        <div class="three fields">
            <div class="ui field" style="width:160px;">
                <div class="ui mini icon input">
                    <input name="date_fld" id="date" type="text" value="<?=date('d.m.Y')?>" placeholder="Начало периода">
                    <i class="calendar icon"
                       onclick="BX.calendar({node:this, field:'date', form: '', bTime: false, currentTime: '<?=time()?>', bHideTime: false});"></i>
                </div>
            </div>
            <select id="manager" name="manager" class="ui mini search dropdown">
            <option value="0">Выберите менеджера</option>
            <?foreach($arUsers as $arUser):?>
                <option value="<?=$arUser['Id']?>"><?=$arUser['Title']?></option>
            <?endforeach;?>
            </select>

            <select id="vendor" name="vendor" class="ui mini search dropdown">
            <option value="0">Выберите вендора</option>
            <?foreach($arVendors as $key => $item):?>
                <option value="<?=$key?>"><?=$item?></option>
            <?endforeach;?>
            </select>

            <select id="partner" name="partner" class="ui mini search dropdown">
            <option value="0">Выберите партнёра</option>
            <?foreach($arPartners as $arItem):?>
                <option value="<?=$arItem['GUID']?>"><?=$arItem['TITLE']?></option>
            <?endforeach;?>
            </select>
        </div>

        <button class="tiny ui primary button" onclick="showReport()">Сформировать отчёт</button>
    </div>
<p></p>
	<div class="content content_1c" style="min-height:300px;">

    </div>
    <script>
        $(function(){
                $(".ui.dropdown").dropdown();
            }
        );

        function showInterest(Id){
            $('.ui.modal.interest').modal({
                onShow: function(){
                    $.ajax({
                        cache: false,
                        type: "GET",
                        dataType: "json",
                        url: "/app/api/events/" + Id
                    }).done(function (data) {
                        if (data.Positive === 'Y') {
                            $("#act_type").html('Положительная');
                            $("#act_type").removeClass('red');
                            $("#act_type").addClass('green');
                        }
                        else {
                            $("#act_type").html('Отрицательная');
                            $("#act_type").removeClass('green');
                            $("#act_type").addClass('red');
                        }
                        if(data.Deal.Title && data.Deal.Title.length > 0) {
                            $('#act_deal').html("<a target='_blank' href='/crm/deal/show/" + data.Deal.Id + "/'>" + data.Deal.Title + "</a>");
                            $("#show_deal").show();
                        }
                        else {
                            $("#show_deal").hide();
                        }
                        $('#act_date').html(data.Date);
                        $('#act_partner').html(data.Partner);
                        $('#act_vendor').html(data.Vendor);
                        $('#act_comment').html(data.Comment);
                        $('#act_author').html(data.Author);
                    });
                }
            }).modal('show');
            ;
        }

        function showReport() {
            var date = $('#date').val();
            var user = $('#manager').val();
            var vendor = $('#vendor').val();
            var partner = $('#partner').val();


            $('.content').html('Загрузка данных....');
            $.ajax({
                cache: false,
                type: "POST",
                url: "/app/api/v1/reports/analyze_partner",
                data: {
                    date: date,
                    user: user,
                    vendor: vendor,
                    partner: partner
                }

            }).done(function (data) {
                $('.content').html(data);
            });
        }
    </script>
    <div class="ui modal interest">
        <i class="close icon"></i>
        <div class="header" style="padding: 1rem;">
			<div id="act_type" class="ui top right attached label"></div>
            Информация об активности
        </div>
        <div class="data">
            <div class="ui form segment">

                <div class="three fields">
                    <div class="field">
                        <label>Дата</label>
                        <span id="act_date"></span>
                    </div>
                    <div class="field">
                        <label>Партнер</label>
                        <span id="act_partner"></span>
                    </div>
                    <div class="field">
                        <label>Вендор</label>
                        <span id="act_vendor"></span>
                    </div>
                </div>
                <div class="field">
                    <label>Коментарий</label>
                    <span id="act_comment"></span>
                </div>
            </div>
        </div>
        <div class="actions" style="padding:0; padding-left:1em;">
			<div>
                <table width=100%><tr><td style="text-align:left">
			<label>Ответственный:</label>
                <span id="act_author"></span>
			</td>
                <td id="show_deal">
                    <label>Сделка:</label>
                    <span id="act_deal"></span>
                </td>
				<td style="text-align:right"><div class="ui button">OK</div></td></tr></table>
            </div>
        </div>
    </div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>