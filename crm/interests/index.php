<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");?>

<link rel="stylesheet" type="text/css" href="/shared/css/angucomplete/angucomplete-alt.css"/>
<link rel="stylesheet" type="text/css" href="/shared/lib/kendo-ui/styles/kendo.common.core.min.css" />
<link rel="stylesheet" type="text/css" href="/shared/lib/kendo-ui/styles/kendo.default.min.css"/>
<link rel="stylesheet" type="text/css" href="/shared/lib/kendo-ui/styles/kendo.silver.min.css"/>
<script type="text/javascript" src="/shared/lib/angular-1.3/angular.js"></script>
<script type="text/javascript" src="/shared/lib/angular-1.3/angular-ui-router.min.js"></script>
<script type="text/javascript" src="/shared/lib/angular-1.3/lodash.min.js"></script>
<script type="text/javascript" src="/shared/lib/angular-1.3/restangular.js"></script>
<script type="text/javascript" src="/shared/lib/angular-1.3/angular-sanitize.min.js"></script>
<script type="text/javascript" src="/shared/lib/angular-1.3/angucomplete-alt.min.js"></script>
<script type="text/javascript" src="/shared/lib/angular-semantic-ui/angular-semantic-ui.js"></script>
<script type="text/javascript" src="/shared/lib/kendo-ui/js/kendo.custom.min.js"></script>
<script type="text/javascript" src="/shared/lib/kendo-ui/js/kendo.ui.core.min.js"></script>
<script type="text/javascript" src="/shared/lib/pagination/dirPagination.js"></script>
<script type="text/javascript" src="app/app.js?v2"></script>
<script type="text/javascript" src="app/menu/menu.js"></script>
<script type="text/javascript" src="app/index/index.js"></script>
<script type="text/javascript" src="app/interests/interests.js"></script>
<script type="text/javascript" src="app/interests/interests-directives.js"></script>
<script type="text/javascript" src="app/directives/directives.js"></script>
    <script>
        angular.element(document).ready(function() {
            angular.bootstrap(document, ['interestApp']);
        });
    </script>
<div style="margin-bottom: 300px">
<top-menu></top-menu>
<div ui-view="main"></div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>