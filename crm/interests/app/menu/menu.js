var app = angular.module('interestApp.menu',['ui.router']);

app.directive('topMenu',function()
{
    return {
        restrict: 'E',
        templateUrl: 'app/menu/menu.html',
        controller: function($location, $state, $scope)
        {
            $scope.filter = 'active';
            this.action = function(path)
            {
                $state.go(path);
            };
            this.isSet = function(item)
            {
                return $state.includes(item);
            };
            this.setFilter = function(value)
            {
                $scope.$broadcast('EventSetFilter', value);
                $scope.filter = value;
            };

            this.isSetFilter = function(value)
            {
                return $scope.filter === value;
            }
        },
        controllerAs: 'menu'
    }
});
