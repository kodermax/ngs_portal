var interestApp;
interestApp = angular.module('interestApp', [
    'restangular',
    'ui.router',
    'interestApp.directives',
    'interestApp.interests',
    'interestApp.interests.directives',
    'interestApp.index',
    'interestApp.menu'
]).config( function ( $stateProvider, $urlRouterProvider ) {
    $urlRouterProvider.otherwise('/interests');
}).config(function (RestangularProvider) {
    RestangularProvider.setBaseUrl('app/api/');
    RestangularProvider.setFullResponse(true);
});

