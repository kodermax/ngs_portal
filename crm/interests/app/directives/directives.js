var app = angular.module('interestApp.directives', [
    'angucomplete-alt',
    'restangular',
    'angularify.semantic.dropdown']);
app.directive('vendorSelector', function() {
    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            value: '@'
        },
        require: 'ngModel',
        templateUrl: 'app/directives/templates/vendor_selector.html',
        link: function (scope, element, attrs, ngModel){
            scope.$watch('vendorModel', function() {
                ngModel.$setViewValue(scope.vendorModel);
            });
        },
        controller: function ($scope, Restangular) {
            $scope.vendorModel = [];
            Restangular.all('companies').getList({type: 1})
                .then(function(data) {
                    // returns a list of users
                    $scope.vendors = data.data; // first Restangular obj in list: { id: 123 }
                    data.data.unshift({Id:0, Title: 'Выберите вендора'});

                    if ($scope.value > 0) {
                        for (var i = 0; i < $scope.vendors.length; i++) {
                            if ($scope.vendors[i].Id == $scope.value)
                            {
                                $scope.currentVendor = $scope.vendors[i];
                                break;
                            }
                        }
                    }
                    else {
                        $scope.currentVendor = data.data[0];
                    }
                })


        }
    }
});
app.directive('contactSelector',function(){
  return {
      restrict: 'E',
      scope: {
          ngModel: '=',
          value: '@'
      },
      require: 'ngModel',
      templateUrl: 'app/directives/templates/contact_selector.html',

      link: function (scope, element, attrs, ngModel){
          scope.$watch('contactModel', function() {
              if (scope.contactModel.originalObject !== undefined)
                  ngModel.$setViewValue(scope.contactModel.originalObject.Id);
          });
      },
      controller: function ($scope, Restangular) {
          $scope.contactModel = [];
      }
  }
});
app.directive('userSelector', function() {
    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            value: '@'
        },
        require: 'ngModel',
        templateUrl: 'app/directives/templates/user_selector.html',
        link: function (scope, element, attrs, ngModel){
            scope.$watch('userModel', function() {
                ngModel.$setViewValue(scope.userModel);
            });
        },
        controller: function ($scope, Restangular) {

            Restangular.all('employees').getList()  // GET: /users
                .then(function(users) {
                    // returns a list of users
                    $scope.users = users.data; // first Restangular obj in list: { id: 123 }
                    users.data.unshift({Id:0, Name: 'Выберите сотрудника'});
                    $scope.currentUser = users.data[0];
                    if ($scope.value > 0) {
                        for (var i = 0; i < $scope.users.length; i++) {
                            if ($scope.users[i].Id == $scope.value)
                            {
                                $scope.currentUser = $scope.users[i];
                                break;
                            }
                        }
                        }
                })

        }
    }
});
app.directive('companySelector', function(){
    return {
        restrict: 'E',
        scope: {
            ngModel: '=',
            value: '@'
        },
        require: 'ngModel',
        templateUrl: 'app/directives/templates/company_selector.html',
        link: function (scope, element, attrs, ngModel){
            scope.$watch('companyModel', function() {
                if (scope.companyModel.originalObject !== undefined)
                  ngModel.$setViewValue(scope.companyModel.originalObject.Id);
            });
        },
        controller: function ($scope) {
            $scope.companyModel = [];
        }
    }
});