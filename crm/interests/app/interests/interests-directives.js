var app = angular.module('interestApp.interests.directives',
    [
        'angularify.semantic.dropdown',
        'angularify.semantic.checkbox',
        'kendo.directives',
        'angularUtils.directives.dirPagination'
    ]
);

app.directive('report', function(){
    return {
        restrict: 'E',
        templateUrl: 'app/interests/templates/report.html',
        controller:  function ($scope, $location, $state, Restangular)
        {
            $scope.getData = function() {
                $scope.$broadcast('loader', true);
                var rest = Restangular.all('InterestsReport');
                rest.post($scope.filter).then(function(response){
                    $scope.areas = response.data.areas;
                    $scope.cells = response.data.data;
                    $scope.vendors = response.data.vendors;
                    $scope.companies = response.data.companies;
                    $scope.totalCompanies = response.data.totalCompanies;
                    if(!$scope.currentArea) {
                        $scope.currentArea = response.data.areas[0];
                        $scope.filter['areaId'] = $scope.currentArea.Id;
                    }
                    Restangular.one('Companies').get({type:'PARTNER',areaId:$scope.filter['areaId']}).then(
                        function(response) {
                            $scope.partners = response.data;
                        }
                    );
                    $scope.$broadcast('loader', false);
                });
            };

            $scope.totalCompanies = 0;
            $scope.usersPerPage = 25; // this should match however many results your API puts on one page
            $scope.filter['page'] = 1;
            $scope.getData();
            $scope.pageChanged = function(num) {
                console.log('meals page changed to ' + num);
                $scope.filter['page'] = num;
                $scope.getData();
            };

            $scope.selectOptions = {
                placeholder: "Выберите вендора",
                dataTextField: "Title",
                dataValueField: "Id",
                autoBind: false,
                dataSource: {
                    serverFiltering: true,
                    transport: {
                        read: {
                            url: "app/api/Companies?type=1",
                            dataType: 'json'
                        }
                    }
                }
            };
            $scope.selectPartnerOptions = {
                placeholder: "Выберите партнера",
                dataTextField: "Title",
                dataValueField: "Id"
            };
            $scope.companies = {};
            $scope.filter = {};
            $scope.$watch('Interest.Responsible.Id',function(value){
                if (value === undefined)
                    return;
                $scope.filter['responsible'] = value;
                $scope.getData();
            });
            $scope.$watch('partnerIds', function(value){
                if (value === undefined)
                    return;
                var arPartners = [];
                if (value !== undefined) {
                    for (var i = 0; i < value.length; i++) {
                        arPartners.push(value[i].Id);
                    }
                    $scope.filter['partner'] = arPartners;
                    $scope.getData();
                }
            });
            $scope.$watch('vendorIds', function(value){
                if (value === undefined)
                    return;
                var arVendors = [];
                if (value !== undefined) {
                    for (var i = 0; i < value.length; i++) {
                        arVendors.push(value[i].Id);
                    }
                    $scope.filter['vendor'] = arVendors;
                    $scope.getData();
                }
            });
            $scope.$watch('filterEmpty', function(value){
                if (value === undefined)
                    return;
                if (value) {
                    $scope.filter['filter'] = 'empty';
                }
                else
                    $scope.filter['filter'] = '';

                $scope.getData();
            });
            $scope.$watch('areaModel', function(value) {
                if (value === undefined)
                    return;
                if (value !== undefined && parseInt(value) > 0) {
                    $scope.filter['areaId'] = value;
                }
                else {
                    $scope.filter['areaId'] = '';
                }
                $scope.getData();
            });
        }
       }
    }
);
app.directive('interests', function(){
    return {
        restrict: 'E',
        templateUrl: 'app/interests/templates/interests.html',
        controller: function($scope, $location, $state, Restangular)
        {
            var Request = Restangular.all('Interests');
            this.interests = [];
            $scope.$broadcast('loader', true);
            Request.getList().then(function(response)
            {
                $scope.interests = response.data;
                $scope.$broadcast('loader', false);
            });
            $scope.remove = function(id)
            {
                var result = confirm("Согласны ли вы удалить интерес с ID " + id + "?");
                if (result) {
                    Request.remove({id: id}).then(function () {
                           alert('Интерес был удален!');
                    });

                }

            };
            $scope.show = function(id) {
                $state.go('interests.detail', {dealId: id});
            };
        }
    }
});