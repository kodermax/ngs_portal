var app = angular.module('interestApp.interests',[
    'ui.router',
    'restangular'
]);
app.config(function config($stateProvider) {
    //Абстрактная страница Интересов
    $stateProvider.state('interests', {
        abstract: true,
        url: '/interests',
        views: {
            "main": {
                controller: function($scope){

                },
                templateUrl: 'app/interests/views/interests.html'
            }
        }
    });
    $stateProvider.state('interests.list', {
        url: '',
        controller: function($scope)
        {
            $scope.$on('loader', function (event, data) {
                $scope.loader = data;
            });
        },
        templateUrl: 'app/interests/views/interests.list.html'
    });
    $stateProvider.state('interests.detail', {
        url: '/{interestId:[0-9]{1,7}}',
        templateUrl: 'app/interests/views/interests.detail.html',
        controller: function(Restangular,$state,$stateParams, $scope) {
            $scope.loader = true;
            var oneRequest = Restangular.one('Interests');
            oneRequest.get({id: $stateParams.interestId}).then(function(response) {
                if (response.status == 200) {
                    $scope.Interest = response.data[0];
                }
                $scope.loader = false;
            });
            $scope.edit = function () {
                $state.go('interests.edit', $stateParams);
            };
        }
    });
    $stateProvider.state('interests.edit', {
        url: '/edit/{interestId:[0-9]{1,7}}',
                templateUrl: 'app/interests/views/interests.edit.html',
                controller: function (Restangular, $stateParams, $state, $scope) {
                    $scope.Interest = [];
                    var oneRequest = Restangular.one('Interests');
                    oneRequest.get({id: $stateParams.interestId}).then(function(response) {
                        if (response.status == 200) {
                            $scope.Interest = response.data[0];
                        }
                        $scope.loader = false;
                    });
                    $scope.done = function(){
                        Restangular.all('Interests').post($scope.Interest).then(
                            function(response){
                                $state.go('interests.list', $stateParams);
                            }
                        );
                    }
                }
    });
    $stateProvider.state('interests.add', {
        url: '/add',
        templateUrl: 'app/interests/views/interests.edit.html',
        controller: function(Restangular,$state,$stateParams, $scope) {
            $scope.loader = true;
            $scope.Interest = {};

            $scope.loader = false;
            $scope.done = function(){
                Restangular.all('interests').post($scope.Interest).then(
                    function(response){
                        $state.go('interests.list', $stateParams);
                    }
                );
            }
        }
    });
    $stateProvider.state('interests.addToCompany', {
        url: '/addToCompany/{companyId:[0-9]{1,7}}',
        templateUrl: 'app/interests/views/interests.edit.html',
        controller: function(Restangular,$state,$stateParams, $scope) {
            $scope.loader = true;
            $scope.Interest = {};
            $scope.Interest.Company = {};

            Restangular.one('companies').get({id: $stateParams.companyId}).then(function(response) {
                if (response.status == 200) {
                    $scope.Interest.Company = response.data[0];
                }
                $scope.loader = false;
            });
            $scope.loader = false;
            $scope.done = function(){
                Restangular.all('interests').post($scope.Interest).then(
                    function(response){
                        $state.go('interests.list', $stateParams);
                    }
                );
            }
        }
    });

    $stateProvider.state('interests.report', {
        url: '/report',
        controller: function ($scope) {
            $scope.$on('loader', function (event, data) {
                $scope.loader = data;
            });
        },
        templateUrl: 'app/interests/views/interests.report.html'

    });
});