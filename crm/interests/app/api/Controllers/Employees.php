<?

class Controllers_Employees extends RestController
{
    public function __construct($request)
    {
        parent::__construct($request);				// Init parent constructor
    }

    public function get()
    {
        $arResult = array();
        $arParams["NAME_TEMPLATE"] = CSite::GetNameFormat();
        $arFilter['!UF_DEPARTMENT'] = false;
        $arFilter['ACTIVE'] = 'Y';
        $dbRes = CUser::GetList($by = 'last_name', $order = 'asc', $arFilter, array('SELECT' => array('UF_DEPARTMENT')));
        while ($arRes = $dbRes->GetNext())
        {
            $arResult[] = array(
                'Id' => $arRes['ID'],
                'Name' => CUser::FormatName($arParams["NAME_TEMPLATE"], $arRes, true, false),
                '~NAME' => CUser::FormatName($arParams["NAME_TEMPLATE"], array(
                    "NAME" => $arRes["~NAME"],
                    "LAST_NAME" => $arRes["~LAST_NAME"],
                    "LOGIN" => $arRes["~LOGIN"],
                    "SECOND_NAME" => $arRes["~SECOND_NAME"]
                ), true, false),
                'LOGIN' => $arRes['LOGIN'],
                'EMAIL' => $arRes['EMAIL'],
                'WORK_POSITION' => $arRes['WORK_POSITION'] ? $arRes['WORK_POSITION'] : $arRes['PERSONAL_PROFESSION'],
                '~WORK_POSITION' => $arRes['~WORK_POSITION'] ? $arRes['~WORK_POSITION'] : $arRes['~PERSONAL_PROFESSION'],
                'PHOTO' => (string)CIntranetUtils::createAvatar($arRes, array(), $arParams['SITE_ID']),
                'HEAD' => false
            );
        }
        $this->responseStatus = 200;
        $this->response = json_encode($arResult);

    }

    public function post()
    {
        $request = $this->request['params'];

    }

    public function put()
    {
        $this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
