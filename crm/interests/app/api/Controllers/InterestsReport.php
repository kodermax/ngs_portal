<?
use Bitrix\Crm as Crm;
use Bitrix\Main\Entity;
use Bitrix\Main\Type as FieldType;

class Controllers_InterestsReport extends RestController
{
    public function __construct($request)
    {
        parent::__construct($request);				// Init parent constructor
        \Bitrix\Main\Loader::includeModule('crm');
    }
    private function getData() {
        $this->request['content-type'] = 'json';
        $request = $this->request['params'];
        $arResult = array();
        $entity = Crm\InterestTable::getEntity();
        $main_query = new Entity\Query($entity);
        $main_query->setSelect(array('ID','TITLE','CONTACT','COMPANY','VENDOR','ASSIGNED_BY'));
        $result = $main_query->exec();
        $result = new CDBResult($result);
        $rows = array();
        $arVendors = array();
        $arCompanies = array();
        while ($row = $result->Fetch())
        {
            $arVendors[] = $row['CRM_INTEREST_VENDOR_ID'];
            $arCompanies[] = $row['CRM_INTEREST_COMPANY_ID'];
            $ar = array(
                'Id'	=> $row['ID'],
                'Title' => $row['TITLE'],
            );
            $ar['Vendor']['Id'] = $row['CRM_INTEREST_VENDOR_ID'];
            $ar['Vendor']['Title'] = $row['CRM_INTEREST_VENDOR_TITLE'];
            $ar['Company']['Id'] = $row['CRM_INTEREST_COMPANY_ID'];
            $ar['Company']['Title'] = $row['CRM_INTEREST_COMPANY_TITLE'];
            $ar['Contact']['Id'] = $row['CRM_INTEREST_CONTACT_ID'];
            $ar['Contact']['Name'] = CUser::FormatName(
                \Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
                array(
                    'LOGIN' => '',
                    'NAME' => $row['CRM_INTEREST_CONTACT_NAME'],
                    'LAST_NAME' => $row['CRM_INTEREST_CONTACT_LAST_NAME']
                ),
                false,
                false
            );
            $ar['Responsible']['Id'] = $row['CRM_INTEREST_ASSIGNED_BY_ID'];
            $ar['Responsible']['Name'] = CUser::FormatName(
                \Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
                array(
                    'LOGIN' => '',
                    'NAME' => $row['CRM_INTEREST_ASSIGNED_BY_NAME'],
                    'SECOND_NAME' => $row['CRM_INTEREST_ASSIGNED_BY_SECOND_NAME'],
                    'LAST_NAME' => $row['CRM_INTEREST_ASSIGNED_BY_LAST_NAME']
                ),
                false,
                false
            );

            $arResult[$ar['Company']['Id']][$ar['Vendor']['Id']][] = $ar;
        }
        $arVendors = array_unique($arVendors);
        $arCompanies = array_unique($arCompanies);

        $allVendors = array();
        $arFilter = array('COMPANY_TYPE' => 1);

        if (is_array($request['vendor'])){
            $arVendors = $request['vendor'];

            $arFilter['ID'] = $arVendors;
        }
        $list = CCrmCompany::GetList(array('TITLE' => "ASC"),$arFilter, array('ID','TITLE','COMPANY_TYPE'));
        while($row = $list->GetNext())
        {
            $allVendors[] = array(
                'Id' => $row['ID'],
                'Title' => $row['TITLE']
            );
        }
        //Areas
        #region areas
        $arAreas = array();
        $list = CIBlockElement::GetList(array('SORT' => 'ASC'), array('IBLOCK_CODE' =>'areas'),false,false,array('ID','NAME'));
        while($row = $list->GetNext()){
            $arAreas[] = array(
                'Id' => $row['ID'],
                'Title' => $row['NAME']);
        }

        if ($request['areaId'] > 0)
            $currentAreaId = intval($request['areaId']);
        else
            $currentAreaId = $arAreas[0]['Id'];
        #endregion
        //Companies

        $arFilter = array(
            'COMPANY_TYPE' => 'PARTNER',
            'UF_CRM_1363603316' => $currentAreaId
        );
        if (is_array($request['partner']))
        {
            $arPartners = $request['partner'];
            $arFilter['ID'] = $arPartners;
        }
        if ($this->request['params']['filter'] == 'empty')
        {
            $arFilter['ID'] = $arCompanies;
        }
        if ($request['responsible'] > 0) {
            $arFilter['ASSIGNED_BY_ID'] = intval($request['responsible']);
        }
        $allCompanies = array();
        $i = 1;

        $list = CCrmCompany::GetListEx(array('TITLE' => "ASC"), $arFilter,false,false, array('ID','TITLE','COMPANY_TYPE'));
        while($row = $list->GetNext())
        {
            $allCompanies[] = array(
                'index' => $i++,
                'Id' => $row['ID'],
                'Title' => html_entity_decode($row['TITLE']),
                'Url' => '/crm/company/show/' . $row['ID'].'/'
            );
        }
        $total = count($allCompanies);
        $page = intval($this->request['params']['page']);
        $pageCount = 50;
        if ($page >= 1)
        {
            $allCompanies = array_splice($allCompanies, ($page - 1) * $pageCount,$pageCount);
        }

        $arResult =  array(
            'data' => $arResult,
            'vendors' => $allVendors,
            'areas' => $arAreas,
            'companies' => $allCompanies,
            'totalCompanies' => $total
        );
        $this->responseStatus = 200;
        $this->response = json_encode($arResult);
    }
    public function get()
    {
       $this->getData();

    }

    public function post()
    {
        $this->getData();
    }

    public function put()
    {
        $this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
