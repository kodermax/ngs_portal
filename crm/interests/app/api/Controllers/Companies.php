<?

class Controllers_Companies extends RestController
{
    public function __construct($request)
    {
        parent::__construct($request);				// Init parent constructor
        \Bitrix\Main\Loader::includeModule('crm');
    }

    public function get()
    {
        $arResult = array();
        $arFilter = array();
        $arStatus = CCrmStatus::GetStatusList('COMPANY_TYPE');
        $nTop = 10;
        $q = trim($this->request['params']['q']);
        if (strlen($q) > 0)
            $arFilter['TITLE'] = "%".$q."%";
        $value = $this->request['params']['filter']['filters'][0]['value'];
        if (strlen($value) > 0)
        {
            $arFilter['TITLE'] = "%".$value."%";
        }
        $type = $this->request['params']['type'];
        if (strlen($type) > 0) {
            if ($type == 1) {
                $arFilter['COMPANY_TYPE'] = $type;
                $nTop = false;
            }
            else if($type== "PARTNER") {
                $arFilter['COMPANY_TYPE'] = $type;
                $nTop = false;
            }
        }
        if($this->request['params']['areaId'] > 0)
        {
           $arFilter['UF_CRM_1363603316'] = $this->request['params']['areaId'];
        }
        $id = intval($this->request['params']['id']);
        if ($id > 0)
            $arFilter['ID'] = $id;
        $list = CCrmCompany::GetList(array('TITLE' => "ASC"),$arFilter, array('ID','TITLE','COMPANY_TYPE'),$nTop);
        while($row = $list->GetNext())
        {
            $arResult[] = array(
                'Id' => $row['ID'],
                'Title' => $row['TITLE'],
                'Type' => $arStatus[$row['COMPANY_TYPE']]

            );
        }
        $this->responseStatus = 200;
        $this->response = json_encode($arResult);

    }

    public function post()
    {
        $request = $this->request['params'];

    }

    public function put()
    {
        $this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
