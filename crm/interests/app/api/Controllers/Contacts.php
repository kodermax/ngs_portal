<?

class Controllers_Contacts extends RestController
{
    public function __construct($request)
    {
        parent::__construct($request);				// Init parent constructor
        \Bitrix\Main\Loader::includeModule('crm');
    }

    public function get()
    {
        $arResult = array();
        $arFilter = array();
        $q = trim($this->request['params']['q']);

        if (strlen($q) > 0)
            $arFilter['FULL_NAME'] = "%".$q."%";
        $list = CCrmContact::GetList(array('LAST_NAME' => "ASC"),$arFilter, array('ID','FULL_NAME'),10);
        while($row = $list->GetNext())
        {
            $arResult[] = array(
                'Id' => $row['ID'],
                'Title' => $row['FULL_NAME']
            );
        }
        $this->responseStatus = 200;
        $this->response = json_encode($arResult);

    }

    public function post()
    {
        $request = $this->request['params'];

    }

    public function put()
    {
        $this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }
    public function delete()
    {
        $this->response = array('TestResponse' => 'I am DELETE response. Variables sent are - ' . http_build_query($this->request['params']));
        $this->responseStatus = 200;
    }

}
