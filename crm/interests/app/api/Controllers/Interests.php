<?
use Bitrix\Crm as Crm;
use Bitrix\Main\Entity;
use Bitrix\Main\Type as FieldType;

class Controllers_Interests extends RestController
{
	public function __construct($request)
	{
		parent::__construct($request);				// Init parent constructor
		\Bitrix\Main\Loader::includeModule('crm');
	}
	private function getVendors(){

	}
	public function get()
	{
		$arResult = array();
		if (!empty($this->request['params']['id']) && intval($this->request['params']['id']) <= 0)
		{
			$this->responseStatus = 200;
			$this->response = json_encode($arResult);
			return;
		}

		$entity = Crm\InterestTable::getEntity();
		$main_query = new Entity\Query($entity);
		$main_query->setSelect(array('ID','TITLE','CONTACT','COMPANY','VENDOR','ASSIGNED_BY','COMMENTS'));
		$arFilter = array();
		if (intval($this->request['params']['id']) > 0) {
			$arFilter['ID'] = $this->request['params']['id'];
		}
		$main_query->setFilter($arFilter);

		$result = $main_query->exec();
		$result = new CDBResult($result);
		$rows = array();
		while ($row = $result->Fetch())
		{
			$ar = array(
				'Id'	=> $row['ID'],
				'Title' => $row['TITLE'],
			);
			$ar['Comment'] = $row['COMMENTS'];
			$ar['Vendor']['Id'] = $row['CRM_INTEREST_VENDOR_ID'];
			$ar['Vendor']['Title'] = $row['CRM_INTEREST_VENDOR_TITLE'];
			$ar['Company']['Id'] = $row['CRM_INTEREST_COMPANY_ID'];
			$ar['Company']['Title'] = $row['CRM_INTEREST_COMPANY_TITLE'];
			$ar['Contact']['Id'] = $row['CRM_INTEREST_CONTACT_ID'];
			$ar['Contact']['Name'] = CUser::FormatName(
				\Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
				array(
					'LOGIN' => '',
					'NAME' => $row['CRM_INTEREST_CONTACT_NAME'],
					'LAST_NAME' => $row['CRM_INTEREST_CONTACT_LAST_NAME']
				),
				false,
				false
			);
			$ar['Responsible']['Id'] = $row['CRM_INTEREST_ASSIGNED_BY_ID'];
			$ar['Responsible']['Name'] = CUser::FormatName(
				\Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
				array(
					'LOGIN' => '',
					'NAME' => $row['CRM_INTEREST_ASSIGNED_BY_NAME'],
					'SECOND_NAME' => $row['CRM_INTEREST_ASSIGNED_BY_SECOND_NAME'],
					'LAST_NAME' => $row['CRM_INTEREST_ASSIGNED_BY_LAST_NAME']
				),
				false,
				false
			);

			$arResult[] = $ar;
		}
		$this->responseStatus = 200;
		$this->response = json_encode($arResult);

	}

	public function post()
	{
		$request = $this->request['params'];

		$this->request['content-type'] = 'json';

			$arFields = array(
				"TITLE" => trim($request['Title']),
				"ASSIGNED_BY_ID" => (int)$request['Responsible']['Id'],
				"CREATED_BY_ID" => $GLOBALS['USER']->GetID(),
				'DATE_MODIFIED' => new FieldType\DateTime(),
				'MODIFY_BY_ID' => $GLOBALS['USER']->GetID(),
			);
			if ($request['Company']['Id'] > 0)
				$arFields['COMPANY_ID'] = $request['Company']['Id'];
			if ($request['Contact']['Id'] > 0)
				$arFields['CONTACT_ID'] = $request['Contact']['Id'];

			if ($request['Vendor']['Id'] > 0)
				$arFields['VENDOR_ID'] = $request['Vendor']['Id'];

		if (strlen(trim($request['Comment'])) > 0)
				$arFields['COMMENTS'] = trim($request['Comment']);

			if ($request['Id'] > 0) {
				$result = Bitrix\Crm\InterestTable::update($request['Id'], $arFields);
				$ID = $request['Id'];
			}
			else {
				$arFields['DATE_CREATE'] = new FieldType\DateTime();
				$arFields['ORIGIN_ID'] = CTools::GUID();
				$result = Bitrix\Crm\InterestTable::add($arFields);
				$ID = $result->getId();
			}
		if ($ID !== false) {
			$this->status = 200;
			$this->responseStatus = 200;
			$this->response = json_encode(array('ID' => $ID));
		} else {
			$this->status = 200;
			$this->responseStatus = 200;
			$this->response = json_encode(array('ERROR' => 'error'));
		}
	}

	public function put()
	{
		$this->response = array('TestResponse' => 'I am PUT response. Variables sent are - ' . http_build_query($this->request['params']));
		$this->responseStatus = 200;
	}
	public function delete()
	{
		$id = intval($this->request['params']['id']);
		if ($id > 0) {
			Crm\InterestTable::delete($id);
			$this->response = json_encode(array('SUCCESS' => '1'));
			$this->responseStatus = 200;
		}
		else {
			$this->response = json_encode(array('SUCCESS' => '0'));
			$this->responseStatus = 200;
		}
	}

}
