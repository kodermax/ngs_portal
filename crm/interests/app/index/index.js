var app = angular.module('interestApp.index', ['ui.router']);
app.config(function config($stateProvider) {
    $stateProvider.state('index',
        {
            url: '/index',

            views: {
                "main": {
                    controller: 'IndexCtrl',
                    templateUrl: 'app/index/index.html'
                }
            }
        }) ;
});
app.controller('IndexCtrl',['$scope', function ($scope) {
    $scope.greetMe = 'World';
}]);
