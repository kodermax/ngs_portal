<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 06.04.2015
 * Time: 14:11
 */

$app->group('/api', function () use ($app, $log) {
        $app->group('/events',function() use ($app){
            $app->get('/:id',function($Id) use($app){
                \Bitrix\Main\loader::includeModule('crm');
                $arData = array(
                    'Deal' => array('Id' => '', 'Title' => ''),
                    'Partner' => '',
                    'Vendor' => '',
                    'Comment' => '',
                    'Author' => '',
                    'Positive' => ''
                );

                $list = CCrmEvent::GetList(array(),array('EVENT_REL_ID' => $Id,'CHECK_PERMISSIONS' => 'N'));

                if($row = $list->GetNext()){
                    $vendorTitle = '';
                if ($row['VENDOR_ID'] > 0){
                    $rsCompanies = CCrmCompany::GetList(array(),array('CHECK_PERMISSIONS' => 'N','ID' => $row['VENDOR_ID']),array('TITLE'));
                    if($arCompany = $rsCompanies->GetNext()){
                        $vendorTitle = $arCompany['TITLE'];
                    }
                }

                    if($row['ENTITY_TYPE'] == 'COMPANY') {
                        $rsCompanies = CCrmCompany::GetList(array(), array('CHECK_PERMISSIONS' => 'N', 'ID' => $row['ENTITY_ID']), array('TITLE'));
                        if ($arCompany = $rsCompanies->GetNext()) {
                            $partnerTitle = $arCompany['TITLE'];
                        }
                    }
                    else if($row['ENTITY_TYPE'] == "DEAL"){
                        $dealTitle = '';
                        $rsDeals = CCrmDeal::GetList(array(),array('CHECK_PERMISSIONS' => 'N','ID' => $row['ENTITY_ID']), array('ID','COMPANY_ID','TITLE'));
                        if($arDeal = $rsDeals->GetNext()){
                            $dealId = $arDeal['ID'];
                            $dealTitle = $arDeal['TITLE'];
                            $rsCompanies = CCrmCompany::GetList(array(), array('CHECK_PERMISSIONS' => 'N', 'ID' => $arDeal['COMPANY_ID']), array('TITLE'));
                            if ($arCompany = $rsCompanies->GetNext()) {
                                $partnerTitle = $arCompany['TITLE'];
                            }
                        }
                    }
                 $arData = array(
                     'Deal' => array(
                         'Id' => $dealId,
                         'Title' => $dealTitle
                     ),
                     'Date' => $row['DATE_CREATE'],
                     'Positive' => $row['POSITIVE'],
                     'Partner' => $partnerTitle,
                     'Vendor' => $vendorTitle,
                     'Comment' => $row['EVENT_TEXT_1'],
                     'Author' => $row['CREATED_BY_NAME'].' '. $row['CREATED_BY_LAST_NAME']);
                }
                $app->response()->write(json_encode($arData), true);
            });
            $app->post('/ds',function() use ($app){
                $request = json_decode($app->request()->getBody(), true);
                \Bitrix\Main\loader::includeModule('crm');
                $eventDate = ConvertTimeStamp(time() + CTimeZone::GetOffset(), 'FULL', SITE_ID);

                $vendorList = CCrmDeal::GetList(array(),array('CHECK_PERMISSIONS' => 'N','ID'=>$request['DEAL_ID']),array('UF_CRM_DEAL_VENDOR'));
                if($arVendor = $vendorList->GetNext()){
                    $vendorId = $arVendor['UF_CRM_DEAL_VENDOR'];
                }
                $list = CUser::GetList($by, $order, array("XML_ID" => $request['CREATED_BY_ID'],array('FIELDS' => array('ID'))));
                if($row = $list->GetNext()){
                    $managerId = $row['ID'];
                }
                $CCrmEvent = new CCrmEvent();
                $arData = array(
                    'ENTITY_TYPE'=> 'DEAL',
                    'ENTITY_ID' => $request['DEAL_ID'],
                    'EVENT_ID' => 'INTEREST',
                    'EVENT_TEXT_1' => $request['COMMENT'],
                    'DATE_CREATE' => $eventDate,
                    'VENDOR_ID' => $vendorId,
                    'USER_ID' => $managerId,
                    'POSITIVE' => 'Y',
                );
                $success = $CCrmEvent->Add($arData, false);
                if($success) {
                    echo json_encode(array(SUCCESS => 1));
                }
                else {
                    echo json_encode(array(SUCCESS => 0));
                }
            });

        });
});