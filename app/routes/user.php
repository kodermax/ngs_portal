<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 13.04.2015
 * Time: 11:48
 */
$app->group('/api', function () use ($app) {
    $app->group('/user', function () use ($app) {
        $app->post('/absent/save', function() use ($app) {
            $request = json_decode($app->request()->getBody(), true);
            $iblockId = 3;
            $property_enums = CIBlockPropertyEnum::GetList(Array("DEF" => "DESC", "SORT" => "ASC"), Array("IBLOCK_ID" => $iblockId, "CODE" => "ABSENCE_TYPE"));
            while ($enum_fields = $property_enums->GetNext()) {
                $arStatus[$enum_fields['XML_ID']] = $enum_fields['ID'];
                $arStatusIds[$enum_fields['ID']] = $enum_fields['XML_ID'];
            }
            $list = CIBlockElement::GetList(array(), array('CHECK_PERMISSIONS' => 'N', "XML_ID" => $request['GUID'], 'IBLOCK_ID' => $iblockId), false, false, array('ID'));
            $arDeletes = array();
            while ($row = $list->GetNext()) {
                $arDeletes[] = $row['ID'];
            }
            foreach ($arDeletes as $item) {
                CIBlockElement::Delete($item);
            }
            $el = new CIBlockElement();
            foreach($request['WORKERS'] as $arItem) {
                //$arData = json_decode($s, true);
                $rsUsers = CUser::GetList($by, $order, array('EMAIL' => $arItem['EMAIL']), array('FIELDS' => array('ID', 'NAME', 'LAST_NAME')));
                if ($arUser = $rsUsers->GetNext()) {
                    $userId = $arUser['ID'];
                    $arFields = array(
                        'ACTIVE_FROM' => $arItem['ACTIVE_FROM'],
                        'ACTIVE_TO' => $arItem['ACTIVE_TO'],
                        'NAME' => $arItem['NAME'],
                        'PROPERTY_VALUES' => array(
                            'USER' => $userId,
                            'ABSENCE_TYPE' => $arStatus[$arItem['ABSENCE_TYPE']]
                        ),

                        'XML_ID' => $request['GUID'],
                        'IBLOCK_ID' => $iblockId
                    );
                    $success = $el->Add($arFields, false, false, false);
                    if (!$success) {
                        $app->response()->write(json_encode(array('success' => 0, 'error' => $el->LAST_ERROR)), true);
                        return;
                    }

                }
                else {
                    $app->response()->write(json_encode(array('success' => 0,'error' => 'not found user')), true);
                    return;
                }
            }
                $app->response()->write(json_encode(array('success' => 1)), true);

        });
    });
});