<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 16.04.2015
 * Time: 9:30
 */
\Bitrix\Main\loader::includeModule('tasks');
$app->group('/api', function () use ($app) {
    $app->group('/tasks', function () use ($app) {
        $app->post('/employees',function() use($app){
            $request = json_decode($app->request()->getBody(), true);
            $arData = array(
                'TITLE' => $request['TITLE'],
                'DESCRIPTION' => $request['DESCRIPTION'],
                'DEADLINE' => $request['DEADLINE'],
                'DESCRIPTION_IN_BBCODE' => 'N',
                'RESPONSIBLE_ID' => 489,
                'ACCOMPLICES' => array(556,479,486,518)
            );
            $rsUsers = CUser::GetList($by, $order, array('EMAIL' => $request['OWNER']), array('FIELDS' => array('ID')));
            if($arUser = $rsUsers->GetNext())
            {
                try {
                    $task = CTaskItem::add($arData, $arUser['ID']);
                    //Запись в файл
                    $fileContent = base64_decode($request['FILE']);
                    $tmpfname = tempnam("/tmp", "FOO");
                    $handle = fopen($tmpfname, "w");
                    fwrite($handle, $fileContent);
                    fclose($handle);
                    $arFile = array(
                        "name" => "file.csv",
                        "size" => filesize($tmpfname),
                        "tmp_name" => $tmpfname,
                        "type" => "",
                        "del" => "N",
                        "MODULE_ID" => "tasks",
                        "description" => "",
                    );
                    $fid = CFile::SaveFile($arFile, "vote");
                    unlink($tmpfname);
                    $arTask = $task->getData();
                    $arFields = Array(
                        "TASK_ID" => $arTask['ID'],
                        "FILE_ID" => $fid
                    );
                    $obTaskFiles = new CTaskFiles;
                    $ID = $obTaskFiles->Add($arFields);
                    $success = ($ID>0);
                    $sortIndex = 1;
                    foreach($request['CHECKLIST'] as $item) {
                        $arFields = array(
                            'TITLE' => $item,
                            'IS_COMPLETE' => 'N',
                            'SORT_INDEX' => $sortIndex
                        );
                        $sortIndex++;
                        $oCheckListItem = CTaskCheckListItem::add($task, $arFields);
                    }




                }
                catch(Exception $ex)
                {
                    $app->response()->write(json_encode(array('success' => 0,'error' => $ex->getMessage())), true);
                    return;
                }
            }
            $app->response()->write(json_encode(array('success' => 1)), true);
        });
    });
});