<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 24.02.2015
 * Time: 12:04
 */

$app->group('/api', function () use ($app, $log) {
    $app->group('/v1', function () use ($app, $log) {
        $app->group('/reports',function() use ($app){
            $app->post('/plans_facts', function() use($app){
                $request = $app->request()->params();
                $date = new DateTime($request['date']);
                $rsUser = CUser::GetByID($GLOBALS['USER']->GetID());
                $arUser = $rsUser->Fetch();
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_plans_facts_api", "");
                $url .= "?manager=".$arUser['XML_ID']."&date=".$date->format('Ymd');
                $result = $client->get($url);
                echo $result;
            });
            $app->post('/sales', function() use($app){
                $request = $app->request()->params();
                if (strlen($request['startDate']) > 0) {
                    $start_date = new DateTime($request['startDate']);
                    $date_start = $start_date->format('Ymd');
                }
                else {
                    $date_start = "";
                }
                if (strlen($request['endDate']) > 0) {
                    $end_date = new DateTime($request['endDate']);
                    $date_end = $end_date->format('Ymd');
                }
                else {
                    $date_end = "";
                }
                $user = $request['user'];
                $pay_full = $request['pay_full'] == "true" ? 1: 0;
                $pay_part = $request['pay_part'] == "true" ? 1: 0;
                $pay_no =  $request['pay_no'] == "true" ? 1: 0;
                $del_full = $request['del_full'] == "true" ? 1: 0;
                $del_part = $request['del_part'] == "true" ? 1: 0;
                $del_no = $request['del_no'] == "true" ? 1: 0;
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $client->setHeader('Accept-Language','ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4');
                $url = COption::GetOptionString("ngsec", "1c_sales_api", "");
                $url .= "?manager=".$user."&date_start=".$date_start."&date_end=".$date_end;
                $url .= "&pay_full=" . $pay_full."&pay_no=".$pay_no."&pay_part=".$pay_part;
                $url .= "&del_full=".$del_full."&del_part=".$del_part."&del_no=".$del_no;
                $result = $client->get($url);
                echo $result;
            });
            $app->post('/analyze_partner', function() use($app){
                $request = $app->request()->params();
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $client->setHeader('Accept-Language','ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4');
                $url = COption::GetOptionString("ngsec", "1c_crm_report_analyze_partner_api", "");
                $params = "?manager=".$request['user']."&vendor=".$request['vendor']."&partner=".$request['partner']."&date=".$request['date'];
                $url .= $url.$params;
                $result = $client->get($url);
                echo $result;
            });
            $app->post('/zp',function() use($app){
                $request = $app->request()->params();
                $date = new DateTime($request['date']);
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_zp_api", "");
                $url .= "?pin=".md5(md5($request['pin']))."&date=".$date->format('Ymd')."&save=".$request['save'];
                $result = $client->get($url);
                echo $result;
            });
            $app->get('/zp/file',function() use($app){
                $request = $app->request()->params();
                $date = new DateTime($request['date']);
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_zp_api", "");
                $url .= "?pin=".md5(md5($request['pin']))."&date=".$date->format('Ymd')."&save=Y";
                $result = $client->get($url);
                $GLOBALS['APPLICATION']->RestartBuffer();
                header('Set-Cookie: fileDownload=true; path=/');
                header('Cache-Control: max-age=60, must-revalidate');
                header("Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                header('Content-Disposition: attachment; filename=zp.xlsx');
                echo $result;
            });
        });
    });
});
?>