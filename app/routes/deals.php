<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 24.02.2015
 * Time: 12:04
 */

$app->group('/api', function () use ($app, $log) {
    $app->group('/v1', function () use ($app, $log) {
        $app->group('/deals',function() use ($app){
            $app->get('/analyze/:id', function($id) use($app){
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_analyze_deals_api", "");
                $url .= "?id=".$id;
                $result = $client->get($url);
                echo $result;
            });
            $app->get('/invoices/:dealId', function($id) use($app){
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_deals_invoices_api", "");
                $url .= "?id=".$id;
                $result = $client->get($url);
                echo $result;
            });
            $app->get('/createOrders/:dealId', function($Id) use ($app){
                \Bitrix\Main\loader::includeModule('crm');
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_add_orders_api", "");
                $arData = array(
                    'id' => $Id
                );
                $list = CCrmDeal::GetList(array(),array('ID' => $Id, 'CHECK_PERMISSIONS' => 'N'), array('CURRENCY_ID'),false);
                if($row = $list->GetNext()){
                  $arData['currency'] = $row['CURRENCY_ID'];
                }
                unset($list);
                $arRows = CCrmProductRow::LoadRows('D', $Id);
                if (count($arRows) > 0) {
                    foreach ($arRows as $item) {
                        $list = CIBlockElement::GetList(array(), array('ID' => $item['PRODUCT_ID']), false, false, array('CODE'));
                        if ($row = $list->GetNext()) {
                            $arData['items'][] = array(
                                'guid' => $row['CODE'],
                                'quantity' => $item['QUANTITY'],
                                'price' => $item['PRICE']
                            );
                        }

                    }
                    $result = $client->post($url, json_encode($arData));
                    $app->response()->write($result, true);
                }
                else {
                    $app->response()->write(json_encode(array('status' => false,'errors' => array('Нет товаров'))), true);
                }
            });
            $app->post('/invoices/add_ppds',function() use($app){
                \Bitrix\Main\loader::includeModule('crm');
                $request = json_decode($app->request()->getBody(), true);
                $userId = $GLOBALS['USER']->GetID();
                $rsUser = CUser::GetList($by, $order, array('ID' => $userId),array('FIELDS' => array('XML_ID')));
                if($arUser = $rsUser->GetNext()){
                    $manager = $arUser['XML_ID'];
                }

                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_add_ppds_api", "");
                $url .= "?manager=".$manager."&guid=".trim($request['GUID']);
                $result = $client->get($url);
                echo json_encode(array('status' =>true));
            });
            $app->get('/invoices/make/:name',function($fileName) use ($app){
                $GLOBALS['APPLICATION']->RestartBuffer();
                header('Set-Cookie: fileDownload=true; path=/');
                header('Cache-Control: max-age=60, must-revalidate');
                header("Content-type: application/pdf");
                header('Content-Disposition: inline; filename=invoice.pdf');
                $result = file_get_contents('/tmp/' . $fileName);
                echo $result;
                unlink('/tmp/' . $fileName);
                die();
            });
            $app->post('/invoices/make/:id',function($id) use($app){
                \Bitrix\Main\loader::includeModule('iblock');
                \Bitrix\Main\loader::includeModule('crm');
                global $DB;
                $request = $app->request()->getBody();
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin','BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_deals_invoices_get_data_api", "");

                $result = $client->post($url, $request);

                $tmpfname = tempnam("/tmp", "FOO");
                $handle = fopen($tmpfname, "w");
                fwrite($handle, $result);
                fclose($handle);
               $app->response()->write(json_encode(array('result' => basename($tmpfname))), true);

               /* $CCrmInvoice = new CCrmInvoice(false);
                $CCrmCompany = new CCrmCompany(false);
                foreach($arResult as $item) {
                    $companyID = 0;
                    $oDeal = $CCrmCompany->GetList(array(),array('CHECK_PERMISSIONS' => 'N','ORIGIN_ID' => $item['PARTNER']),array('ID'));
                    if($arDeal = $oDeal->GetNext()){
                        $companyID = $arDeal['ID'];
                    }
                    $date = new DateTime(date('Y-m-d'));
                    if ($item['CURRENCY'] == 'RUB')
                        $date->add(new DateInterval('P3D'));
                    else
                        $date->add(new DateInterval('P10D'));
                   $arFields = array(
                        'XML_ID' => $item['ORDER_GUID'],
                        'ORDER_TOPIC' => 'Счёт по заказу № '.$item['ORDERID'],
                        'RESPONSIBLE_ID' => $GLOBALS['USER']->GetID(),
                        'STATUS_ID' => 'N',
                        'DATE_INSERT' => date('d.m.Y H:i:s'),
                        'PAY_VOUCHER_DATE' => '',
                        'CURRENCY' => $item['CURRENCY'],
                        'CURRENCY_ID' => $item['CURRENCY'],
                        'COMMENTS' => '',
                        'DATE_BILL' => date('d.m.Y'),
                        'DATE_PAY_BEFORE' => $date->format('d.m.Y'),
                        'UF_DEAL_ID' => $item["ID"],
                        'PERSON_TYPE_ID' => 1,
                        'PAY_SYSTEM_ID' => 1,
                        'UF_COMPANY_ID' => $companyID,
                        'UF_CONTACT_ID' => 0,
                        'USER_DESCRIPTION' => '',
                        'UF_QUOTE_ID' => 0,
                       'INVOICE_PROPERTIES' => Array
                       (
                           '10' => '',
                            '11' => '',
                           '8' => '',
                            '9' =>'',
                            '12' => '',
                            '13' => '',
                            '14' => '',
                            '15' => '',
                            '16' => '',
                            '17' => '',
                            '18' => '',
                            '19' => ''
                        )

                    );
                    foreach($item['ITEMS'] as $row){
                        $arProduct = GetProductIdByGuid($row['GUID']);
                        if($arProduct){
                            $arFields['PRODUCT_ROWS'][] = array(
                                'ID' => 0,
                                'PRODUCT_ID' => $arProduct['ID'],
                                'PRODUCT_NAME' => $arProduct['NAME'],
                                'QUANTITY' => $row['AMOUNT'],
                                'PRICE' =>  $row['PRICE'],
                                'DISCOUNT_PRICE' => 0,
                                'VAT_RATE' => $row['VAT_RATE'],
                                'MEASURE_CODE' => 796,
                                'MEASURE_NAME' => 'шт',
                                'CUSTOMIZED' => 'Y',
                                'CURRENCY' => $item['CURRENCY']
                            );
                        }

                    }
                    $recalculate = false;
                    $DB->StartTransaction();
                    $ID = $CCrmInvoice->Add($arFields, $recalculate, SITE_ID, array('REGISTER_SONET_EVENT' => false, 'UPDATE_SEARCH' => false));
                    $bSuccess = (intval($ID) > 0) ? true : false;
                    if($bSuccess)
                    {
                        $DB->Commit();
                    }
                    else {
                        $DB->Rollback();
                    }
                }
               */
              //  echo json_encode(array('status' =>true));
            });
        });
    });
});

function GetProductIdByGuid($guid)
{
    if (strlen($guid) <= 0) return array();
    $list = CIBlockElement::GetList(array(),array('IBLOCK_ID'=> IBLOCK_CRM_CATALOG, 'CODE' => $guid, 'CHECK_PERMISSIONS' => 'N'), false,false, array('ID', 'NAME'));
    if ($row = $list->GetNext())
    {
        return array('ID' => $row['ID'],'NAME' => $row['~NAME']);
    }
    return array();
}
?>