<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 24.02.2015
 * Time: 12:03
 */
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('BX_NO_ACCELERATOR_RESET', true);
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', false);
define('NO_AGENT_STATISTIC','Y');
define('NO_AGENT_CHECK', true);
define('DisableEventsCheck', true);
require("/home/bitrix/www/bitrix/modules/main/include/prolog_before.php");
require 'vendor/autoload.php';

$app = new Slim\Slim();

//// API group
require 'routes/deals.php';
require 'routes/reports.php';
require 'routes/events.php';
require 'routes/user.php';
require 'routes/tasks.php';
$app->run();

?>