<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 28.01.2015
 * Time: 14:39
 */

if(!$USER->IsAdmin())
    return;

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

$arAllOptions = Array(
    Array("1c_deals_api", GetMessage("NGSEC_1C_DEALS_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/deals.hs/new/", Array("text", 130)),
    Array("1c_contragents_api", GetMessage("NGSEC_1C_CONTRAGENTS_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/customers.hs/task/", Array("text", 130)),
    Array("1c_analyze_deals_api", GetMessage("NGSEC_1C_ANALYZE_DEALS_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/deals.hs/analyze_deals", Array("text", 130)),
    Array("1c_plans_facts_api", GetMessage("NGSEC_1C_PLANS_FACTS_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/mpr.hs/report/", Array("text", 130)),
    Array("1c_deals_invoices_api", GetMessage("NGSEC_1C_DEALS_INVOICES_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/invoice.hs/get_orders", Array("text", 130)),
    Array("1c_deals_invoices_get_data_api", GetMessage("NGSEC_1C_DEALS_INVOICES_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/invoice.hs/get_data/", Array("text", 130)),
    Array("1c_deals_invoices_get_pdf_api", GetMessage("NGSEC_1C_DEALS_INVOICES_GET_PDF_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/invoice.hs/get_pdf", Array("text", 130)),
    Array("1c_zp_api", GetMessage("NGSEC_1C_ZP_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/rvozup.hs", Array("text", 130)),
    Array("1c_sales_api", GetMessage("NGSEC_1C_SALES_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/mor.hs/report/", Array("text", 130)),
    Array("1c_crm_events_api", GetMessage("NGSEC_1C_CRM_EVENTS_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/activ.hs/act/", Array("text", 130)),
    Array("1c_crm_report_analyze_partner_api", GetMessage("NGSEC_1C_CRM_REPORT_ANALYZE_PARTNER_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/activ.hs/report/", Array("text", 130)),
    Array("1c_add_ppds_api", GetMessage("NGSEC_1C_ADD_PPDS_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/invoice.hs/add_ppds", Array("text", 130)),
    Array("1c_add_orders_api", GetMessage("NGSEC_1C_ADD_ORDERS_API"), "https://1c.ngsec.ru:8443/oper_dev/hs/invoice.hs/add_orders", Array("text", 130)),
);
$aTabs = array(
    array("DIV" => "edit1", "TAB" => GetMessage("MAIN_TAB_SET"), "ICON" => "ib_settings", "TITLE" => GetMessage("MAIN_TAB_TITLE_SET")),
);
$tabControl = new CAdminTabControl("tabControl", $aTabs);

if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid())
{

    if(strlen($RestoreDefaults)>0)
    {
        COption::RemoveOption("ngsec");
    }
    else
    {
        foreach($arAllOptions as $arOption)
        {
            $name=$arOption[0];
            $val=$_REQUEST[$name];
            if($arOption[2][0]=="checkbox" && $val!="Y")
                $val="N";
            COption::SetOptionString("ngsec", $name, $val, $arOption[1]);
        }
    }
if(strlen($Update)>0 && strlen($_REQUEST["back_url_settings"])>0)
        LocalRedirect($_REQUEST["back_url_settings"]);
    else
        LocalRedirect($APPLICATION->GetCurPage()."?mid=".urlencode($mid)."&lang=".urlencode(LANGUAGE_ID)."&back_url_settings=".urlencode($_REQUEST["back_url_settings"])."&".$tabControl->ActiveTabParam());

}


$tabControl->Begin();
?>
<form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?echo LANGUAGE_ID?>">
    <?$tabControl->BeginNextTab();?>
    <?
    foreach($arAllOptions as $arOption):
        $val = COption::GetOptionString("ngsec", $arOption[0], $arOption[2]);
        $type = $arOption[3];
        ?>
        <tr>
            <td width="40%" nowrap <?if($type[0]=="textarea") echo 'class="adm-detail-valign-top"'?>>
                <label for="<?echo htmlspecialcharsbx($arOption[0])?>"><?echo $arOption[1]?>:</label>
            <td width="60%">
                <?if($type[0]=="checkbox"):?>
                    <input type="checkbox" id="<?echo htmlspecialcharsbx($arOption[0])?>" name="<?echo htmlspecialcharsbx($arOption[0])?>" value="Y"<?if($val=="Y")echo" checked";?>>
                <?elseif($type[0]=="text"):?>
                    <input type="text" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($arOption[0])?>">
                <?elseif($type[0]=="textarea"):?>
                    <textarea rows="<?echo $type[1]?>" cols="<?echo $type[2]?>" name="<?echo htmlspecialcharsbx($arOption[0])?>"><?echo htmlspecialcharsbx($val)?></textarea>
                <?endif?>
            </td>
        </tr>
    <?endforeach?>
    <?$tabControl->Buttons();?>
    <input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
    <input type="submit" name="Apply" value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
    <?if(strlen($_REQUEST["back_url_settings"])>0):?>
        <input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
        <input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
    <?endif?>
    <input type="submit" name="RestoreDefaults" title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
    <?=bitrix_sessid_post();?>
    <?$tabControl->End();?>
</form>
