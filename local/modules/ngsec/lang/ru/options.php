<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 28.01.2015
 * Time: 14:45
 */

$MESS["NGSEC_1C_DEALS_API"] = "Обмен сделок с 1С";
$MESS["NGSEC_1C_CONTRAGENTS_API"] = "Обмен контрагентов с 1С";
$MESS["NGSEC_1C_ANALYZE_DEALS_API"] = "Анализ сделки";
$MESS["NGSEC_1C_PLANS_FACTS_API"] = "Планы и факты продаж";
$MESS["NGSEC_1C_DEALS_INVOICES_API"] = "Счета для сделок";
$MESS["NGSEC_1C_DEALS_INVOICES_GET_PDF_API"] = "Получение pdf для счета";
$MESS["NGSEC_1C_ZP_API"] = "Сервис для зарплаты";
$MESS["NGSEC_1C_SALES_API"] = "Продажи";
$MESS["NGSEC_1C_CRM_EVENTS_API"] = "События CRM";
$MESS["NGSEC_1C_CRM_REPORT_ANALYZE_PARTNER_API"] = "Отчёт Анализ партнеров";
$MESS["NGSEC_1C_ADD_PPDS_API"] = "Планируемое поступление денежных средств";
$MESS["NGSEC_1C_ADD_ORDERS_API"] = "Создание заказов";

?>