<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 28.01.2015
 * Time: 14:52
 */


$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang)-strlen("/install/index.php"));
IncludeModuleLangFile($strPath2Lang."/install.php");

Class ngsec extends CModule
{
    var $MODULE_ID = "ngsec";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;

    var $errors;

    function ngsec()
    {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path."/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }
        else
        {
            $this->MODULE_VERSION = IBLOCK_VERSION;
            $this->MODULE_VERSION_DATE = IBLOCK_VERSION_DATE;
        }

        $this->MODULE_NAME = GetMessage("NGSEC_INSTALL_NAME");
        $this->MODULE_DESCRIPTION = GetMessage("NGSEC_INSTALL_DESCRIPTION");
    }


    function InstallDB()
    {
        global $DB, $DBType, $APPLICATION;
        $this->errors = false;


        if($this->errors !== false)
        {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }

        RegisterModule("ngsec");

        return true;
    }

    function UnInstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;
        $this->errors = false;
        $arSQLErrors = array();

        if(!CModule::IncludeModule("ngsec"))
            return false;

        $arSql = $arErr = array();
        if(!array_key_exists("savedata", $arParams) || ($arParams["savedata"] != "Y"))
        {

        }

        if(is_array($this->errors))
            $arSQLErrors = array_merge($arSQLErrors, $this->errors);

        if(!empty($arSQLErrors))
        {
            $this->errors = $arSQLErrors;
            $APPLICATION->ThrowException(implode("", $arSQLErrors));
            return false;
        }

        UnRegisterModule("ngsec");

        return true;
    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles()
    {

        return true;
    }

    function UnInstallFiles()
    {

        return true;
    }


    function DoInstall()
    {
        global $APPLICATION, $step, $obModule;
        $step = IntVal($step);

        if($this->InstallDB())
        {
            $this->InstallFiles();
        }
        $obModule = $this;
        $APPLICATION->IncludeAdminFile(GetMessage("NGSEC_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/ngsec/install/step.php");

    }

    function DoUninstall()
    {
        global $APPLICATION, $step, $obModule;
        $step = IntVal($step);

        $this->UnInstallDB(array(
            "savedata" => $_REQUEST["savedata"],
        ));
        $GLOBALS["CACHE_MANAGER"]->CleanAll();
        $this->UnInstallFiles();
        $obModule = $this;
        $APPLICATION->IncludeAdminFile(GetMessage("NGSEC_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/ngsec/install/unstep.php");

    }

    function OnGetTableSchema()
    {
        return array(
            "ngces" => array()
        );
    }
}
