<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 30.03.2015
 * Time: 16:08
 */

class RestCrmEvents
{
    function __construct()
    {
        \Bitrix\Main\loader::includeModule('crm');
    }

    public function saveEvent($eventId, $arFields)
    {


        if($arFields['VENDOR_ID'] > 0) {
            $list = CCrmCompany::GetList(array(), array('CHECK_PERMISSIONS' => 'N','ID' => $arFields['VENDOR_ID']), array('ORIGIN_ID'));
            if ($row = $list->GetNext()) {
                $vendorGUID = $row['ORIGIN_ID'];
            }
        }
        if($arFields['ENTITY_TYPE'] == 'COMPANY') {
            $list = CCrmCompany::GetList(array(), array('CHECK_PERMISSIONS' => 'N', 'ID' => $arFields['ENTITY_ID']), array('ORIGIN_ID'));
            if ($row = $list->GetNext()) {
                $partnerGUID = $row['ORIGIN_ID'];
            }
        }
        else if($arFields['ENTITY_TYPE'] == "DEAL"){
            $list = CCrmDeal::GetList(array(),array('CHECK_PERMISSIONS' => 'N','ID' => $arFields['ENTITY_ID']), array('COMPANY_ID'));
            if($row = $list->GetNext()){
                $list = CCrmCompany::GetList(array(), array('CHECK_PERMISSIONS' => 'N', 'ID' => $row['COMPANY_ID']), array('ORIGIN_ID'));
                if ($row = $list->GetNext()) {
                    $partnerGUID = $row['ORIGIN_ID'];
                }
            }
        }
        if ($arFields['USER_ID'] > 0){
            $userId = $arFields['USER_ID'];
        }
        else {
            $userId = $GLOBALS['USER']->GetID();
        }
        $rsUser = CUser::GetByID($userId);
        $arUser = $rsUser->Fetch();
        $arData = array(
            "ID" => strval($eventId),
            "ACTION" => "save",
            "PARTNER" => $partnerGUID,
            "VENDOR" =>  $vendorGUID,
            "MANAGER" => $arUser['XML_ID'],
            "STAGE" => $arFields['STAGE'] == 1 ? "Первоначальная" : "Конкретный интерес",
            "TYPE" =>  $arFields['POSITIVE'] == 'Y' ? "Положительная":"Отрицательная",
            "DESC" =>  $arFields['EVENT_TEXT_1'],
            "DATE" =>  date('YmdHis')
        );
        if ($arFields['ENTITY_TYPE'] == 'DEAL'){
            $arData['DEAL_ID'] = strval($arFields['ENTITY_ID']);
        }
        $client = new \Bitrix\Main\Web\HttpClient();
        $client->setAuthorization('admin','BdfyjD3341');
        $url = COption::GetOptionString("ngsec", "1c_crm_events_api", "");
        $s = json_encode($arData);
        $result = $client->post($url, $s);


    }
    public function deleteEvent($eventId){
        $list = CCrmEvent::GetList(array(),array('ID' => $eventId));
        if($row = $list->GetNext()){
            if ($row['EVENT_NAME'] == 'Активность' && ($row['ENTITY_TYPE'] == 'COMPANY' || $row['ENTITY_TYPE'] == 'DEAL')) {
                $client = new \Bitrix\Main\Web\HttpClient();
                $client->setAuthorization('admin', 'BdfyjD3341');
                $url = COption::GetOptionString("ngsec", "1c_crm_events_api", "");
                $arData = array('ID' => strval($row['EVENT_ID']), 'ACTION' => 'delete');
                $s = json_encode($arData);
                $result = $client->post($url, $s);
            }
        }


    }
}