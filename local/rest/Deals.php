<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 14.01.2015
 * Time: 10:27
 */

class RestDeals {
    function __construct()
    {
        \Bitrix\Main\loader::includeModule('crm');
    }

    public function UpdateDeal($arFields)
    {
        $arFields['BEGINDATE'] = str_replace("'","",$arFields['~BEGINDATE']); //2013-07-22
        $arFields['CLOSEDATE'] = str_replace("'","",$arFields['~CLOSEDATE']); //2013-07-22
        if ($arFields['STAGE_ID'] == 1){
            $arFields['STAGE_ID'] = 'COMPETITION';
        }
        if ($arFields['STAGE_ID'] == 2){
            $arFields['STAGE_ID'] = 'MAINTENANCE';
        }
        if (strlen($arFields['UF_CRM_1364300676']) > 0) {
            $regStatus = $arFields['UF_CRM_1364300676'];
        }
        else {
            $regStatus = $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_DEAL','UF_CRM_1364300676', $arFields['ID'], LANGUAGE_ID);
        }
        $arStatuses = array();
        $rs = CUserFieldEnum::GetList(array(), array(
            "USER_FIELD_NAME" => 'UF_CRM_1364300676',
        ));
        while($ar = $rs->GetNext()) {
            $arStatuses[$ar['ID']] = $ar['XML_ID'];
        } 
        $managerId = $arFields['ASSIGNED_BY_ID'];
        $managerName = '';
        if ($managerId > 0) {
            if ($managerId == 537)
                return;
            $rsUser = CUser::GetByID($managerId);
            $arUser = $rsUser->Fetch();
            $managerName = $arUser['NAME'] . " ". $arUser['LAST_NAME'];
        }


        $arData = array(
            'NUMBER' => $arFields['ID'],
            'NAME' => $arFields['TITLE'],
            'BEGIN_DATE' => strlen($arFields['BEGINDATE']) > 0 ? date("Ymd",MakeTimeStamp($arFields['BEGINDATE'],"YYYY-MM-DD")) : "",
            'CLOSE_DATE' => strlen($arFields['CLOSEDATE']) > 0 ? date("Ymd",MakeTimeStamp($arFields['CLOSEDATE'],"YYYY-MM-DD")) : "",
            "CURRENCY" => $arFields['CURRENCY_ID'],
            "STAGE_ID" => $arFields['STAGE_ID'],
            'REG_STATUS' => $arStatuses[$regStatus],
            'MANAGER' => $managerName,
            'PROBABILITY' => $arFields['PROBABILITY'],
            'CLOSED' => $arFields['CLOSED'] == 'Y' ? true : false
        );
        if ($arFields['UF_CRM_DEAL_VENDOR'] > 0) {
            $arData['VENDOR'] = $this->GetCompanyGuid($arFields['UF_CRM_DEAL_VENDOR']);
        }
        else {
            $vendorId = $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_DEAL','UF_CRM_DEAL_VENDOR', $arFields['ID'], LANGUAGE_ID);
            if ($vendorId > 0)
                $arData['VENDOR'] = $this->GetCompanyGuid($vendorId);
        }
        if ($arFields['COMPANY_ID'] > 0) {
                $arData['PARTNER'] = $this->GetCompanyGuid($arFields['COMPANY_ID']);
        }
        //Заказчик
        if ($arFields['UF_CRM_1381998634']){
            $arData['CUSTOMER'] = $this->GetCompanyGuid($arFields['UF_CRM_1381998634']);
        }
        else {
            $customerId = $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_DEAL','UF_CRM_1381998634', $arFields['ID'], LANGUAGE_ID);
            if ($customerId > 0)
                $arData['CUSTOMER'] = $this->GetCompanyGuid($customerId);
        }
        $s = json_encode($arData);
        $client = new \Bitrix\Main\Web\HttpClient();
        $client->setAuthorization('admin','BdfyjD3341');
        $url = COption::GetOptionString("ngsec", "1c_deals_api", "");
        $result = $client->post($url,$s);

        if (strlen($result) > 0)
        {

        }
   }
    private function GetCompanyGuid($id)
    {
        $arFilter = array(
            'ID' => $id
        );
        $list = CCrmCompany::GetList(array(),$arFilter,array('ORIGIN_ID'));
        if($row = $list->GetNext())
        {
            return $row['ORIGIN_ID'];
        }
        return "";
    }
}
?>