<?php
/**
 * Created by PhpStorm.
 * User: mkarpychev
 * Date: 30.01.2015
 * Time: 9:42
 */
class RestContragents
{
    function __construct()
    {
        \Bitrix\Main\loader::includeModule('crm');
    }

    public function UpdateContragent($arFields) {
        $arFields['UF_CRM_CO_NAME_EN'] = strlen($arFields['UF_CRM_CO_NAME_EN']) > 0 ? $arFields['UF_CRM_CO_NAME_EN'] : $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_COMPANY','UF_CRM_CO_NAME_EN', $arFields['ID'], LANGUAGE_ID);
        $arFields['UF_CRM_1363603025'] = strlen($arFields['UF_CRM_1363603025']) > 0 ? $arFields['UF_CRM_1363603025'] : $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_COMPANY','UF_CRM_1363603025', $arFields['ID'], LANGUAGE_ID);
        $arFields['UF_CRM_1363603036'] = strlen($arFields['UF_CRM_1363603036']) > 0 ? $arFields['UF_CRM_1363603036'] : $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_COMPANY','UF_CRM_1363603036', $arFields['ID'], LANGUAGE_ID);
        $arFields['UF_CRM_CO_S_ADR_RU'] = strlen($arFields['UF_CRM_CO_S_ADR_RU']) > 0 ? $arFields['UF_CRM_CO_S_ADR_RU'] : $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_COMPANY','UF_CRM_CO_S_ADR_RU', $arFields['ID'], LANGUAGE_ID);
        $arFields['UF_CRM_BANK_DETAILS'] = strlen($arFields['UF_CRM_BANK_DETAILS']) > 0 ? $arFields['UF_CRM_BANK_DETAILS'] : $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_COMPANY','UF_CRM_BANK_DETAILS', $arFields['ID'], LANGUAGE_ID);
        $arFields['UF_CLASS'] = strlen($arFields['UF_CLASS']) > 0 ? $arFields['UF_CLASS'] : $GLOBALS['USER_FIELD_MANAGER']->GetUserFieldValue('CRM_COMPANY','UF_CLASS', $arFields['ID'], LANGUAGE_ID);
        if ($arFields['UF_CLASS'] > 0) {
            $rsClass = CUserFieldEnum::GetList(array(), array(
                "ID" => $arFields["UF_CLASS"],
            ));
            if ($arClass = $rsClass->GetNext())
                $arFields['UF_CLASS'] = $arClass["VALUE"];
        }
        $arData = array(
            'ID' => strval($arFields['ID']),
            'CLASS' => $arFields['UF_CLASS'],
            'GUID' => '',
            'TIP' => $arFields['COMPANY_TYPE'],
            'NAME' => $arFields['TITLE'],
            'ENGNAME' => $arFields['UF_CRM_CO_NAME_EN'],
            'INN' => $arFields['UF_CRM_1363603025'],
            'KPP' => $arFields['UF_CRM_1363603036'],
            'MANAGER' => '',
            'ADDRESSFACT' => $arFields['ADDRESS'],
            'ADDRESSUR' => $arFields['ADDRESS_LEGAL'],
            'ADDRESSDEL' => $arFields['UF_CRM_CO_S_ADR_RU'],
            'ACTIVE' => 'Y',
            'BANKS' => $arFields['UF_CRM_BANK_DETAILS']
        );
        $userId = $arFields['ASSIGNED_BY_ID'] > 0 ? $arFields['ASSIGNED_BY_ID'] : $GLOBALS['USER']->GetID();
        if ($userId > 0) {
            if ($userId == 537) return;
            $rsUsers = CUser::GetList($by = "LAST_NAME", $order = "asc", array('ID' => $userId),array('SELECT' => array('XML_ID')));
            if($arUser = $rsUsers->Fetch())
            {
                $arData['MANAGER'] = $arUser['XML_ID'];
            }

        }
        $list = CCrmCompany::GetList(array(),array('ID' => $arFields['ID'],'CHECK_PERMISSIONS' => 'N'), array('ORIGIN_ID'));
        if($row = $list->GetNext())
        {
            if (strlen($row['ORIGIN_ID']) > 0 )
                $arData['GUID'] = $row['ORIGIN_ID'];
        }

        foreach($arFields['FM']['EMAIL'] as $item)
        {
            if ($item['VALUE_TYPE'] == 'WORK') {
                $arData['EMAIL'] = $item['VALUE'];
                break;
            }
        }
        foreach($arFields['FM']['PHONE'] as $item)
        {
            if ($item['VALUE_TYPE'] == 'WORK') {
                $arData['PHONE'] = $item['VALUE'];
                break;
            }
        }
        foreach($arFields['FM']['WEB'] as $item)
        {
            if ($item['VALUE_TYPE'] == 'WORK') {
                $arData['WEB'] = $item['VALUE'];
                break;
            }
        }
        $client = new \Bitrix\Main\Web\HttpClient();
        $client->setAuthorization('admin','BdfyjD3341');
        $url = COption::GetOptionString("ngsec", "1c_contragents_api", "");
        $s = json_encode($arData);
        
        $result = $client->post($url,$s);
        if (strlen($result) > 0)
        {
            $arResult = json_decode($result, true);
            $crm = new CCrmCompany(false);
            $arUpdate = array(
                'ORIGIN_ID' => $arResult['GUID']
            );
            if (count($arResult['BANK_DETAILS']) > 0){
                $arUpdate['UF_CRM_BANK_DETAILS'] = $arResult['BANK_DETAILS'];
            }
            $crm->Update($arFields['ID'], $arUpdate, false,false);
        }
    }
}