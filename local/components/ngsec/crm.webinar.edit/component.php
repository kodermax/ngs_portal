<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Type as FieldType;

$ELEMENT_ID = (int)$arParams['ELEMENT_ID'];
$arParams['PATH_TO_WEBINAR_SHOW'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_WEBINAR_SHOW'],
    array(
        'webinar_id' => $ELEMENT_ID
    )
);

$userId = $GLOBALS['USER']->GetID();
$context = HttpApplication::getInstance()->getContext();
$request = $context->getRequest();
if ($ELEMENT_ID > 0) {
    $entity = \Bitrix\Crm\WebinarTable::getEntity();
    $query = new \Bitrix\Main\Entity\Query($entity);
    $query->setSelect(array('*','RESPONSIBLE_' => 'ASSIGNED_BY'));
    $query->setFilter(array('ID' => $ELEMENT_ID));
    $result = new CDBResult($query->exec());
    while ($row = $result->Fetch())
    {
        $row['ASSIGNED_BY_FORMAT_NAME'] = CUser::FormatName(
            \Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
            array(
                'LOGIN' => '',
                'NAME' => $row['RESPONSIBLE_NAME'],
                'SECOND_NAME' => $row['RESPONSIBLE_SECOND_NAME'],
                'LAST_NAME' => $row['RESPONSIBLE_LAST_NAME']
            ),
            false,
            false
        );
        $arResult['ITEM']['TITLE'] = $row['TITLE'];
        $arResult['ITEM']['DATE_START'] = $row['DATE_START'];
        $arResult['ITEM']['RESPONSIBLE_ID'] = $row['ASSIGNED_BY_ID'];
        $arResult['ITEM']['RESPONSIBLE_FULL_NAME'] = $row['ASSIGNED_BY_FORMAT_NAME'];
        $arResult['ITEM']['COMMENTS'] = $row['COMMENTS'];
    }
}
if ($request->isPost())
{
    if (strlen($request['TITLE']) > 0 && (int)$request['response_id'] > 0)
    {
        $arFields = array(
            "TITLE" => trim($request['TITLE']),
            "ASSIGNED_BY_ID" => (int)$request['response_id'],
            "CREATED_BY_ID" => $userId,
            'DATE_MODIFIED' => new FieldType\DateTime(),
            'MODIFY_BY_ID' => $userId
        );

        if (strlen($request['DATE_START']) > 0)
        {
            $date = new Bitrix\Main\Type\DateTime(ConvertDateTime($request['DATE_START'],'YYYY-MM-DD HH:MI:SS'),'Y-m-d H:i:s');
            $arFields['DATE_START'] = $date;
        }

        if (strlen($request['COMMENTS']) > 0)
            $arFields['COMMENTS'] = trim($request['COMMENTS']);
        if ($ELEMENT_ID > 0)
        {
            $result = Bitrix\Crm\WebinarTable::update($arParams['ELEMENT_ID'], $arFields);
        }
        else {
            $arFields['ORIGIN_ID'] = CTools::GUID();
            $arFields['DATE_CREATE'] = new FieldType\DateTime();

            $result = Bitrix\Crm\WebinarTable::add($arFields);
            $ID = $result->getId();
        }
        if($result->isSuccess())
        {
            if($request["save"] <> '')
                LocalRedirect($arParams['PATH_TO_WEBINAR_LIST']);
        }
        else
        {
            $errors = $result->getErrorMessages();
        }
    }
}
else {
    //Получить инфу о вебинаре
}
$this->IncludeComponentTemplate();
?>
