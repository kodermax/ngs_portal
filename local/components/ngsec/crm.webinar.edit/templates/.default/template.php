<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
?>
<div class="ui menu">
    <a class="item" href="<?=$arParams['PATH_TO_WEBINAR_LIST']?>">
        <i class="large grid layout purple icon"></i>
        Список
    </a>
    <a class="item" href="<?=$arParams['PATH_TO_WEBINAR_SHOW']?>">
        <i class="large search black icon"></i>
        Просмотр
    </a>
</div>
    <div class="ui form">
        <form action="<?=CComponentEngine::MakePathFromTemplate(
            $arParams["PATH_TO_WEBINAR_VIEW"],
            array('webinar_id' => $arParams['WEBINAR_ID'])
        );?>" method="POST" name="add_form">
        <div class="ui left floated segment">
            <?if($arParams["ELEMENT_ID"] > 0):?>
                <h2>Редактирование вебинара</h2>
            <?else:?>
                <h2>Добавление вебинара</h2>
            <?endif;?>
            <div class="field">
                <label for="TITLE">Наименование</label>
                <input id="TITLE" name="TITLE" type="text" value="<?=$arResult['ITEM']['TITLE']?>"/>
            </div>
            <div class="field">
                <label for="DATE_START">Дата проведения:</label>
                <div class="ui icon input">
                <input type="text" id="DATE_START" name="DATE_START" value="<?=$arResult['ITEM']['DATE_START']?>"/>
                <i class="icon">
                <?
                $APPLICATION->IncludeComponent(
                    'bitrix:main.calendar',
                    '',
                    array(
                        'FORM_NAME' => 'add_form',
                        'INPUT_NAME' => "DATE_START",
                        'INPUT_VALUE' => '',
                    ),
                    null,
                    array('HIDE_ICONS' => 'Y')
                );
                ?>
                </i>
                </div>
            </div>
            <div class="field">
                <label for="RESPONSE">Ответственный</label>
                <input id="RESPONSE" name="RESPONSE" type="text" value="<?=$arResult['ITEM']['RESPONSIBLE_FULL_NAME']?>"/>
                <?$APPLICATION->IncludeComponent(
                    "bitrix:intranet.user.selector.new",
                    "",
                    Array(
                        "NAME" => "response_id",
                        "INPUT_NAME" => "RESPONSE",
                        "SHOW_LOGIN" => "Y",
                        "VALUE" => array($arResult['ITEM']['RESPONSIBLE_ID'])
                    )
                );?>
            </div>
            <div class="field">
                <label for="COMMENTS">Комментарий:</label>
                <textarea name="COMMENTS"><?=$arResult['ITEM']['COMMENTS']?></textarea>
            </div>
            <div class="field">
                <input type="submit" name="save" value="Сохранить" class="ui positive button">
                &nbsp;&nbsp;
                <input type="reset" name="cancel" value="Отмена" class="ui button">
            </div>
        </div>
            </form>
    </div>
<script type="text/javascript">

    $(document).ready(function() {
        var validationRules = {
            Title: {
                identifier: 'TITLE',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Вы должны заполнить это поле'
                    }
                ]
            },
            Response: {
                identifier: 'RESPONSE',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Вы должны заполнить это поле'
                    }
                ]
            },
            DateStart: {
                identifier: 'DATE_START',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Вы должны заполнить это поле'
                    }
                ]
            }
        };
        var settingsRules = {
            inline : true,
            on     : 'blur'
        };
        $('.ui.form').form(validationRules,settingsRules);
    });
</script>