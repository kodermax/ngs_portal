<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>
<meta http-equiv="Content-type" content="text/html;charset=<?echo LANG_CHARSET?>" />
<style type="text/css">
    .report-red-neg-val { color: red; }
</style>

<table border="1">
    <thead>
    <tr>
        <th>Лид</th>
        <th>Контакт</th>
        <th>Компания</th>
        <th>Задача</th>
        <th>Ответственный</th>
        <th>Комментарии</th>
        <th>E-mail</th>
    </tr>
    </thead>
    <tbody>
    <?foreach($arResult['LEADS'] as $key => $arItem):?>
        <tr>
            <td><a target="_blank" href="<?=$arItem["LEAD_URL_SHOW"]?>"><?=$arItem['LEAD_TITLE']?></a></td>
            <td>
                <?if($arItem['CONTACT_ID'] > 0):?>
                    <a target="_blank" href="<?=$arItem['CONTACT_URL_SHOW']?>"><?=$arItem['CONTACT_FULL_NAME']?></a>
                <?endif;?>
            </td>
            <td>
                <?if($arItem['COMPANY_ID'] > 0):?>
                    <a target="_blank" href="<?=$arItem['COMPANY_URL_SHOW']?>"><?=$arItem['COMPANY_TITLE']?></a>
                <?endif;?>
            </td>
            <td align="center"<? if($arItem['TASK_IS_OVERDUE'] == 1):?> class="error" <? elseif($arItem['TASK_IS_FINISHED'] == 1):?>class="positive"<? endif; ?>>
                <?if($arItem['TASK_ID'] > 0):?>
                    <a target="_blank" href="<?=$arItem['TASK_URL_SHOW']?>" title="<?=$arItem['TASK_TITLE']?>">Задача № <?=$arItem['TASK_ID']?></a>
                <?elseif($arItem['CONTACT_ID'] > 0):?>
                    <i data-value="<?=$arItem['ID']?>" title="Создать задачу: Получить обратную связь" class="add link icon"></i>
                <?endif;?>
            </td>
            <td>
                <?if($arItem['TASK_ID'] > 0):?>
                    <?=$arItem["TASK_RESPONSIBLE_FULL_NAME"];?>
                <?endif;?>
            </td>
            <td>
                <?if (count($arItem['COMMENTS']) > 0):?>
                    <div class="ui comments">
                        <?foreach($arItem['COMMENTS'] as $arComment):?>
                            <div class="comment">
                                <div class="content">
                                    <a class="author"><?=$arComment['AUTHOR']?></a>
                                    <div class="metadata">
                                        <span class="date"><?=$arComment['POST_DATE']?></span>
                                    </div>
                                    <div class="text">
                                        <?=$arComment['MESSAGE']?>
                                    </div>
                                </div>
                            </div>
                        <?endforeach;?>
                    </div>
                <?endif;?>
            </td>
            <td><?=$arItem['CRM_WEBINAR_LEAD_LEAD_EMAIL_WORK']?></td>
        </tr>
    <?endforeach;?>
    </tbody>
</table>