<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
?>
<div class="ui menu">
    <a class="item" href="<?=$arParams['PATH_TO_WEBINAR_ADD']?>">
        <i class="large add green icon"></i>
        Добавить
    </a>
    <a class="item" href="<?=$arParams['PATH_TO_WEBINAR_EDIT']?>">
        <i class="large edit black icon"></i>
        Редактировать
    </a>
    <a class="item" href="<?=$arParams['PATH_TO_WEBINAR_LIST']?>">
        <i class="large grid layout purple icon"></i>
        Список
    </a>
    <a class="item" href="<?=$arParams['PATH_TO_WEBINAR_IMPORT']?>">
        <i class="large download red icon"></i>
        Импорт лидов
    </a>
    <a class="item" href="<?=$arParams['PATH_TO_SAVE_EXCEL']?>">
        <i class="large save blue icon"></i>
        Сохранить Excel
    </a>
</div>
   <div class="ui form segment">
       <?if($GLOBALS["USER"]->isAdmin()):?>
           <div class="field">
               <b>GUID:</b> <?=$arResult['ITEM']['ORIGIN_ID']?>
           </div>
       <?endif;?>
        <div class="field">
           <b>Наименование:</b> <?=$arResult['ITEM']['TITLE']?>
        </div>
        <div class="field">
            <b>Дата проведения:</b> <?=$arResult['ITEM']['DATE_START']?>
        </div>
        <div class="field">
            <b>Ответственный:</b> <?=$arResult['ITEM']['RESPONSIBLE_FORMAT_NAME']?>
        </div>
       <div class="field">
           <b>Кол-во участников:</b> <?=count($arResult['LEADS'])?>
       </div>

       <div class="field">
            <b>Комментарий:</b> <?=$arResult['ITEM']['COMMENTS'] ? $arResult['ITEM']['COMMENTS'] : '-'?>
        </div>
    </div>

       <table class="ui celled table segment">
           <thead>
            <tr>
                <th>Лид</th>
                <th>Сконвертирован?</th>
                <th>Контакт</th>
                <th>Компания</th>
                <th>Задача</th>
                <th>Ответственный</th>
                <th>Комментарии</th>
                <th>E-mail</th>
            </tr>
           </thead>
           <tbody>
                <?foreach($arResult['LEADS'] as $key => $arItem):?>
                    <tr>
                        <td><a target="_blank" href="<?=$arItem["LEAD_URL_SHOW"]?>"><?=$arItem['RLEAD_TITLE']?></a></td>
                        <td style="text-align:center">
                            <?if((int)$arItem['RLEAD_IS_CONVERT'] == 1):?>
                                да
                            <?else:?>
                                <div class="ui left pointing dropdown icon button">
                                    <i class="settings icon"></i>
                                    <div class="menu">
                                        <a class="item" target="_blank" href="<?=$arItem["LEAD_URL_CONVERT"];?>" title="Конвертировать?"><i class="upload icon"></i>Сконвертировать</a>
                                        <div class="item taskForAleksey" data-value="<?=$arItem['ID']?>" title="Обработка неклассифицированного лида для Комарова"><i class="male icon"></i>Комаров</div>
                                        <div class="item taskForElena" data-value="<?=$arItem['ID']?>" title="Обработка неклассифицированного лида для Выходцевой"><i class="female icon"></i>Выходцева</div>
                                    </div>
                                </div>
                            <?endif;?>
                        </td>
                        <td>
                            <?if($arItem['CONTACT_ID'] > 0):?>
                                <a target="_blank" href="<?=$arItem['CONTACT_URL_SHOW']?>"><?=$arItem['CONTACT_FULL_NAME']?></a>
                            <?endif;?>
                        </td>
                        <td>
                            <?if($arItem['COMPANY_ID'] > 0):?>
                                <a target="_blank" href="<?=$arItem['COMPANY_URL_SHOW']?>"><?=$arItem['COMPANY_TITLE']?></a>
                            <?endif;?>
                        </td>
                        <td align="center"<? if($arItem['TASK_IS_FINISHED'] == 1):?> class="positive" <? elseif($arItem['TASK_IS_OVERDUE'] == 1):?>class="error"<? endif; ?>>
                            <?if($arItem['TASK_ID'] > 0):?>
                                <a class="nowrap" target="_blank" href="<?=$arItem['TASK_URL_SHOW']?>" title="<?=$arItem['TASK_TITLE']?>">№ <?=$arItem['TASK_ID']?></a>
                            <?elseif($arItem['CONTACT_ID'] > 0):?>
                                <i data-value="<?=$arItem['ID']?>" title="Создать задачу: Получить обратную связь" class="add link icon"></i>
                            <?endif;?>
                        </td>
                        <td>
                            <?if($arItem['TASK_ID'] > 0):?>
                                <?=$arItem['TASK_RESPONSIBLE_FULL_NAME']?>
                            <?endif;?>
                        </td>
                        <td>
                            <?if (count($arItem['COMMENTS']) > 0):?>
                            <div class="ui comments">
                                <?foreach($arItem['COMMENTS'] as $arComment):?>
                                <div class="comment">
                                    <div class="content">
                                        <a class="author"><?=$arComment['AUTHOR']?></a>
                                        <div class="metadata">
                                            <span class="date"><?=$arComment['POST_DATE']?></span>
                                        </div>
                                        <div class="text">
                                            <?=$arComment['MESSAGE']?>
                                        </div>
                                    </div>
                                </div>
                                <?endforeach;?>
                            </div>
                        <?endif;?>
                        </td>
                        <td><?=$arItem['RLEAD_EMAIL_WORK']?></td>
                    </tr>
                <?endforeach;?>
           </tbody>
       </table>
<div class="ui modal taskModal">
    <i class="close icon"></i>
    <div class="header">
        Создать задачу: <span class="taskTitle"></span>
    </div>
    <div class="content">
        <div class="ui dimmer modalLoader"><div class="ui mini text loader">Идёт создание задачи</div></div>
            <p>
                <b>Ответственный:</b> <span class="responsible"></span>
            </p>
    </div>
    <div class="actions">
        <div class="two fluid ui buttons">
            <div class="ui positive right labeled icon button">
                Да
                <i class="checkmark icon"></i>
            </div>
            <div class="ui negative labeled icon button">
                <i class="remove icon"></i>
                Нет
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    var leads = <?=CUtil::PhpToJSObject($arResult['LEADS'])?>;
    var webinarGuid = '<?=$arResult['WEBINAR_GUID']?>';
    function addTaskFromWebinar(element, type)
    {
        var leadId = $(element).attr('data-value');
        var taskTitle = $('.ui.modal .taskTitle');
        var responsibleText = $('.ui.modal .responsible');
        switch (type)
        {
            case 0:
                taskTitle.text('Получить обратную связь от ' + leads[leadId].LEAD_FULL_NAME);
                break;
            case 1:
            case 2:
                taskTitle.text('Обработать неклассифицированный лид ' + leads[leadId].LEAD_FULL_NAME);
                break;

        }
        var responsibleId;
        switch (type)
        {
            case 0:
                responsibleText.text(leads[leadId].CONTACT_RESPONSIBLE_FULL_NAME);
                responsibleId = leads[leadId].CONTACT_RESPONSIBLE_ID;
                break;
            case 1:
                responsibleText.text("Алексей Комаров");
                responsibleId = 501;
                break;
            case 2:
                responsibleText.text("Елена Выходцева");
                responsibleId = 507;
                break;

        }
        $('.taskModal').modal('setting', {
            closable  : true,
            onDeny    : function(){
                return true;
            },
            onApprove : function() {
                $('.modalLoader').addClass('active');
                $.ajax({
                    type: "POST",
                    dataType: 'json',
                    url: "/local/components/ngsec/crm.webinar.show/ajax.php",
                    data: {MODE: 'ADD_TASK',RESPONSIBLE_ID: responsibleId, LEAD_ID: leads[leadId].LEAD_ID, CONTACT_ID: leads[leadId].CONTACT_ID,
                        COMPANY_ID: leads[leadId].COMPANY_ID, WEBINAR_GUID: webinarGuid }
                }).done(function( data ) {
                    if (data != null && data.TASK_ID > 0)
                    {
                        $('.modalLoader').removeClass('active');
                        $('.taskModal').modal('hide all');
                        alert('Задача успешно создана!');
                    }
                });
                return false;
            }
        }).modal('show');
    }
    $(document).ready(function() {
        $('.ui.dropdown').dropdown();
        $('.item.taskForAleksey').click(function()
        {
            addTaskFromWebinar(this, 1);
        });
        $('.item.taskForElena').click(function()
        {
            addTaskFromWebinar(this, 2);
        });
        $('.ui.modal').modal();
        $('.add.link.icon').click(function()
        {
               addTaskFromWebinar(this, 0);
        });

        $('.filter.menu .item').tab({history:false});
    });
</script>