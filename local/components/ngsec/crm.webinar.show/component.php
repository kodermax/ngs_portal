<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Entity;
global $APPLICATION;

$ELEMENT_ID = (int)$arParams['ELEMENT_ID'];
$excelView = isset($_GET["EXCEL"]) && $_GET["EXCEL"] == "Y";

$arParams['PATH_TO_WEBINAR_EDIT'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_WEBINAR_EDIT'],
    array(
        'webinar_id' => $ELEMENT_ID
    )
);
$arParams['PATH_TO_WEBINAR_IMPORT'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_WEBINAR_IMPORT'],
    array(
        'webinar_id' => $ELEMENT_ID
    )
);
$arParams['PATH_TO_WEBINAR_ADD'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_WEBINAR_ADD'],
    array(
        'webinar_id' => 0
    )
);

$arParams['PATH_TO_LEAD_SHOW'] = COption::GetOptionString('crm', strtolower("PATH_TO_LEAD_SHOW"));
$arParams['PATH_TO_CONTACT_SHOW'] = COption::GetOptionString('crm', strtolower("PATH_TO_CONTACT_SHOW"));
$arParams['PATH_TO_COMPANY_SHOW'] = COption::GetOptionString('crm', strtolower("PATH_TO_COMPANY_SHOW"));
$arParams['PATH_TO_LEAD_CONVERT'] = "/crm/lead/convert/#lead_id#/";
$arParams['PATH_TO_TASK_SHOW'] = "/company/personal/user/#user_id#/tasks/task/view/#task_id#/";
$arParams["PATH_TO_SAVE_EXCEL"] = $APPLICATION->GetCurPageParam("EXCEL=Y");
if ($ELEMENT_ID > 0)
{
    //Информация о вебинаре
    $entity = Bitrix\Crm\WebinarTable::getEntity();
    $query = new Entity\Query($entity);
    $query->setSelect(array('*','RESPONSIBLE_' => 'ASSIGNED_BY'));
    $query->setFilter(array('ID' => $ELEMENT_ID));
    $result = new CDBResult($query->exec());
    if ($row = $result->Fetch())
    {
        $row['RESPONSIBLE_FORMAT_NAME'] = CUser::FormatName(
            \Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
            array(
                'LOGIN' => '',
                'NAME' => $row['RESPONSIBLE_NAME'],
                'SECOND_NAME' => $row['RESPONSIBLE_SECOND_NAME'],
                'LAST_NAME' => $row['RESPONSIBLE_LAST_NAME']
            ),
            false,
            false
        );
       $arResult['ITEM'] = $row;
       $webinarGuid = $row['ORIGIN_ID'];
       $arResult['WEBINAR_GUID'] = $webinarGuid;
    }
    //Информация о лидах вебинара
    if (strlen($webinarGuid) > 0) {
        $entity = Bitrix\Crm\WebinarLeadTable::getEntity();

        $query = new Entity\Query($entity);
      //  $query->setSelect(array('*',,'CONTACT_ID', 'CONTACT_'=>'CONTACT','COMPANY_' => 'COMPANY', 'LEAD_' => 'LEAD','LEAD.IS_CONVERT','LEAD.PHONE_MOBILE','LEAD.PHONE_WORK','LEAD.EMAIL_HOME','LEAD.EMAIL_WORK','RESPONSIBLE_'=>'LEAD.ASSIGNED_BY'));
        $query->setSelect(array('*','CONTACT_ID','COMPANY_ID','RLEAD_' => 'LEAD','LEAD_ID','LEAD.IS_CONVERT','LEAD.PHONE_MOBILE','LEAD.PHONE_WORK','LEAD.EMAIL_HOME','LEAD.EMAIL_WORK','RESPONSIBLE_'=>'LEAD.ASSIGNED_BY'));
        $query->registerRuntimeField('CONTACT_ID' , array(
            'data_type' => 'integer',
            'expression' => array(
                '(SELECT CONTACT_ID FROM b_crm_lead WHERE ID = %s)', 'LEAD_ID'
            )
        ));
        $query->registerRuntimeField('COMPANY_ID' , array(
            'data_type' => 'integer',
            'expression' => array(
                '(SELECT COMPANY_ID FROM b_crm_lead WHERE ID = %s)', 'LEAD_ID'
            )
        ));

        $query->setFilter(array('WEBINAR_GUID' => $webinarGuid));
        $result = new CDBResult($query->exec());
        while ($row = $result->Fetch()) {
            $arContact = \Bitrix\Crm\WebinarLeadTable::getContactData($row['CONTACT_ID']);
            if ($arContact != null && is_array($arContact))
                $row = array_merge($arContact, $row);
            $arCompany = \Bitrix\Crm\WebinarLeadTable::getCompanyData($row['COMPANY_ID']);
            if(is_array($arCompany) && $arCompany != null)
                $row = array_merge($arCompany, $row);
            $row['LEAD_URL_SHOW'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_LEAD_SHOW'],
                array(
                    'lead_id' => $row['LEAD_ID']
                )
            );
            $row['LEAD_URL_CONVERT'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_LEAD_CONVERT'],
                array(
                    'lead_id' => $row['LEAD_ID']
                )
            );
            $row['LEAD_URL_CONVERT'] .= "?webinar_guid=".$arResult['WEBINAR_GUID'];
            $row['LEAD_FULL_NAME'] = CUser::FormatName(
                \Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
                array(
                    'LOGIN' => '',
                    'NAME' => $row['RLEAD_NAME'],
                    'SECOND_NAME' => $row['RLEAD_SECOND_NAME'],
                    'LAST_NAME' => $row['RLEAD_LAST_NAME']
                ),
                false,
                false
            );
            if($row['CONTACT_ID'] >  0) {
                //Поиск задачи для контакта
                $list = \Bitrix\Crm\WebinarTaskTable::getList(array(
                    'select' => array('TASK_ITEM_' => "TASK","TASK_ITEM_IS_OVERDUE"=>"TASK.IS_OVERDUE"),
                    'filter' => array('CONTACT_ID' => $row['CONTACT_ID'],'WEBINAR_GUID' => $webinarGuid)
                ));
                if ($arTask = $list->fetch())
                {
                    $row['TASK_ID'] = $arTask['TASK_ITEM_ID'];
                    $row = array_merge($row, getTaskComments($row['TASK_ID']));
                    $row['TASK_TITLE'] = $arTask['TASK_ITEM_TITLE'];
                    $row['TASK_IS_OVERDUE'] = $arTask['TASK_ITEM_IS_OVERDUE'];
                    $row['TASK_IS_FINISHED'] = $arTask['TASK_ITEM_CLOSED_DATE'] != null ? 1 : 0;
                    $rsUser = CUser::GetByID($arTask['TASK_ITEM_RESPONSIBLE_ID']);
                    $arUser = $rsUser->Fetch();
                    $row['TASK_RESPONSIBLE_FULL_NAME'] = $arUser['NAME'].' '.$arUser['LAST_NAME'];
                    $row['TASK_URL_SHOW'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_TASK_SHOW'],
                        array(
                            'task_id' => $arTask['TASK_ITEM_ID'],
                            'user_id' => $arTask['TASK_ITEM_RESPONSIBLE_ID']

                        )
                    );
                }

                $row['CONTACT_URL_SHOW'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_CONTACT_SHOW'],
                    array(
                        'contact_id' => $row['CONTACT_ID']
                    )
                );

                $row['CONTACT_FULL_NAME'] = CUser::FormatName(
                    \Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
                    array(
                        'LOGIN' => '',
                        'NAME' => $row['CONTACT_NAME'],
                        'SECOND_NAME' => $row['CONTACT_SECOND_NAME'],
                        'LAST_NAME' => $row['CONTACT_LAST_NAME']
                    ),
                    false,
                    false
                );
            }
            if($row['COMPANY_ID'] > 0)
            {
                $row['COMPANY_URL_SHOW'] = CComponentEngine::MakePathFromTemplate($arParams['PATH_TO_COMPANY_SHOW'],
                    array(
                        'company_id' => $row['COMPANY_ID']
                    )
                );

            }
            $row['RESPONSIBLE_FULL_NAME'] = CUser::FormatName(
                \Bitrix\Crm\Format\PersonNameFormatter::getFormat(),
                array(
                    'LOGIN' => '',
                    'NAME' => $row['RESPONSIBLE_NAME'],
                    'SECOND_NAME' => $row['RESPONSIBLE_SECOND_NAME'],
                    'LAST_NAME' => $row['RESPONSIBLE_LAST_NAME']
                ),
                false,
                false
            );
            $arResult['LEADS'][$row['ID']] = $row;
           }

    }

}
if ($excelView)
{
    $APPLICATION->RestartBuffer();
   /* Header("Content-Type: application/force-download");
    Header("Content-Type: application/octet-stream");
    Header("Content-Type: application/download");
    Header("Content-Disposition: attachment;filename=report.xls");
    Header("Content-Transfer-Encoding: binary");*/
    ob_start();
    $this->IncludeComponentTemplate('excel');
    $sVal = ob_get_contents();
    ob_end_clean();
    $arFile = array('MODULE_ID' => 'crm',"name"=>$arResult['ITEM']['TITLE']."xls", "tmp_name"=>$_SERVER['DOCUMENT_ROOT']."/bitrix/tmp/","content"=>$sVal, "size"=>strlen($sVal));
    $fid = CFile::SaveFile($arFile, "webinars");
    $path = CFile::GetPath($fid);
    LocalRedirect($path);
    exit;
}
else
{
    $this->IncludeComponentTemplate();
}


function getTaskComments($taskId)
{
    $arResult = array();
    $query = new \Bitrix\Main\Entity\Query(\Bitrix\Tasks\TaskTable::getEntity());
    $query->setSelect(array('ID',"DESCRIPTION",'FORUM_TOPIC_ID','IS_OVERDUE'));
    $query->setFilter(array('ID' => $taskId));
    $query->registerRuntimeField('FORUM_TOPIC_ID',
        array(
            'data_type' => 'integer'
        ));
    $result = new CDBResult($query->exec());
    $forumTopicId = 0;
    $status = 0;
    if($row = $result->fetch())
    {
        if($row['IS_OVERDUE'])
            $status = "overdue";
        if ($row['IS_FINISHED'])
            $status = "closed";
        if ($row['IS_OVERDUE'] == 0 && $row['IS_FINISHED'] == 0)
            $status = "opened";

        $arResult = array("ID" => $row['ID'],"TITLE" => $row['DESCRIPTION'], "STATUS" => $status);
        $forumTopicId = $row['FORUM_TOPIC_ID'];
    }
    if ($forumTopicId > 0)
    {
        \Bitrix\Main\Loader::includeModule('forum');
        $db_res = CForumMessage::GetList(array("ID"=>"ASC"), array("NEW_TOPIC"=>"N","TOPIC_ID"=>$forumTopicId));
        while ($ar_res = $db_res->Fetch())
        {
            $parser = new textParser();
            $ar_res["POST_MESSAGE"] = $parser->convert($ar_res["POST_MESSAGE"]);
            $arResult["COMMENTS"][] = array("POST_DATE"=>$ar_res['POST_DATE'],"MESSAGE" => $ar_res["POST_MESSAGE"], "ID" => $ar_res["ID"],"AUTHOR" => $ar_res["AUTHOR_NAME"]);
        }
    }
    return $arResult;
}
?>
