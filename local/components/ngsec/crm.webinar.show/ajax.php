<?
define('STOP_STATISTICS', true);
define('BX_SECURITY_SHOW_MESSAGE', true);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

/*
 * ONLY 'POST' SUPPORTED
 * SUPPORTED MODES:
 * 'UPDATE' - update deal field
 */
global $USER, $APPLICATION;

if (!$USER->IsAuthorized() ||  $_SERVER['REQUEST_METHOD'] != 'POST')
{
    return;
}
CUtil::JSPostUnescape();
$APPLICATION->RestartBuffer();
Header('Content-Type: application/x-javascript; charset='.LANG_CHARSET);

$mode = isset($_POST['MODE']) ? $_POST['MODE'] : '';
if(!isset($mode[0]))
{
    echo CUtil::PhpToJSObject(array('ERROR'=>'MODE IS NOT DEFINED!'));
    die();
}
if($mode === 'ADD_TASK')
{
    $taskId = \Bitrix\Crm\WebinarTaskTable::createTaskForContact($_POST["TASK_TITLE"], $_POST['LEAD_ID'], $_POST['CONTACT_ID'], $_POST['COMPANY_ID'],$_POST['RESPONSIBLE_ID'], $_POST['WEBINAR_GUID']);
    echo json_encode(array('TASK_ID' => $taskId));
    die();
}



