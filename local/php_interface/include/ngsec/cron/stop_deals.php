<?php
$_SERVER["DOCUMENT_ROOT"] = realpath(dirname(__FILE__)."/../../../../..");
$DOCUMENT_ROOT = $_SERVER["DOCUMENT_ROOT"];
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define("BX_CRONTAB", true);
define('BX_NO_ACCELERATOR_RESET', true);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

@set_time_limit(0);
@ignore_user_abort(true);

use Bitrix\Main;
use Bitrix\Main\Entity;
use Bitrix\Main\Application;
use Bitrix\Crm;

\Bitrix\Main\Loader::includeModule('crm');
\Bitrix\Main\Loader::includeModule('currency');
//ищем неактивные сделки
$arrAdd = array(
    "DD"	=> 0,
    "MM"	=> -1,
    "YYYY"	=> 0,
    "HH"	=> 0,
    "MI"	=> 0,
    "SS"	=> 0,
);
$monthAgo = AddToTimeStamp($arrAdd, time()); // две недели назад
$entity = \Bitrix\Crm\DealTable::getEntity();
$query = new Entity\Query($entity);
$query->setSelect(array('ID','RESPONSIBLE_' => 'ASSIGNED_BY', 'VENDOR_' => 'VENDOR','TITLE','DATE_MODIFY','OPPORTUNITY','CURRENCY_ID',
    'STAGE_BY_' => 'STAGE_BY'))
    ->setFilter(
        array(
            'STAGE_ID' => 'ON_HOLD'
        ));
$query->registerRuntimeField(
    'VENDOR',
    array(
        'data_type' => 'uts_crm_deal',
        'reference' => array('=this.ID' => 'ref.VALUE_ID')));
$options = null;
$result = new CDBResult($query->exec());
$arDeals = array();
$arDealIDs = array();
$listDeals = array();
while ($row = $result->fetch())
{
    $url = CComponentEngine::MakePathFromTemplate("https://".SITE_SERVER_NAME.COption::GetOptionString('crm', 'path_to_deal_show'),
        array(
            'deal_id' => $row['ID']
        )
    );
    if ($row['VENDOR_UF_CRM_DEAL_VENDOR'] > 0)
    {

        $companyResult = Crm\CompanyTable::getList(
            array(
                'filter' => array('ID' => $row['VENDOR_UF_CRM_DEAL_VENDOR']),
                'select' => array('TITLE','ASSIGNED_BY_ID','RESPONSIBLE_' => 'ASSIGNED_BY'),
                'runtime' => array(
                    'ASSIGNED_BY_ID' => array('data_type' => 'integer'),
                    'ASSIGNED_BY' => array('data_type' =>'\Bitrix\Main\User','reference' =>array('=this.ASSIGNED_BY_ID' => 'ref.ID'))
                )
            )
        );
        if ($companyRow = $companyResult->fetch())
        {
            $row['VENDOR_ASSIGNED'] = $companyRow['RESPONSIBLE__EMAIL'];
            $row['VENDOR_ASSIGNED_ID'] = $companyRow['ASSIGNED_BY_ID'];
            $row['VENDOR_NAME'] = $companyRow['TITLE'];
        }
    }
    $row['STATUS'] = $row['STAGE_BY_NAME'];
    $row['SUM'] = CurrencyFormat($row['OPPORTUNITY'], strlen($row['CURRENCY_ID']) > 0 ? $row['CURRENCY_ID'] : 'RUB');
    $row["URL"] = $url;
    $arDeals[$row['RESPONSIBLE_ID']][$row['ID']] = $row;
}
foreach($arDeals as $userId => $arDeal)
{
    SendDealReport($userId, $arDeal);
}


print "Done!";

function SendDealReport($userId, $arData, $arDataKZ = array())
{
    $HTML_HEADER = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<style>
			.tablehead1 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;}
			.tablehead2 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;}
			.tablehead3 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-right: 1px solid #A8C2D7;}
			.tablebody1 {background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody2 {background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody3 {background-color:#F0F1F2; padding:2px; border-right:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebodytext {font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;}
			.tableheadtext {font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;}
			.tablelinebottom {border-bottom:1pt solid #D1D1D1}
            .notesmall {font-family: Arial, Helvetica, sans-serif; font-size:11px; color:#008400; font-weight:normal;}
			.tablebody1_sel {background-color:#E0EBF1; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody2_sel {background-color:#E0EBF1; padding:2px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody3_sel {background-color:#E0EBF1; padding:2px; border-right:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			</style>
			</head>
			<body bgcolor="FFFFFF" leftmargin="2" topmargin="2" marginwidth="2" marginheight="2">';
    $BODY = '';
    if (count($arData) > 0) {
        $TABLE_HEADER = '
                <b>Неактивные сделки:</b>
                <table border="0" cellspacing="1" cellpadding="3" width="750">
                                    <tr>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Название</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Вендор</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Статус регистрации</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Сумма</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Ответственный</b></span></td>
                                    </tr>
                                    ';
        $TABLE_DATA = '';
        foreach ($arData as $arItem) {
            $TABLE_DATA .= '
                       <tr valign="top">
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><a href="' . $arItem["URL"] . '">' . $arItem["TITLE"] . '</a></span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">' . $arItem['VENDOR_NAME'] . '</span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">' . $arItem['STATUS'] . '</span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">' . $arItem['SUM'] . '</span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">' . $arItem['RESPONSIBLE_NAME'] . " " . $arItem['RESPONSIBLE_LAST_NAME'] . '</span></td>
                        </tr>
                        ';

        }
        $TABLE_FOOTER = '</table>';
        $BODY .= $TABLE_HEADER . $TABLE_DATA . $TABLE_FOOTER;
    }
    if(count($arDataKZ) > 0)
    {
        $TABLE_HEADER = '
                <b>Неактивные сделки Казахстана:</b>
                <table border="0" cellspacing="1" cellpadding="3" width="750">
                                    <tr>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Название</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Дата следующего взаимодействия</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Вендор</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Статус регистрации</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Сумма</b></span></td>
                                        <td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Ответственный</b></span></td>
                                    </tr>
                                    ';
        $TABLE_DATA = '';
        foreach($arDataKZ as $arItem)
        {
            $TABLE_DATA .= '
                       <tr valign="top">
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><a href="'.$arItem["URL"].'">'.$arItem["TITLE"].'</a></span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem['DATE_NEXT'].'</span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem['VENDOR_NAME'].'</span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem['STATUS'].'</span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem['SUM'].'</span></td>
                            <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem['RESPONSIBLE_NAME']. " ".$arItem['RESPONSIBLE_LAST_NAME'].'</span></td>
                        </tr>
                        ';

        }
        $TABLE_FOOTER = '</table>';
        $BODY .= $TABLE_HEADER.$TABLE_DATA.$TABLE_FOOTER;
    }


    $HTML_FOOTER = '</body>
                        </html>';
    $now_full_date = GetTime(time(), "FULL", "s1", true);
    $arEventFields = array(
        "EMAIL_TO" => CTools::GetByEmail($userId),
        "HTML_HEADER" => $HTML_HEADER,
        "BODY"        => $BODY,
        "HTML_FOOTER" => $HTML_FOOTER,
        "SERVER_TIME" => $now_full_date,
        "USER_NAME"   => CTools::GetUserFullNameById($userId)
    );
    CEvent::Send("DEAL_REPORT_NA", "s1", $arEventFields);
}




