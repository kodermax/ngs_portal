<?php
/**
 * Created by PhpStorm.
 * User: m.karpychev
 * Date: 22.10.13
 * Time: 16:44 
 */

class CMyTasks {

    function __construct()
    {
        if (!CModule::IncludeModule("tasks")) return;
    }
    function SendReportITDepartment(){
        $arSections = CIntranetUtils::getSubDepartments(5431);
        $arDepartments = array(5431);
        $arDepartments = array_merge($arSections, $arDepartments);
        $arFilter = array("ACTIVE" => "Y",'UF_DEPARTMENT' => $arDepartments);
        $rsUsers = CUser::GetList($by="id", $order="desc", $arFilter);
        while ($arUser = $rsUsers->Fetch())
        {
         $arUsers[] = $arUser['ID'];
        }
        $this->SendReportForManager(489, $arUsers);
        $this->SendReportForManager(584, $arUsers);
    }
    function SendReportAllUsers()
    {
        $arFilter = array("ACTIVE" => "Y");
        $rsUsers = CUser::GetList($by="id", $order="desc", $arFilter);
        while ($arUser = $rsUsers->Fetch())
        {
            $this->SendWeeklyReport($arUser["ID"]);
        }
    }
    function GetFinishedTasks($userId){
        $arSelect = array(
            // basic task's data:
            'ID', 'TITLE', 'PRIORITY', 'STATUS', 'REAL_STATUS', 'MULTITASK',
            'DATE_START', 'GROUP_ID', 'DEADLINE',
            'ALLOW_TIME_TRACKING', 'TIME_ESTIMATE', 'TIME_SPENT_IN_LOGS',
            'COMMENTS_COUNT', 'FILES', 'MARK', 'ADD_IN_REPORT', 'SUBORDINATE',
            'CREATED_DATE', 'VIEWED_DATE', 'FORUM_TOPIC_ID', 'END_DATE_PLAN',
            'START_DATE_PLAN', 'CLOSED_DATE', 'PARENT_ID', 'ALLOW_CHANGE_DEADLINE',
            'ALLOW_TIME_TRACKING', 'CHANGED_DATE',
            // ORIGINATOR data:
            'CREATED_BY', 'CREATED_BY_NAME', 'CREATED_BY_LAST_NAME', 'CREATED_BY_SECOND_NAME',
            'CREATED_BY_LOGIN', 'CREATED_BY_WORK_POSITION', 'CREATED_BY_PHOTO',
            // RESPONSIBLE data:
            'RESPONSIBLE_ID', 'RESPONSIBLE_NAME', 'RESPONSIBLE_LAST_NAME', 'RESPONSIBLE_SECOND_NAME',
            'RESPONSIBLE_LOGIN', 'RESPONSIBLE_WORK_POSITION', 'RESPONSIBLE_PHOTO',
            // extra data
            // TODO: try to select it only when such columns are presents in list
            'UF_CRM_TASK'
        );
        $date = new DateTime("now");
        $date = $date->sub(new DateInterval('P30D'));
        $date = $date->format('d.m.Y');
        $arOrder = array("STATUS" => "ASC");
        $arFilter = array("STATUS"=> 5,'!DEADLINE' => false, ">=CLOSED_DATE" => $date, "RESPONSIBLE_ID" => $userId);
        list($arTaskItems, $rsItems) = CTaskItem::fetchList($userId, $arOrder, $arFilter, false, $arSelect);
        $arResult = array();
        $rsUser = CUser::GetByID($userId);
        $arUser = $rsUser->Fetch();
        foreach ($arTaskItems as $oTaskItem)
        {
            $task = $oTaskItem->getData();
            $pathToTask = CTaskNotifications::GetNotificationPath($arUser, $task["ID"], true, array("s1"));
            $deadLine = MakeTimeStamp($task['DEADLINE'], "DD.MM.YYYY HH:MI:SS");
            $closedDate = MakeTimeStamp($task['CLOSED_DATE'], "DD.MM.YYYY HH:MI:SS");
            if ($closedDate > $deadLine) {
                $arResult[] = array("TITLE" => $task['TITLE'], "URL" => $pathToTask,
                    "RESPONSIBLE" => $task["RESPONSIBLE_NAME"] . " " . $task["RESPONSIBLE_LAST_NAME"],
                    "AUTHOR" => $task["CREATED_BY_NAME"] . " " . $task["CREATED_BY_LAST_NAME"],
                    "DEADLINE" => $task["DEADLINE"],
                    'CLOSED_TIME' => ConvertDateTime($task['CLOSED_DATE'], 'HH:MI'),
                    'CLOSED_DATE' => ConvertDateTime($task['CLOSED_DATE'], 'DD.MM.YYYY'),
                    "DEADLINE_DATE" => ConvertDateTime($task['DEADLINE'], 'DD.MM.YYYY'),
                    "DEADLINE_TIME" => ConvertDateTime($task['DEADLINE'], 'HH:MI'),
                );
            }
        }
        return $arResult;
    }
    function GetOverdueTasks($userId)
    {
        $arSelect = array(
            // basic task's data:
            'ID', 'TITLE', 'PRIORITY', 'STATUS', 'REAL_STATUS', 'MULTITASK',
            'DATE_START', 'GROUP_ID', 'DEADLINE',
            'ALLOW_TIME_TRACKING', 'TIME_ESTIMATE', 'TIME_SPENT_IN_LOGS',
            'COMMENTS_COUNT', 'FILES', 'MARK', 'ADD_IN_REPORT', 'SUBORDINATE',
            'CREATED_DATE', 'VIEWED_DATE', 'FORUM_TOPIC_ID', 'END_DATE_PLAN',
            'START_DATE_PLAN', 'CLOSED_DATE', 'PARENT_ID', 'ALLOW_CHANGE_DEADLINE',
            'ALLOW_TIME_TRACKING', 'CHANGED_DATE',
            // ORIGINATOR data:
            'CREATED_BY', 'CREATED_BY_NAME', 'CREATED_BY_LAST_NAME', 'CREATED_BY_SECOND_NAME',
            'CREATED_BY_LOGIN', 'CREATED_BY_WORK_POSITION', 'CREATED_BY_PHOTO',
            // RESPONSIBLE data:
            'RESPONSIBLE_ID', 'RESPONSIBLE_NAME', 'RESPONSIBLE_LAST_NAME', 'RESPONSIBLE_SECOND_NAME',
            'RESPONSIBLE_LOGIN', 'RESPONSIBLE_WORK_POSITION', 'RESPONSIBLE_PHOTO',
            // extra data
            // TODO: try to select it only when such columns are presents in list
            'UF_CRM_TASK'
        );
        $arOrder = array("STATUS" => "ASC");
        $arFilter = array("STATUS"=> -1,  "RESPONSIBLE_ID" => $userId);
        list($arTaskItems, $rsItems) = CTaskItem::fetchList($userId, $arOrder, $arFilter, false, $arSelect);
        $arResult = array();
        $rsUser = CUser::GetByID($userId);
        $arUser = $rsUser->Fetch();
        foreach ($arTaskItems as $oTaskItem)
        {
            $task = $oTaskItem->getData();
            $pathToTask = CTaskNotifications::GetNotificationPath($arUser, $task["ID"], true, array("s1"));
            $arResult[] = array("TITLE" => $task['TITLE'], "URL" => $pathToTask,
                "RESPONSIBLE" => $task["RESPONSIBLE_NAME"] . " ". $task["RESPONSIBLE_LAST_NAME"],
                "AUTHOR" => $task["CREATED_BY_NAME"] . " ". $task["CREATED_BY_LAST_NAME"],
                "DEADLINE" => $task["DEADLINE"],
                "DEADLINE_DATE" => ConvertDateTime($task['DEADLINE'], 'DD.MM.YYYY'),
                "DEADLINE_TIME" => ConvertDateTime($task['DEADLINE'], 'HH:MI'),

            );
        }
        return $arResult;
    }

    function SendReportForManager($managerId, $arUsers){
        $HTML_HEADER = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<style>
			.tablehead1 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;}
			.tablehead2 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;}
			.tablehead3 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-right: 1px solid #A8C2D7;}
			.tablebody1 {background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody2 {background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody3 {background-color:#F0F1F2; padding:2px; border-right:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebodytext {font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;}
			.tableheadtext {font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;}
			.tablelinebottom {border-bottom:1pt solid #D1D1D1}
                                    .notesmall {font-family: Arial, Helvetica, sans-serif; font-size:11px; color:#008400; font-weight:normal;}
			.tablebody1_sel {background-color:#E0EBF1; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody2_sel {background-color:#E0EBF1; padding:2px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody3_sel {background-color:#E0EBF1; padding:2px; border-right:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			</style>
			</head>
			<body bgcolor="FFFFFF" leftmargin="2" topmargin="2" marginwidth="2" marginheight="2">
			<font class="h2">Отчет по задачам Технического департамента<br>
                Данные на <font color="#0D716F">'.FormatDate('d F Y',time()).'</font></font>
            <br>
            <hr><br>';

        $TABLE_HEADER = '<font class="h2">Незавершенные задачи, у которых истек крайний срок:<br><table border="0" cellspacing="1" cellpadding="3" width="750">
						<tr>
							<td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Название</b></span></td>
							<td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="120"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Крайний срок</b></span></td>
							<td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Ответственный</b></span></td>
							<td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Постановщик</b></span></td>
						</tr>';
        $arTaskItems = array();
        foreach($arUsers as $userId) {
            $items = $this->GetOverdueTasks($userId);
            $arTaskItems = array_merge($arTaskItems, $items);
        }
        $TABLE_DATA = '';
        foreach($arTaskItems as $arItem)
        {

            $TABLE_DATA .= '
            <tr valign="top">
                <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><a href="'.$arItem["URL"].'">'.$arItem["TITLE"].'</a></span></td>
                <td valign="top" align="right" style="text-align:center;background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="120"><span style="text-align:center;font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>'.$arItem["DEADLINE_DATE"].'</b><br/>'.$arItem['DEADLINE_TIME'].'</span></td>
                <td valign="top" align="right" style="background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem["RESPONSIBLE"].'</span></td>
                <td valign="top" align="right" style="background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem["AUTHOR"].'</span></td>
            </tr>';

        }
        $TABLE_FOOTER = '</table>';
        $arTaskItems = array();
        foreach($arUsers as $userId) {
            $items = $this->GetFinishedTasks($userId);
            $arTaskItems = array_merge($arTaskItems, $items);
        }
        $DATA = '';
        foreach($arTaskItems as $arItem)
        {
            $DATA .= '
            <tr valign="top">
                <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><a href="'.$arItem["URL"].'">'.$arItem["TITLE"].'</a></span></td>
                <td valign="top" align="right" style="text-align:center;background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="120"><span style="text-align:center;font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>'.$arItem["DEADLINE_DATE"].'</b><br/>'.$arItem['DEADLINE_TIME'].'</span></td>
                <td valign="top" align="right" style="text-align:center;background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="120"><span style="text-align:center;font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>'.$arItem["CLOSED_DATE"].'</b><br/>'.$arItem['CLOSED_TIME'].'</span></td>
                <td valign="top" align="right" style="background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem["RESPONSIBLE"].'</span></td>
                <td valign="top" align="right" style="background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem["AUTHOR"].'</span></td>
            </tr>';

        }
        $HTML_FOOTER = '<br/><br/><font class="h2">Завершенные задачи за последние 30 дней, которые не были завершены в срок:<br>
                        <table border="0" cellspacing="1" cellpadding="3" width="750">
						<tr>
							<td valign="center" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Название</b></span></td>
							<td valign="center" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="120"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Крайний срок</b></span></td>
							<td valign="center" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="120"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Дата завершения</b></span></td>
							<td valign="center" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Ответственный</b></span></td>
							<td valign="center" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Постановщик</b></span></td>
						</tr>'.$DATA.'
						</table>
						</body>
                        </html>';
        $now_full_date = GetTime(time(), "FULL", "s1", true);
        $arEventFields = array(
            "EMAIL_TO" => CTools::GetByEmail($managerId),
            "HTML_HEADER" => $HTML_HEADER,
            "TABLE_HEADER" => $TABLE_HEADER,
            "TABLE_DATA" => $TABLE_DATA,
            "TABLE_FOOTER" => $TABLE_FOOTER,
            "HTML_FOOTER" => $HTML_FOOTER,
            "SERVER_TIME" => $now_full_date,
            "USER_NAME"   => CTools::GetUserFullNameById($userId),
            'TITLE' => 'Отчет по задачам Технического департамента',
        );
        if (count($arTaskItems) > 0) {
            CEvent::Send("TASK_REPORT", "s1", $arEventFields);
            echo "<pre>";
            print_r($arEventFields);
            echo "</pre>";
        }
    }



    function SendWeeklyReport($userId)
    {
        $HTML_HEADER = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<style>
			.tablehead1 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;}
			.tablehead2 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;}
			.tablehead3 {background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-right: 1px solid #A8C2D7;}
			.tablebody1 {background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody2 {background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody3 {background-color:#F0F1F2; padding:2px; border-right:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebodytext {font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;}
			.tableheadtext {font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;}
			.tablelinebottom {border-bottom:1pt solid #D1D1D1}
                                    .notesmall {font-family: Arial, Helvetica, sans-serif; font-size:11px; color:#008400; font-weight:normal;}
			.tablebody1_sel {background-color:#E0EBF1; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody2_sel {background-color:#E0EBF1; padding:2px; border-bottom:#B9D3E6 solid 1px;}
			.tablebody3_sel {background-color:#E0EBF1; padding:2px; border-right:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;}
			</style>
			</head>
			<body bgcolor="FFFFFF" leftmargin="2" topmargin="2" marginwidth="2" marginheight="2">
			<font class="h2">Отчет по незавершенным задачам  пользователя <font color="#A52929">'.CTools::GetUserFullNameById($userId).'</font><br>
                Данные на <font color="#0D716F">'.GetTime(time(), "FULL", "s1", true).'</font></font>
            <br>
            <hr><br>';
        $TABLE_HEADER = '<table border="0" cellspacing="1" cellpadding="3" width="750">
						<tr>
							<td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7; border-left: 1px solid #A8C2D7;" width="300"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Название</b></span></td>
							<td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="120"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Крайний срок</b></span></td>
							<td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Ответственный</b></span></td>
							<td valign="top" align="center" style="background-color:#C2DBED; padding:2px; border-top: 1px solid #A8C2D7; border-bottom: 1px solid #A8C2D7;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><b>Постановщик</b></span></td>
						</tr>';
        $arTaskItems = $this->GetOverdueTasks($userId);
        $TABLE_DATA = '';
        foreach($arTaskItems as $arItem)
        {

            $TABLE_DATA .= '
            <tr valign="top">
                <td valign="top" style="background-color:#F0F1F2; padding:2px; border-left:#B9D3E6 solid 1px; border-bottom:#B9D3E6 solid 1px;" width="300" nowrap><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;"><a href="'.$arItem["URL"].'">'.$arItem["TITLE"].'</a></span></td>
                <td valign="top" align="right" style="background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="120"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem["DEADLINE"].'</span></td>
                <td valign="top" align="right" style="background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem["RESPONSIBLE"].'</span></td>
                <td valign="top" align="right" style="background-color:#F0F1F2; padding:2px; border-bottom:#B9D3E6 solid 1px;" width="150"><span style="font-family: Arial, Helvetica, sans-serif; font-size:12px; color:#000000;">'.$arItem["AUTHOR"].'</span></td>
            </tr>';

        }
        $TABLE_FOOTER = '</table>';
        $HTML_FOOTER = '</body>
                        </html>';
        $arEventFields = array(
           "EMAIL_TO" => CTools::GetByEmail($userId),
           "HTML_HEADER" => $HTML_HEADER,
           "TABLE_HEADER" => $TABLE_HEADER,
           "TABLE_DATA" => $TABLE_DATA,
           "TABLE_FOOTER" => $TABLE_FOOTER,
           "HTML_FOOTER" => $HTML_FOOTER,
            'TITLE' => 'Отчёт по не завершенным задачам'
        );
        if (count($arTaskItems) > 0) {
            CEvent::Send("TASK_REPORT", "s1", $arEventFields);
        }
    }
}
