<?
CJSCore::Init(array("jquery"));
$APPLICATION->SetAdditionalCSS("/local/semantic/semantic.min.css", true);
$APPLICATION->AddHeadScript("/local/semantic/semantic.min.js", true);


require_once($_SERVER["DOCUMENT_ROOT"].'/local/php_interface/include/ngsec/autoload.php');
AddEventHandler('crm','OnBeforeCrmEventDelete','OnBeforeCrmEventDelete');
AddEventHandler('crm','OnAfterCrmAddEvent','OnAfterCrmAddEvent');
AddEventHandler('crm','OnBeforeCrmDealAdd','OnBeforeCrmDealAdd');
AddEventHandler('crm','OnBeforeCrmDealUpdate','OnBeforeCrmDealUpdate');
AddEventHandler('crm', 'OnAfterCrmDealUpdate', 'OnAfterCrmDealUpdate');
AddEventHandler('crm', 'OnAfterCrmCompanyAdd', 'OnAfterCrmCompanyAdd');
AddEventHandler('crm', 'OnAfterCrmCompanyUpdate', 'OnAfterCrmCompanyUpdate');
AddEventHandler('crm', 'OnAfterCrmDealAdd', 'OnAfterCrmDealAdd');
AddEventHandler('crm', 'OnAfterCrmContactAdd', 'OnAfterCrmContactAdd');
AddEventHandler('crm', 'OnBeforeCrmAddEvent', 'OnBeforeCrmAddEvent');
AddEventHandler('tasks','OnBeforeTaskDelete', 'OnBeforeTaskDelete');
AddEventHandler('crm', 'OnBeforeCrmDealDelete','OnBeforeCrmDealDelete');
AddEventHandler('crm','OnAfterCrmLeadUpdate','OnAfterCrmLeadUpdate');
AddEventHandler("im", "OnAfterNotifyAdd", "OnAfterNotifyAddHandler");
AddEventHandler("socialnetwork", "OnBeforeSocNetMessagesAdd", "OnBeforeSocNetMessagesAdd");
AddEventHandler("sale", "OnSaleBeforeStatusOrder", "OnSaleBeforeStatusOrder");
AddEventHandler("sale", "OnSaleStatusOrder", "OnSaleStatusOrder");

function OnBeforeCrmEventDelete($eventId){
    $rest = new RestCrmEvents();
    $rest->deleteEvent($eventId);
}

function OnAfterCrmAddEvent($eventId, $arFields){
    if ($arFields['EVENT_ID'] == 'INTEREST' && ($arFields['ENTITY_TYPE'] == 'COMPANY' || $arFields['ENTITY_TYPE'] == 'DEAL')){
        $rest = new RestCrmEvents();
        $rest->saveEvent($eventId, $arFields);
    }
}

$NGSbOrderStatusChange = false;
function OnBeforeCrmDealAdd(&$arFields){
    if ($arFields['COMPANY_ID'] == 0 && !$arFields['EXCHANGE_1C']) {
        $arFields['RESULT_MESSAGE'] = 'Не указан партнер!';
        return false;
    }
    if ($arFields['UF_CRM_1381998634'] == 0 && !$arFields['EXCHANGE_1C']) {
        $arFields['RESULT_MESSAGE']  = 'Не указан заказчик!';
        return false;
    }
     /*if (strlen($arFields['CLOSEDATE']) <= 0 && !$arFields['EXCHANGE_1C']){
        $arFields['RESULT_MESSAGE']  = 'Не указана предполагаема дата завершения сделки!';
        return false;
    }*/
}
function OnBeforeCrmDealUpdate(&$arFields, $ID){
   /* if ($arFields['COMPANY_ID'] == 0 && !$arFields['EXCHANGE_1C']) {
        $arFields['RESULT_MESSAGE'] = 'Не указан партнер!';
        return false;
    }
    if ($arFields['UF_CRM_1381998634'] == 0 && !$arFields['EXCHANGE_1C']) {
        $arFields['RESULT_MESSAGE'] = 'Не указан заказчик!';
        return false;
    }*/
    /*if (strlen($arFields['CLOSEDATE']) <= 0 && !$arFields['EXCHANGE_1C']){
        $arFields['RESULT_MESSAGE']  = 'Не указана предполагаема дата завершения сделки!';
        return false;
    }*/
    if (count($arFields['UF_CRM_DEAL_HELPERS']) > 0){
        \Bitrix\Main\loader::includeModule('crm');
        $arUsers = $arFields['UF_CRM_DEAL_HELPERS'];
        $arResult = array();
        foreach($arUsers as $item){
            if ($item > 0){
                $arResult[] = $item;
            }
        }

        $obFields = CCrmDeal::GetList(array(),array('ID' => $ID,'CHECK_PERMISSIONS' => 'N'),array('UF_CRM_DEAL_HELPERS'));
        if($row = $obFields->GetNext()){
            $arResult = array_diff($arResult, $row['UF_CRM_DEAL_HELPERS']);
        }

        foreach($arResult as $arUser){
            $rsUser = CUser::GetByID($arUser);
            if($rUser = $rsUser->Fetch()) {
                $arEventFields = array(
                    "MESSAGE" => "Вас добавили в ассистенты сделки № " . $ID,
                    'EMAIL_TO' => $rUser['EMAIL']

                );
                CEvent::Send("NOTIFY_EMAIL", 's1', $arEventFields);
            }
        }
    }
}

function OnAfterCrmCompanyAdd($arFields)
{
    if (!$arFields['IS_EXTERNAL'])
    {
        $arFields['COMPANY_TYPE'] = $arFields['COMPANY_TYPE'] == 1 ? 'VENDOR' : $arFields['COMPANY_TYPE'];
        if ($arFields['COMPANY_TYPE'] == 'PARTNER' || $arFields['COMPANY_TYPE'] == 'INVESTOR' || $arFields['COMPANY_TYPE'] == 'VENDOR')
        {
            $restContragents = new RestContragents();
            $restContragents->UpdateContragent($arFields);
        }
    }
}
function OnAfterCrmCompanyUpdate($arFields)
{
    if (!$arFields['IS_EXTERNAL'])
    {
        $arFields['COMPANY_TYPE'] = $arFields['COMPANY_TYPE'] == 1 ? 'VENDOR' : $arFields['COMPANY_TYPE'];
        if ($arFields['COMPANY_TYPE'] == 'PARTNER' || $arFields['COMPANY_TYPE'] == 'INVESTOR' || $arFields['COMPANY_TYPE'] == 'VENDOR')
        {
            $restContragents = new RestContragents();
            $restContragents->UpdateContragent($arFields);
        }
    }
}
function OnSaleStatusOrder_mail($ID, $val){

    $arInvoiceStatusList = CCrmStatus::GetStatusListEx('INVOICE_STATUS');
    $arFilter = array('CHECK_PERMISSIONS' => 'N');
    $arFilter['ID'] = $ID;
    $obRes = CCrmInvoice::GetList($arOrder, $arFilter, false, false);
    $arEventFields = array();
    while ($arRes = $obRes->Fetch()) {
        $arEventFields['ID'] = $ID;
        $arEventFields['STATUS'] = $arInvoiceStatusList[$val];
        $arEventFields['URL'] = "https://".SITE_SERVER_NAME.CComponentEngine::MakePathFromTemplate('/crm/invoice/show/#ID#/',
            array('ID' => $ID));
        if (strlen($arRes['USER_EMAIL'])>0)
            $arEventFields['EMAIL'] = $arRes['USER_EMAIL'];
        
        $arEventFields['MANAGER'] = $arRes['RESPONSIBLE_NAME'] . " " . $arRes['RESPONSIBLE_LAST_NAME'];
    }
    $res = CEvent::Send("INVOICE_CHANGE_STATUS", SITE_ID, $arEventFields);
    file_put_contents('/home/bitrix/www/test.log',SITE_ID." - ".print_r($res, true));
}

function OnSaleBeforeStatusOrder($ID, $STATUS_ID)
{
	global $NGSbOrderStatusChange;
	if ($STATUS_ID == 'S') {
		$CCrmInvoice = new CCrmInvoice();
		$arInvoice = $CCrmInvoice->GetByID($ID);
		if ($arInvoice['STATUS_ID'] != 'S') {
			$NGSbOrderStatusChange = true;
		}
	}
}

function OnSaleStatusOrder($ID, $STATUS_ID)
{
	global $NGSbOrderStatusChange;

    OnSaleStatusOrder_mail($ID, $STATUS_ID);
	if ($NGSbOrderStatusChange) {
		$CCrmInvoice = new CCrmInvoice();
		$arInvoice = $CCrmInvoice->GetByID($ID);
        //Вытаскиваем конечного заказчика у счета по сделки
		$customerTitle = '';
        if ($arInvoice['UF_DEAL_ID'] > 0)
        {
            $rsDeals = CCrmDeal::GetList(array(),array('ID' => $arInvoice['UF_DEAL_ID'],'CHECK_PERMISSIONS'=>'N'),array('UF_CRM_1381998634'));
            if($arDeal = $rsDeals->Fetch()){
                if ($arDeal['UF_CRM_1381998634'] > 0) {
                    $rsCompany = CCrmCompany::GetList(array(), array('ID' => $arDeal['UF_CRM_1381998634'],'CHECK_PERMISSIONS' => 'N'), array('TITLE'));
                    if($arCompany = $rsCompany->GetNext()){
                        $customerTitle = $arCompany['TITLE'];
                    }
                }
            }
        }
		$rsCompany = CCrmCompany::GetList(array(), array('ID' => $arInvoice['UF_COMPANY_ID']), array());
		$arCompany = $rsCompany->GetNext();
		
		if (!empty($arInvoice['RESPONSIBLE_ID'])) {
			$CUser = new CUser();
			$rsResponsible = $CUser->GetByID($arInvoice['RESPONSIBLE_ID']);
			$arResponsible = $rsResponsible->GetNext();
		}

		$arResult = array(
			'ID' => $ID,
			'GUID' => $arInvoice['XML_ID'],
			'RESPONSIBLE_ID' => $arResponsible['XML_ID'],
			'INN' => $arCompany['UF_CRM_1363603025'],
			'KPP' => $arCompany['UF_CRM_1363603036'],
			'CURRENCY' => $arInvoice['CURRENCY'],
			'CUSTOMER' => $customerTitle,
			'ITEMS' => array()
		);
		$CSaleProducts = new CSaleProducts();
            $CSaleBasket = new CSaleBasket();
            $dbRes = $CSaleBasket->GetList(
                array('ID' => 'ASC'), array('ORDER_ID' => $ID), false, false,
                array(
                    'PRODUCT_ID',
                    'QUANTITY',
                    'PRICE'
                )
            );
            $arItems = array();
            while ($row = $dbRes->Fetch())
            {
            	$arItem = array();
                $arItem['ARTICLE'] = $CSaleProducts->getArticleByProductId($row['PRODUCT_ID']);
                $arItem['QUANTITY'] = $row['QUANTITY'];
                $arItem['PRICE'] = $row['PRICE'];
                
                $arItems[] = $arItem;
            }
		$arResult['ITEMS'] = $arItems;

		$sJson = json_encode($arResult);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://1c.ngsec.ru:8443/oper_dev/hs/order.hs/Invoice/?Inv='.$sJson);
		curl_setopt($ch, CURLOPT_USERPWD, "admin:BdfyjD3341");
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'));
		$res = curl_exec($ch);
        function removeBOM($text="") {
            $text = substr($text, 1);
            return $text;
        }
		if (curl_errno($ch)) {
			$serror = curl_error($ch);
			//return false;
		} else {
			$arData = json_decode(trim(removeBOM($res)), true);
            $arFields = array(
                'XML_ID' => $arData['GUID'],
                'RESPONSIBLE_ID' => $arInvoice['RESPONSIBLE_ID']
            );
			$CCrmInvoice->Update($ID, $arFields);
		}
	}
}

function OnBeforeSocNetMessagesAdd($arFields)
{
    return false;
}
function OnAfterNotifyAddHandler($messageId, $arFields)
{
    CIMMail::MailNotifyAgent();
}

function OnAfterCrmLeadUpdate($arFields)
{
    //Обработка лида при конвертировании
    if ($arFields['STATUS_ID'] == 'CONVERTED' && $arFields['CONTACT_ID'] > 0 && strlen($_REQUEST['webinar_guid']) >0)
    {
        $arErrorsTmp = array();
         $webinarGuid = $_REQUEST['webinar_guid'];
        \Bitrix\Crm\WebinarTaskTable::createTaskForContact("",$arFields['ID'], $arFields['CONTACT_ID'], $arFields['COMPANY_ID'],0, $webinarGuid);
    }
}

function OnBeforeCrmDealDelete($ID)
{
    if ($GLOBALS["USER"]->isAdmin())
        return true;
    else
        return false;
}
function OnBeforeTaskDelete($ID)
{
    if ($GLOBALS["USER"]->isAdmin())
        return true;
    else
        return false;
}
function OnBeforeCrmAddEvent($arFields)
{
    if (array_key_exists('comment', $_REQUEST) && strlen($_REQUEST['comment']) > 0)
    {
        $reqComment = $_REQUEST['comment'];
        if ($arFields['ENTITY_FIELD'] == "STAGE_ID" && $arFields["ENTITY_TYPE"] == "DEAL")
        {
            $arFields["EVENT_TEXT_2"] .= ". \nКомментарий: ".$reqComment;
        }
    }
    return $arFields;
}

function OnAfterCrmContactAdd($arFields)
{
    if (!CModule::IncludeModule('crm')) return;
    $arRewriteFields = array('EXPORT' => 'Y', 'OPENED' => 'Y');
    $CrmContact = new CCrmContact();
    $CrmContact->Update($arFields['ID'], $arRewriteFields);
}
function OnAfterCrmDealAdd(&$arFields)
{
    $datePay = MakeTimeStamp($arFields["UF_CRM_1369313724"]); //Планируемая дата платежа
    $dateShipment = MakeTimeStamp($arFields["UF_CRM_1385483673"]); // Планируемая дата отгрузки
    $datePostPay = MakeTimeStamp($arFields['UF_CRM_1385483692']); // Постпродажное сопровождение
    $resultDate = max($datePay, $dateShipment, $datePostPay);
   /* $closeDate = MakeTimeStamp($arFields["CLOSEDATE"]);
    if ($resultDate > 0 && $closeDate != $resultDate) {
        $arFields["CLOSEDATE"] = ConvertTimeStamp($resultDate,"FULL");
    }*/
    if (strlen($arFields["UF_CRM_1371562912"]) <= 0)
    {
        $arrAdd = array(
            "DD"	=> 7,
            "MM"	=> 0,
            "YYYY"	=> 0,
            "HH"	=> 0,
            "MI"	=> 0,
            "SS"	=> 0
        );
        $stmp = AddToTimeStamp($arrAdd, MakeTimeStamp(time()));
        $arFields["UF_CRM_1371562912"] = ConvertTimeStamp($stmp,"FULL");
    }
    if (strlen($arFields["UF_CRM_1385483673"]) <= 0)
    {
        $arrAdd = array(
            "DD"	=> 90,
            "MM"	=> 0,
            "YYYY"	=> 0,
            "HH"	=> 0,
            "MI"	=> 0,
            "SS"	=> 0
        );
        $stmp = AddToTimeStamp($arrAdd, MakeTimeStamp(time()));
        $arFields["UF_CRM_1385483673"] = ConvertTimeStamp($stmp,"FULL");
    }
    if (!CModule::IncludeModule('crm')) return;
    $CCrmDeal = new CCrmDeal();
    $arFields['NOT_EVENT'] = true;
    $bSuccess = $CCrmDeal->Update($arFields['ID'], $arFields);
}

function OnAfterCrmDealUpdate($arFields)
{
    if (strlen($arFields['TITLE']) > 0 && !$arFields['NOT_EXCHANGE']) {
        $rest = new RestDeals();
        $rest->UpdateDeal($arFields);

    }
    if ($arFields['NOT_EVENT']) return;

    //TODO: Уведомить продукт-менеджера о проверки новой сделки

    $datePay = MakeTimeStamp($arFields["UF_CRM_1369313724"]); //Планируемая дата платежа
    $dateShipment = MakeTimeStamp($arFields["UF_CRM_1385483673"]); // Планируемая дата отгрузки
    $datePostPay = MakeTimeStamp($arFields['UF_CRM_1385483692']); // Постпродажное сопровождение
    $resultDate = max($datePay, $dateShipment, $datePostPay);
    $closeDate = MakeTimeStamp($arFields["CLOSEDATE"]);
    $reqComment = $_REQUEST['comment'];
    if (strlen($reqComment) > 0)
        $arFields['COMMENTS'] .= '<br />'.ConvertTimeStamp(false, "SHORT")." - ".$reqComment;
    if ($resultDate > 0 && $closeDate != $resultDate) {
        \Bitrix\Main\Loader::includeModule('crm');
        $CCrmDeal = new CCrmDeal();
        //$arFields["CLOSEDATE"] = ConvertTimeStamp($resultDate,"FULL");
        $arFields['NOT_EVENT'] = true;
        $arFields['NOT_EXCHANGE'] = true;
        unset($_REQUEST['comment']);
        $bSuccess = $CCrmDeal->Update($arFields['ID'], $arFields, true, false);

    }
    elseif(strlen($reqComment) > 0)
    {
        unset($_REQUEST['comment']);
        $arUpdateFields = array('COMMENTS' => $arFields['COMMENTS']);
        $updateResult = \Bitrix\Crm\DealTable::update($arFields['ID'],$arUpdateFields);

    }
}
function AgentGetCurrencyRate()
{
    global $DB;

    // подключаем модуль "валют"
    if(!CModule::IncludeModule('currency'))
        return "AgentGetCurrencyRate();";
 
    $arCurList = array('USD', 'EUR', 'KZT', 'UAH');
    $bWarning = False;
    $rateDay = GetTime(time(), "SHORT", LANGUAGE_ID);
    $QUERY_STR = "date_req=".$DB->FormatDate($rateDay, CLang::GetDateFormat("SHORT", SITE_ID), "D.M.Y");
    $strQueryText = QueryGetData("www.cbr.ru", 80, "/scripts/XML_daily.asp", $QUERY_STR, $errno, $errstr);

    // данная строка нужна только для сайтов в кодировке utf8
    $strQueryText = iconv('windows-1251', 'utf-8', $strQueryText);
    if (strlen($strQueryText) <= 0)
        $bWarning = True;

    if (!$bWarning)
    {
        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");
        $strQueryText = eregi_replace("]{1,}>", "", $strQueryText);
        $strQueryText = eregi_replace("<"."\?XML[^>]{1,}\?".">", "", $strQueryText);
        $objXML = new CDataXML();
        $objXML->LoadString($strQueryText);
        $arData = $objXML->GetArray();
        $arFields = array();
        $arCurRate["CURRENCY_CBRF"] = array();


        if (is_array($arData) && count($arData["ValCurs"]["#"]["Valute"])>0)
        {
            for ($j1 = 0; $j1<count($arData["ValCurs"]["#"]["Valute"]); $j1++)
            {
                if(array_search($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"], $arCurList) > 0)
                {
                    $arFields = array(
                        "CURRENCY" => $arData["ValCurs"]["#"]["Valute"][$j1]["#"]["CharCode"][0]["#"],
                        'DATE_RATE' => $rateDay,
                        'RATE' => DoubleVal(str_replace(",", ".", $arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Value"][0]["#"])),
                        'RATE_CNT' => IntVal($arData["ValCurs"]["#"]["Valute"][$j1]["#"]["Nominal"][0]["#"]),
                    );
                    CCurrencyRates::Add($arFields);
                }
            }
        }
    }
    return "AgentGetCurrencyRate();";
}
?>
